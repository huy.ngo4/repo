# Container tools

## 0. Require

- Estimate time: 3 day
- Raise any questions when problem occurs

## 1. Concept

- [SQL tutorial link](https://www.postgresqltutorial.com/)

## 2. Exercise

- go to [website](https://sqliteonline.com/)
- sign in
- click `PostgreSQL` in left menu
- click `Import`
- select `northwind.sql`
- do some test below 

## practice


# FROM -> WHERE -> GROUP BY -> HAVING -> SELECT -> DISTINCT -> ORDER BY -> LIMIT 
HÀM TỔNG HỢP https://www.postgresqltutorial.com/postgresql-aggregate-functions/


1. Write a query to get Product name and quantity/unit.
SELECT product_name, quantity_per_unit 
FROM products; 

2. Write a query to get current Product list (Product ID and name). 

SELECT product_id, product_name 
FROM products 
WHERE discontinued=0
ORDER BY product_name;

3. Write a query to get discontinued Product list (Product ID and name).
SELECT product_id, product_name 
FROM products 
WHERE discontinued=1
ORDER BY product_name;


4. Write a query to get most expensive and least expensive Product list (name and unit price).  

Đắt nhất và rẻ nhất
SELECT product_name, unit_price
FROM products
WHERE unit_price in (
	SELECT MAX(unit_price)
  	FROM products
)
UNION 
SELECT product_name, unit_price
FROM products
WHERE unit_price in (
	SELECT MIN(unit_price)
  	FROM products
)

5. Write a query to get Product list (id, name, unit price) where current products cost less than $20.

SELECT product_id, product_name, unit_price  
FROM products 
WHERE discontinued=0 AND unit_price<20
ORDER BY unit_price DESC;


6. Write a query to get Product list (id, name, unit price) where products cost between $15 and $25.

SELECT product_id, product_name, unit_price  
FROM products 
WHERE unit_price BETWEEN 15 AND 25
ORDER BY unit_price DESC;

7. Write a query to get Product list (name, unit price) of above average price.
SELECT product_name, unit_price  
FROM products 
WHERE unit_price>(
      SELECT AVG(unit_price) 
      FROM products)
ORDER BY unit_price;

8. Write a query to get Product list (name, unit price) of ten most expensive products.
SELECT product_name, unit_price  
FROM products 
ORDER BY unit_price desc
LIMIT 10;

9. Write a query to count current and discontinued products.
SELECT discontinued, COUNT(discontinued)
FROM products
GROUP by discontinued;

10. Write a query to get Product list (name, units on order , units in stock) of stock is less than the quantity on order. 
SELECT product_id, product_name, units_in_stock, units_on_order
FROM products
INNER JOIN order_details USING(product_id)
GROUP By product_id
HAVING units_in_stock < sum(quantity)
ORDER BY product_id;


11. Make a list of products that are out of stock
SELECT *
FROM products
WHERE units_in_stock=0;

12. Make a complete list of customers along with the number of orders they have placed
SELECT customers.*, COUNT(customer_id) AS number_of_orders
FROM customers
INNER JOIN orders
USING(customer_id)
GROUP BY customer_id
ORDER BY customer_id;

13. Make a complete list of customers, the OrderID and date of any orders they have made. Include customers who have not placed any orders
SELECT customers.*, order_id, order_date
FROM customers
LEFT JOIN orders USING(customer_id)
ORDER BY customer_id;

14. Make a list of categories and suppliers who supply products within those categories
Lấy cả danh mục mà kể cả chưa có nhà cung cấp
SELECT categories.*, suppliers.*
FROM categories
LEFT JOIN products USING(category_id)
LEFT JOIN suppliers USING(supplier_id)
ORDER by supplier_id;

Chỉ lấy các danh mục đã có nhà cung cấp
SELECT categories.*, suppliers.*
FROM categories
INNER JOIN products USING(category_id)
INNER JOIN suppliers USING(supplier_id)
ORDER by supplier_id;

15. Make a list of products and total up the number of actual items ordered. Put the most frequently ordered item at the top of the list and so on to the least frequently ordered item.
SELECT products.*, SUM(quantity) as number_of_actual_items_ordered
FROM products
INNER JOIN order_details 
USING(product_id)
GROUP BY product_id
ORDER BY number_of_actual_items_ordered DESC;


16. Make a list of products and the number of orders in which the product appears.  Put the most frequently ordered item at the top of the list and so on to the least frequently ordered item.
SELECT products.*, COUNT(product_id) as number_of_orders
FROM products
INNER JOIN order_details 
USING(product_id)
GROUP BY product_id
ORDER BY number_of_orders DESC;

17. Make a list of products that need to be re-ordered i.e. where the units in stock and the units on order is less than the reorder level.
SELECT * 
FROM products
WHERE units_in_stock < reorder_level AND units_on_order < reorder_level;

18. Make a list of products that are out of stock and have not been discontinued. Include the suppliers’ names.

SELECT products.*, company_name
FROM products
INNER JOIN suppliers USING(supplier_id)
WHERE units_in_stock=0 AND discontinued=0






