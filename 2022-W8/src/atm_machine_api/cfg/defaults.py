from fii.setup import env

APPLICATION_NAME = env("APPLICATION_NAME", "atm-machine-app")
DB_DSN = None
EXPOSE_SWAGGER_DOCS = False

ATM_MACHINE_API_DOMAIN = None
ATM_MACHINE_API_ENDPOINT = None

APP_RUNNING_PORT = None
ATM_MACHINE_SCHEMA = None


# Swagger
OPENAPI_EXPOSE_SWAGGER_DOCS = True
OPENAPI_URL_PREFIX = "gotecq.atm.machine"
OPENAPI_FILE_META = "tmpl/openapi.yml"
OPENAPI_TITLE = "ATM Machine API docs"
