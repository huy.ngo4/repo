from datetime import datetime
from pyrsistent import field
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import AtmMachineModel

@cr.register("atm-machine")
class AtmMachineResource(PostgresCqrsResource):
    __backend__ = AtmMachineModel

    _id = field(type=UUID_TYPE)

    amount = field(type=nullable(int))
    address = field(type=nullable(str))
    street = field(type=nullable(str))
    district = field(type=nullable(str))
    city = field(type=nullable(str))
    country = field(type=nullable(str))
    bank_id = field(type=nullable(UUID_TYPE))


    