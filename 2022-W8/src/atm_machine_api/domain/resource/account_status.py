from pyrsistent import field

from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import AccountStatusModel

#xác định các đối tượng mà muốn xử lý theo lớp
@cr.register("account-status")
class AccountStatusResource(PostgresCqrsResource):
    __backend__ = AccountStatusModel

    _id = field(type=UUID_TYPE)

    description = field(type=nullable(str))  
    