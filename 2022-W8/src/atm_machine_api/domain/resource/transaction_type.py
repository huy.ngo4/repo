from datetime import datetime
from pyrsistent import field
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import TransactionTypeModel

@cr.register("transaction-type")
class TransactionTypeResource(PostgresCqrsResource):
    __backend__ = TransactionTypeModel

    _id = field(type=UUID_TYPE)

    transaction_fee_amount = field(type=nullable(int)) 
    transaction_limit_amount = field(type=nullable(int)) 
    description = field(type=nullable(str))
    name = field(type=nullable(str))
    bank_id = field(type=nullable(UUID_TYPE)) 