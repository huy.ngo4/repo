from pyrsistent import field
from datetime import datetime

from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import CardModel

#xác định các đối tượng mà muốn xử lý theo lớp
@cr.register("card")
class CardResource(PostgresCqrsResource):
    __backend__ = CardModel

    _id = field(type=UUID_TYPE)
    pin = field(type=nullable(str))  
    effective_date = field(type=nullable(datetime))
    expiration_date = field(type=nullable(datetime))
    bank_account_id = field(type=nullable(UUID_TYPE)) 