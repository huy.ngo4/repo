from datetime import datetime
from pyrsistent import field
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import TransactionModel

@cr.register("transaction")
class TransactionResource(PostgresCqrsResource):
    __backend__ = TransactionModel

    _id = field(type=UUID_TYPE)
    transaction_date = field(type=nullable(datetime)) 
    amount = field(type=nullable(int))
    transaction_fee = field(type=nullable(int)) 
    transfer_content = field(type=nullable(str))
    transaction_type_id = field(type=nullable(UUID_TYPE)) 
    atm_machine_id = field(type=nullable(UUID_TYPE)) 
    source_account_id = field(type=nullable(UUID_TYPE)) 
    destination_account_id = field(type=nullable(UUID_TYPE)) 
