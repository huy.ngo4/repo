from datetime import datetime
from pyrsistent import field
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import BankModel, BankBranchModel

@cr.register("bank")
class BankResource(PostgresCqrsResource):
    __backend__ = BankModel

    _id = field(type=UUID_TYPE)

    name = field(type=nullable(str))
    address = field(type=nullable(str))
    street = field(type=nullable(str))
    district = field(type=nullable(str))
    city = field(type=nullable(str))
    country = field(type=nullable(str))
    minimum_balance=field(type=nullable(int))

@cr.register("bank-branch")
class BankBranchResource(PostgresCqrsResource):
    __backend__ = BankBranchModel

    _id = field(type=UUID_TYPE)

    bank_id = field(type=nullable(UUID_TYPE)) 
    name = field(type=nullable(str))
    address = field(type=nullable(str))
    street = field(type=nullable(str))
    district = field(type=nullable(str))
    city = field(type=nullable(str))
    country = field(type=nullable(str))
 


    