from datetime import datetime
from pyrsistent import field
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import IdCardModel

@cr.register("id-card")
class IdCardResource(PostgresCqrsResource):
    __backend__ = IdCardModel

    _id = field(type=UUID_TYPE)

    first_name = field(type=nullable(str))  
    middle_name = field(type=nullable(str))   
    last_name = field(type=nullable(str))  
    date_of_birth = field(type=nullable(datetime))   
    number = field(type=nullable(str))
    address = field(type=nullable(str))
    street = field(type=nullable(str))
    district = field(type=nullable(str))
    city = field(type=nullable(str))
    country = field(type=nullable(str))
    date_of_issue = field(type=nullable(datetime)) 
    place_of_issue = field(type=nullable(str))
    gender_id = field(type=nullable(str)) 
