from datetime import datetime
from xmlrpc.client import Boolean
from pyrsistent import field

from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import CustomerModel

from atm_machine_api.model import CustomerAddressModel


#xác định các đối tượng mà muốn xử lý theo lớp
@cr.register("customer")
class CustomerResource(PostgresCqrsResource):
    __backend__ = CustomerModel

    _id = field(type=UUID_TYPE)
    first_name = field(type=nullable(str))  
    middle_name = field(type=nullable(str))   
    last_name = field(type=nullable(str))   
    date_of_birth = field(type=nullable(datetime))   
    email = field(type=nullable(str)) 
    phone_number = field(type=nullable(str)) 
    id_number = field(type=nullable(UUID_TYPE)) 
    bank_id = field(type=nullable(UUID_TYPE)) 

#xác định các đối tượng mà muốn xử lý theo lớp
@cr.register("customer-address")
class CustomerAddressResource(PostgresCqrsResource):
    __backend__ = CustomerAddressModel

    _id = field(type=UUID_TYPE)
    address = field(type=nullable(str))
    street = field(type=nullable(str))
    district = field(type=nullable(str))
    city = field(type=nullable(str))
    country = field(type=nullable(str))
    is_primary = field(type=nullable(bool))
    customer_id = field(type=nullable(UUID_TYPE)) 