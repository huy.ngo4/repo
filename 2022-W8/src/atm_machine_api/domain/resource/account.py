from datetime import datetime
from pyrsistent import field
from enum import Enum
from fii_cqrs import resource as cr
from fii_cqrs.helper import nullable
from fii_cqrs.identifier import UUID_TYPE
from sanic_cqrs import PostgresCqrsResource

from atm_machine_api.model import AccountModel


class AccountStatus(Enum):
    actived = "actived"
    deactived = "deactived"

#xác định các đối tượng mà muốn xử lý theo lớp
@cr.register("account")
class AccountResource(PostgresCqrsResource):
    __backend__ = AccountModel

    _id = field(type=UUID_TYPE)

    username = field(type=nullable(str))  
    account_password = field(type=nullable(str))   
    avaiable_balance = field(type=nullable(int))   
    current_balance = field(type=nullable(int))   
    account_type_id = field(type=nullable(str)) 
    account_status_id = field(type=nullable(int))
    customer_id = field(type=nullable(UUID_TYPE)) 
      