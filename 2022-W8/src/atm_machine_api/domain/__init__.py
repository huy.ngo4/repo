from sanic_cqrs import register_domain_endpoint

from .command import *  # noqa
from .domain import AtmMachineDomain
from .event import *  # noqa
from .resource import *  # noqa
from .response import *  # noqa


def configure_domain(app):
    register_domain_endpoint(app, AtmMachineDomain)


__all__ = "configure_domain"
