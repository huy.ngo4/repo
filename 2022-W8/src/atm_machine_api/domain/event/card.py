from fii_cqrs.event import Event, field
from fii_cqrs.state import InsertRecord, UpdateRecord, InvalidateRecord

from atm_machine_api.domain.datadef import CreateCardEventData, UpdateCardEventData


from ..domain import AtmMachineDomain

_entity = AtmMachineDomain.entity
_committer = AtmMachineDomain.event_committer

#Create
@_entity('card-created')
class CardCreated(Event):
    data = field(type=CreateCardEventData, mandatory=True)


@_committer(CardCreated)
async def process__card_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity('card-updated')
class CardUpdated(Event):
    data = field(type=UpdateCardEventData, mandatory=True)

@_committer(CardUpdated)
async def process__card_updated(statemgr, event):
    yield UpdateRecord(resource=event.target.resource, identifier=event.target.identifier, data=event.data)


@_entity('card-deleted')
class CardDeleted(Event):
    pass

@_committer(CardDeleted)
async def process__card_deleted(statemgr, event):
    yield InvalidateRecord(resource=event.target.resource, identifier=event.target.identifier)