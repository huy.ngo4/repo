from fii_cqrs.event import Event, field
from fii_cqrs.state import InsertRecord, UpdateRecord, InvalidateRecord

from atm_machine_api.domain.datadef import UpdateAtmMachineEventData, CreateAtmMachineEventData


from ..domain import AtmMachineDomain

_entity = AtmMachineDomain.entity
_committer = AtmMachineDomain.event_committer

#Create
@_entity('atm-machine-created')
class AtmMachineCreated(Event):
    data = field(type=CreateAtmMachineEventData, mandatory=True)


@_committer(AtmMachineCreated)
async def process__atm_machine_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)

@_entity('atm-machine-updated')
class AtmMachineUpdated(Event):
    data = field(type=UpdateAtmMachineEventData, mandatory=True)

@_committer(AtmMachineUpdated)
async def process__atm_machine_updated(statemgr, event):
    yield UpdateRecord(resource=event.target.resource, identifier=event.target.identifier, data=event.data)


@_entity('atm-machine-deleted')
class AtmMachineDeleted(Event):
    pass

@_committer(AtmMachineDeleted)
async def process__atm_machine_deleted(statemgr, event):
    yield InvalidateRecord(resource=event.target.resource, identifier=event.target.identifier)