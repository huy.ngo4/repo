from fii_cqrs.event import Event, field
from fii_cqrs.state import InsertRecord, UpdateRecord, InvalidateRecord

from atm_machine_api.domain.datadef import  UpdateIdCardEventData, UpdateCustomerEventData

from ..domain import AtmMachineDomain

_entity = AtmMachineDomain.entity
_committer = AtmMachineDomain.event_committer



@_entity('id-card-updated')
class IdCardUpdated(Event):
    data = field(type=UpdateIdCardEventData, mandatory=True)

@_committer(IdCardUpdated)
async def process__id_card_updated(statemgr, event):
    customer = await statemgr.fetch("customer", _id=event.target.identifier)
    id_card_data = UpdateIdCardEventData.extend_pclass(
            pclass=event.data,
            _id=customer.id_number
        )
    yield UpdateRecord(resource=event.target.resource, identifier=id_card_data._id, data=id_card_data)
    

    id_card_data = await statemgr.fetch(event.target.resource, _id=customer.id_number)
    customer_data = UpdateCustomerEventData(
        _id=event.target.identifier,
        first_name =  id_card_data.first_name,
        middle_name = id_card_data.middle_name,
        last_name = id_card_data.last_name
    )
    yield UpdateRecord(resource="customer", identifier=customer_data._id, data=customer_data)
    

