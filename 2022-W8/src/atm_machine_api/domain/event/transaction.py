from fii_cqrs.event import Event, field
from fii_cqrs.state import InsertRecord, UpdateRecord
from fii_cqrs.identifier import UUID_GENR
from datetime import datetime

from atm_machine_api.domain.datadef import CreateTransactionEventData, UpdateTransactionTypeEventData
from atm_machine_api.domain.datadef import UpdateAccountEventData, TransferMoneyEventData, TradingAccountEventData, UpdateAtmMachineData


from ..domain import AtmMachineDomain

_entity = AtmMachineDomain.entity
_committer = AtmMachineDomain.event_committer


@_entity('transaction-created')
class TransactionCreated(Event):
    data = field(type=CreateTransactionEventData, mandatory=True)

@_committer(TransactionCreated)
async def process__transaction_created(statemgr, event):
    yield InsertRecord(resource=event.target.resource, data=event.data)



#trading account updated version 2
@_entity('trading-account-updated')
class TradingAccountUpdated(Event):
    data = field(type=TradingAccountEventData, mandatory=True)   

@_committer(TradingAccountUpdated)
async def process__trading_account_updated(statemgr, event):
    account_data_update = UpdateAccountEventData(
        _id=event.data._id,
        avaiable_balance = event.data.avaiable_balance
    )
    
    yield UpdateRecord(resource=event.target.resource, 
                    identifier=event.data._id, 
                    data=account_data_update)
   
    try:
      
        transaction_data = CreateTransactionEventData(
            _id=UUID_GENR(),
            transaction_date = datetime.utcnow(), 
            amount = event.data.amount,
            transaction_type_id = event.data.transaction_type_id,
            atm_machine_id = event.data.atm_machine_id,
            source_account_id = event.data._id,
            transaction_fee = event.data.transaction_fee
        )
        atm_machine_data = UpdateAtmMachineData(
            amount = event.data.amount_atm_machine_new
        )
        yield UpdateRecord(resource="atm-machine", identifier=event.data.atm_machine_id, data=atm_machine_data)
    except:
        transaction_data = CreateTransactionEventData(
            _id=UUID_GENR(),
            transaction_date = datetime.utcnow(), 
            amount = event.data.amount,
            transaction_type_id = event.data.transaction_type_id,
            source_account_id = event.data._id,
            transaction_fee = event.data.transaction_fee
        )

    yield InsertRecord(resource="transaction", data=transaction_data)



#money-transfered
@_entity('money-transfered')
class TransactionCreated(Event):
    data = field(type=TransferMoneyEventData, mandatory=True)   

@_committer(TransactionCreated)
async def process__transaction_created(statemgr, event):
 
    source_account_data_update = UpdateAccountEventData(
        _id=event.data.source_account_id,
        avaiable_balance = event.data.new_balance_source_account
    )
    yield UpdateRecord(resource=event.target.resource, identifier=source_account_data_update._id, data=source_account_data_update)

    destination_account_data_update = UpdateAccountEventData(
        _id=event.data.destination_account_id,
        avaiable_balance = event.data.new_balance_destination_account
    )
    yield UpdateRecord(resource=event.target.resource, 
                    identifier=destination_account_data_update._id, 
                    data=destination_account_data_update)

    
    try:
        data_transaction = CreateTransactionEventData(
        _id = event.data._id,
        transaction_date = datetime.utcnow(),
        amount = event.data.amount,
        transfer_content =  event.data.transfer_content,
        transaction_type_id = event.data.transaction_type_id,
        atm_machine_id = event.data.atm_machine_id,
        source_account_id = event.data.source_account_id,
        destination_account_id = event.data.destination_account_id,
        transaction_fee = event.data.transaction_fee
        )
        atm_machine_data = UpdateAtmMachineData(
            amount = event.data.amount_atm_machine_new
        )
        yield UpdateRecord(resource="atm-machine", identifier=event.data.atm_machine_id, data=atm_machine_data)

    except:
        data_transaction = CreateTransactionEventData(
        _id = event.data._id,
        transaction_date = datetime.utcnow(),
        amount = event.data.amount,
        transfer_content =  event.data.transfer_content,
        transaction_type_id = event.data.transaction_type_id,
        source_account_id = event.data.source_account_id,
        destination_account_id = event.data.destination_account_id,
        transaction_fee = event.data.transaction_fee
        )
    yield InsertRecord(resource="transaction", data=data_transaction)


#version 2
@_entity('transaction-type-updated')
class TransactionTypeUpdate(Event):
    data = field(type=UpdateTransactionTypeEventData, mandatory=True)   

@_committer(TransactionTypeUpdate)
async def process__transaction_type_updated(statemgr, event):
   
    yield UpdateRecord(resource=event.target.resource, identifier=event.data._id, data=event.data)

