from fii_cqrs import resource
from fii_cqrs.event import Event, field
from fii_cqrs.identifier import UUID_GENR
from fii_cqrs.state import InsertRecord, UpdateRecord, InvalidateRecord

from atm_machine_api.domain.datadef import CreateCustomerEventData, NewCustomerEventData
from atm_machine_api.domain.datadef import UpdateCustomerEventData
from atm_machine_api.domain.datadef import CreateAccountEventData
from atm_machine_api.domain.datadef import CreateCustomerAddressEventData
from atm_machine_api.domain.datadef import CreateIdCardEventData, CreateCardEventData
 

from ..domain import AtmMachineDomain





_entity = AtmMachineDomain.entity
_committer = AtmMachineDomain.event_committer

@_entity('customer-created')
class CustomerCreated(Event):
    data = field(type=NewCustomerEventData, mandatory=True)

@_committer(CustomerCreated)
async def process__customer_created(statemgr, event):
    data_idcard = CreateIdCardEventData(
        _id = UUID_GENR(),
        first_name = event.data.first_name,
        middle_name = event.data.middle_name,
        last_name = event.data.last_name,
        date_of_birth = event.data.date_of_birth,
        number = event.data.number,
        address = event.data.address,
        street = event.data.street,
        district = event.data.district,
        city = event.data.city,
        country = event.data.country,
        date_of_issue = event.data.date_of_issue,
        place_of_issue = event.data.place_of_issue,
        gender_id = event.data.gender_id,

    )

    yield InsertRecord(resource="id-card", data=data_idcard)
    data_customer = CreateCustomerEventData(
        _id = event.data._id,
        first_name = event.data.first_name,
        middle_name = event.data.middle_name,
        last_name = event.data.last_name,
        date_of_birth = event.data.date_of_birth,
        email = event.data.email,
        phone_number = event.data.phone_number,
       
        id_number = data_idcard._id,
        bank_id = event.data.bank_id 

    )

    yield InsertRecord(resource=event.target.resource, data=data_customer)

    data_account = CreateAccountEventData(
        _id = UUID_GENR(),

        username = event.data.username ,
        account_password = event.data.account_password ,   
        avaiable_balance = 0 ,   
        current_balance =  0 ,  
        account_type_id = "PA" ,   
        account_status_id = 0 ,
        customer_id = data_customer._id,
        
       
    )
    yield InsertRecord(resource="account", data=data_account)

    data_card = CreateCardEventData(
        _id = UUID_GENR(),
        pin = event.data.pin,
        bank_account_id = data_account._id,
        effective_date = event.data.effective_date,
        expiration_date = event.data.expiration_date,
    )
    yield InsertRecord(resource="card", data=data_card)

    data_customer_address = CreateCustomerAddressEventData(
        _id = UUID_GENR(),

        address = event.data.current_address,
        street = event.data.current_stresst,
        district = event.data.current_district,
        city =  event.data.current_city,
        country = event.data.current_country,
        customer_id = data_customer._id,
        is_primary = True
    )
    yield InsertRecord(resource="customer-address", data=data_customer_address)


@_entity('customer-updated')
class CustomerUpdated(Event):
    data = field(type=UpdateCustomerEventData, mandatory=True)

@_committer(CustomerUpdated)
async def process__customer_updated(statemgr, event):
    
    yield UpdateRecord(resource=event.target.resource, identifier=event.target.identifier, data=event.data)


@_entity('customer-deleted')
class CustomerDeleted(Event):
    pass

@_committer(CustomerDeleted)
async def process__customer_deleted(statemgr, event):
    customer_data = await statemgr.fetch(event.target.resource, _id=event.target.identifier)
    yield InvalidateRecord(resource="id-card", identifier=customer_data.id_number)
    
    
    data_query1 = {
        "where": {
            "customer_id": customer_data._id
        }
    }
    data_accounts = await statemgr.find("account", data_query=data_query1)
    for data_account in data_accounts:
        data_query2 = {
            "where": {
                "bank_account_id": data_account._id
            }
        }
        
        data_cards = await statemgr.find("card", data_query=data_query2)
        if data_cards:
            for data_card in data_cards:
                yield InvalidateRecord(resource="card", identifier=data_card._id)
        
        data_query3 = {
            "where": {
                "source_account_id": data_account._id
            }
        }
        data_transactions1 = await statemgr.find("transaction", data_query=data_query3)
        for data in data_transactions1:
            yield InvalidateRecord(resource="transaction", identifier=data._id)

        data_query4 = {
            "where": {
                "destination_account_id": data_account._id
            }
        }
        data_transactions2 = await statemgr.find("transaction", data_query=data_query4)
        for data in data_transactions2:
            yield InvalidateRecord(resource="transaction", identifier=data._id)
        yield InvalidateRecord(resource="account", identifier=data_account._id)
        
    data_query5 = {
        "where":{
            "customer_id": customer_data._id
        }
    }
    data_customer_address = await statemgr.find("customer-address", data_query=data_query5)
    
    for data in data_customer_address:
        yield InvalidateRecord(resource="customer-address", identifier=data._id)

    yield InvalidateRecord(resource=event.target.resource, identifier=event.target.identifier)

