from fii_cqrs.event import Event, field
from fii_cqrs.identifier import UUID_GENR
from fii_cqrs.state import InsertRecord, UpdateRecord, InvalidateRecord

from atm_machine_api.domain.datadef import CreateAccountEventData, CreateAccountData
from atm_machine_api.domain.datadef import UpdateAccountEventData
from atm_machine_api.domain.datadef import CreateCardEventData, CreateAccountCardEventData

from ..domain import AtmMachineDomain

_entity = AtmMachineDomain.entity
_committer = AtmMachineDomain.event_committer

#Create account
@_entity('account-created')
class AccountCreated(Event):
    data = field(type=CreateAccountCardEventData, mandatory=True)

@_committer(AccountCreated)
async def process__account_created(statemgr, event):
    
    data_account = CreateAccountEventData(
        _id = event.data._id,
        username = event.data.username,
        account_password = event.data.account_password,
        avaiable_balance = 0,
        current_balance = 0,
        account_type_id = "PA",
        account_status_id = 0,
        customer_id = event.data.customer_id,


    )

    yield InsertRecord(resource=event.target.resource, data=data_account)

    data_card = CreateCardEventData(
        _id = UUID_GENR(),
        bank_account_id = data_account._id,
        pin = "huy12345@",
        effective_date = event.data.effective_date,
        expiration_date = event.data.expiration_date
    )
    
    yield InsertRecord(resource="card", data=data_card)


#Update account
@_entity('account-updated')
class AccountUpdated(Event):
    data = field(type=UpdateAccountEventData, mandatory=True)

@_committer(AccountUpdated)
async def process__account_updated(statemgr, event):
    yield UpdateRecord(resource=event.target.resource, identifier=event.target.identifier , data=event.data)


#Delete account
@_entity('account-deleted')
class AccountDeleted(Event):
    pass

@_committer(AccountDeleted)
async def process__account_deleted(statemgr, event):
    data_query2 = {
            "where": {
                "bank_account_id": event.target.identifier
            }
        }
        
    data_cards = await statemgr.find("card", data_query=data_query2)
    if data_cards:
        for data_card in data_cards:
            yield InvalidateRecord(resource="card", identifier=data_card._id)
    
    data_query3 = {
        "where": {
            "source_account_id": event.target.identifier
        }
    }
    data_transactions1 = await statemgr.find("transaction", data_query=data_query3)
    for data in data_transactions1:
        yield InvalidateRecord(resource="transaction", identifier=data._id)

    data_query4 = {
        "where": {
            "destination_account_id": event.target.identifier
        }
    }
    data_transactions2 = await statemgr.find("transaction", data_query=data_query4)
    for data in data_transactions2:
        yield InvalidateRecord(resource="transaction", identifier=data._id)


    yield InvalidateRecord(resource=event.target.resource, identifier=event.target.identifier)





