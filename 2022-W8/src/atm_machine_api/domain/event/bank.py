from fii_cqrs import resource
from fii_cqrs import identifier
from fii_cqrs.event import Event, field
from fii_cqrs.state import InsertRecord, UpdateRecord, InvalidateRecord

from atm_machine_api.domain.datadef import CreateBankEventData, UpdateBankEventData, CreateTransactionTypeEventData
from atm_machine_api.domain.datadef import CreateBankSystemEventData, CreateAtmMachineEventData
from fii_cqrs.identifier import UUID_GENR


from ..domain import AtmMachineDomain

_entity = AtmMachineDomain.entity
_committer = AtmMachineDomain.event_committer



@_entity('bank-updated')
class BankUpdated(Event):
    data = field(type=UpdateBankEventData, mandatory=True)

@_committer(BankUpdated)
async def process__bank_updated(statemgr, event):
    yield UpdateRecord(resource=event.target.resource, identifier=event.target.identifier, data=event.data)


@_entity('bank-deleted')
class BankDeleted(Event):
    pass

@_committer(BankDeleted)
async def process__bank_deleted(statemgr, event):
    data_query1 = {
        "where": {
            "bank_id": event.target.identifier
        }
    }
    #delete customer
    customers_data = await statemgr.find("customer", data_query=data_query1)
    
    if customers_data:
        for customer_data in customers_data:
            #delete id-card
            yield InvalidateRecord(resource="id-card", identifier=customer_data.id_number)

            #delete account
            data_query = {
                "where": {
                    "customer_id": customer_data._id
                }
            }
            data_accounts = await statemgr.find("account", data_query=data_query)

            for data_account in data_accounts:
                #delete card
                data_query = {
                    "where": {
                        "bank_account_id": data_account._id
                    }
                }
                
                data_cards = await statemgr.find("card", data_query=data_query)
                if data_cards:
                    for data_card in data_cards:
                        yield InvalidateRecord(resource="card", identifier=data_card._id)
                #delete transaction
                data_query = {
                    "where": {
                        "source_account_id": data_account._id
                    }
                }
                data_transactions1 = await statemgr.find("transaction", data_query=data_query)
                for data in data_transactions1:
                    yield InvalidateRecord(resource="transaction", identifier=data._id)

                data_query = {
                    "where": {
                        "destination_account_id": data_account._id
                    }
                }
                data_transactions2 = await statemgr.find("transaction", data_query=data_query)
                for data in data_transactions2:
                    yield InvalidateRecord(resource="transaction", identifier=data._id)
                
                yield InvalidateRecord(resource="account", identifier=data_account._id)
            
            #delete customer address
            data_query = {
                "where":{
                    "customer_id": customer_data._id
                }
            }
            data_customer_address = await statemgr.find("customer-address", data_query=data_query)
            
            for data in data_customer_address:
                yield InvalidateRecord(resource="customer-address", identifier=data._id)

            yield InvalidateRecord(resource="customer", identifier=customer_data._id)

    #delete atm machine
    atm_machine_data = await statemgr.find("atm-machine", data_query=data_query1)

    if atm_machine_data:
        for data in atm_machine_data:
            yield InvalidateRecord(resource="atm-machine", identifier=data._id)
    
    #delete transaction type
    transaction_type_data = await statemgr.find("transaction-type", data_query=data_query1)
    for data in transaction_type_data:
         yield InvalidateRecord(resource="transaction-type", identifier=data._id)
    #delete bank branch
    bank_branch_data = await statemgr.find("bank-branch", data_query=data_query1)
    if bank_branch_data:
        for data in bank_branch_data:
            yield InvalidateRecord(resource="bank-branch", identifier=data._id)
    yield InvalidateRecord(resource=event.target.resource, identifier=event.target.identifier)


#Create bank and transaction type, cai mat định phí giao dịch, chuyển, rút , và nạp tiền là 0
@_entity('bank-created')
class BankCreated(Event):
    data = field(type=CreateBankSystemEventData, mandatory=True)


@_committer(BankCreated)
async def process__bank_created(statemgr, event):

    data_bank = CreateBankEventData(
        _id=event.data._id,
        name=event.data.name,
        address=event.data.address,
        street=event.data.street,
        district=event.data.district,
        city=event.data.city,
        country=event.data.country,
        minimum_balance=event.data.minimum_balance
    )
    yield InsertRecord(resource=event.target.resource, data=data_bank)

    #atm-machine
    data_atm_machine = CreateAtmMachineEventData(
        _id = UUID_GENR(),

        amount = 300000000,  
        address = event.data.address,
        street = event.data.street,
        district = event.data.district,
        city = event.data.city,
        country = event.data.country, 
        bank_id = data_bank._id
    )
    yield InsertRecord(resource="atm-machine", data=data_atm_machine)
    #rút tiền   
    data1= CreateTransactionTypeEventData(
        _id = UUID_GENR(),
        transaction_fee_amount=event.data.withdrawal_fee,
        name="withdraw",
        transaction_limit_amount=event.data.withdrawal_limit,
        bank_id=event.data._id
    )
    
    yield InsertRecord(resource="transaction-type", data=data1)
    #nạp tiền
    data2= CreateTransactionTypeEventData(
        _id = UUID_GENR(),
        transaction_fee_amount=event.data.deposit_fee,
        name="deposit",
        transaction_limit_amount=event.data.deposit_limit,
        bank_id=event.data._id
    )
    yield InsertRecord(resource="transaction-type", data=data2)

    #chuyen tien
    data3= CreateTransactionTypeEventData(
        _id = UUID_GENR(),
        transaction_fee_amount=event.data.transfer_fee,
        name="transfer",
        transaction_limit_amount=event.data.transfer_limit,
        bank_id=event.data._id
    )
    yield InsertRecord(resource="transaction-type", data=data3)


