from pyrsistent import PClass

from fii_cqrs.command import field
from fii_cqrs.response import CqrsResponse

from .domain import AtmMachineDomain

_entity = AtmMachineDomain.entity


def to_dict(data):
    if isinstance(data, PClass):
        return data.serialize()

    return data


@_entity("account-response")
class AccountResponse(CqrsResponse):
    data = field(type=dict, factory=to_dict, mandatory=True)

@_entity("customer-response")
class AccountResponse(CqrsResponse):
    data = field(type=dict, factory=to_dict, mandatory=True)

@_entity("response")
class AccountResponse(CqrsResponse):
    data = field(type=dict, factory=to_dict, mandatory=True)
