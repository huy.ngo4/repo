from http.client import NotConnected
from fii_cqrs.aggregate import Aggregate
from fii_cqrs.event import Event
from fii_cqrs.identifier import UUID_GENR
from datetime import datetime


from atm_machine_api.domain.datadef import *


class AtmMachineAggregate(Aggregate):
    
    #create account
    async def do__create_account(
        self, data: CreateAccountData
    ) -> Event:
        data_query = {
            "where": {
                "username": data.username
            }
        }
        account = await self.statemgr.find_one("account", data_query=data_query)
        if account:
            raise ValueError("Username already exists ")
        

        event_data = CreateAccountCardEventData.extend_pclass(
            pclass=data, _id=UUID_GENR()
        )
        return self.create_event(
            "account-created", target=self.aggroot, data=event_data
        )

    #update account
    async def do__update_account(
        self, data: UpdateAccountData, id
    ) -> Event:
        event_data = UpdateAccountEventData.extend_pclass(
            pclass=data, _id=id
        )
        return self.create_event(
            "account-updated", target={'resource': 'account', 'identifier': event_data._id}, data=event_data
        )
    
    #Activate account
    async def do__activate_account(
        self, id
    ) -> Event:
        event_data = UpdateAccountEventData.extend_pclass(
            pclass=None, 
            _id=id,
            account_status_id=1
        )
        return self.create_event(
            "account-updated", target={'resource': 'account', 'identifier': event_data._id}, data=event_data
        )
    #Deactivate account
    async def do__deactivate_account(
        self, id
    ) -> Event:
        event_data = UpdateAccountEventData.extend_pclass(
            pclass=None, 
            _id=id,
            account_status_id=0
        )
        return self.create_event(
            "account-updated", target={'resource': 'account', 'identifier': event_data._id}, data=event_data
        )

    #delete account
    async def do__delete_account(
        self
    ) -> Event:

        return self.create_event(
            "account-deleted", target={'resource': 'account', 'identifier': self.aggroot.identifier}, 
            data=None
        )


    #create customer
    async def do__create_customer(
        self, data: NewCustomerData
    ) -> Event:
        data_query = {
            "where": {
                "number": data.number
            }
        }
        id_card = await self.statemgr.find_one("id-card", data_query=data_query)
        
        if id_card:
            data_query = {
            "where": {
                "bank_id": data.bank_id,
                "id_number": id_card._id
            }
            }
            customer = await self.statemgr.find_one("customer", data_query=data_query)

            if customer:
                raise ValueError("Customer already exists")
        
        data_query = {
            "where": {
                "username": data.username
            }
        }
        account = await self.statemgr.find_one("account", data_query=data_query)
        if account:
            raise ValueError("Username already exists ")
        
        event_data = NewCustomerEventData.extend_pclass(
            pclass=data, 
            _id=UUID_GENR(),
            is_primary=True,
            pin = "huy12345@"
        )
        return self.create_event(
            "customer-created", target=self.aggroot, data=event_data
        )
    

    #update customer
    async def do__update_customer(
        self, data: UpdateCustomerData, id
    ) -> Event:
        event_data = UpdateCustomerEventData.extend_pclass(
            pclass=data, _id=id
        )
        return self.create_event(
            "customer-updated", target={'resource': 'customer', 'identifier': event_data._id}, data=event_data
        )
    

    #delete customer
    async def do__delete_customer(
        self, id
    ) -> Event:
       
        return self.create_event(
            "customer-deleted", 
            target={'resource': 'customer', 'identifier': id},
            data=None
        )

     #withdraw money version 2
    async def do__withdraw_money(
        self, data: WithdrawMoneyData
    ) -> Event:
        account = await self.statemgr.fetch(self.aggroot.resource, _id=data._id)
        customer = await self.statemgr.fetch("customer", _id=account.customer_id)
        if account.account_status_id == 0:
            raise ValueError("Your account has been lock")
        data_query1 = {
             "where": {
                "name": "withdraw",
                "bank_id": customer.bank_id
        }
        }
        transaction_type_data=await self.statemgr.find_one("transaction-type", data_query=data_query1)
        

        if transaction_type_data is None:
            raise ValueError("Sai transaction type")
        
       
        bank = await self.statemgr.fetch("bank", _id=customer.bank_id)
        # raise ValueError(bank)
        new_balance = account.avaiable_balance - data.amount - transaction_type_data.transaction_fee_amount
        #Kiểm tra đủ tiền để rút không
        if (new_balance - bank.minimum_balance) < bank.minimum_balance:
            raise ValueError("You dont have enough money to withdraw")

        if data.amount > transaction_type_data.transaction_limit_amount:
            raise ValueError("You can't withdraw too much money at once")
        
        try:
            atm_machine_id = data.atm_machine_id
        except:
            atm_machine_id = None

        if atm_machine_id:
            atm_machine_data = await self.statemgr.fetch("atm-machine", _id=data.atm_machine_id)
            amount_atm_machine_new = atm_machine_data.amount - data.amount
            if amount_atm_machine_new < 0:
                raise ValueError("ATM machine not enough money")
            event_data = TradingAccountEventData.extend_pclass(
                pclass=data,
                avaiable_balance=new_balance,
                amount_atm_machine_new = amount_atm_machine_new,
                transaction_type_id = transaction_type_data._id,
                transaction_fee = transaction_type_data.transaction_fee_amount
            )
            
        else:
            event_data = TradingAccountEventData(
                _id = data._id,
                avaiable_balance=new_balance,
                amount = data.amount,
                transaction_type_id = transaction_type_data._id,
                transaction_fee = transaction_type_data.transaction_fee_amount
            )

        return self.create_event(
            "trading-account-updated", target=self.aggroot, data=event_data
        )

    
    #Deposit money
    async def do__deposit_money(
        self, data: DepositMoneyData
    ) -> Event:

        account = await self.statemgr.fetch(self.aggroot.resource, _id=data._id)
        customer = await self.statemgr.fetch("customer", _id=account.customer_id)
        if account.account_status_id == 0:
            raise ValueError("Your account has been lock")
        data_query3 = {
             "where": {
                "name": "deposit",
                "bank_id": customer.bank_id
        }
        }
        transaction_type = await self.statemgr.find_one("transaction-type", data_query=data_query3)
        if transaction_type is None:
            raise ValueError("Wrong transaction type")
        
        #query phí nạp tiền 
        new_balance = account.avaiable_balance + data.amount - transaction_type.transaction_fee_amount
        
        try:
            atm_machine_id=data.atm_machine_id
        except:
            atm_machine_id = None
        
        if atm_machine_id:
            atm_machine_data = await self.statemgr.fetch("atm-machine", _id=data.atm_machine_id)
            amount_atm_machine_new = atm_machine_data.amount + data.amount

            event_data = TradingAccountEventData.extend_pclass(
                pclass=data,
                avaiable_balance=new_balance,
                amount_atm_machine_new = amount_atm_machine_new,
                transaction_type_id = transaction_type._id,
                transaction_fee = transaction_type.transaction_fee_amount
            )
            
        else:
            event_data = TradingAccountEventData(
                _id = data._id,
                avaiable_balance=new_balance,
                amount = data.amount,
                transaction_type_id = transaction_type._id,
                transaction_fee = transaction_type.transaction_fee_amount
            )
        
        return self.create_event(
            "trading-account-updated", target=self.aggroot, data=event_data
        )


    async def do__transfer_money(
        self, data: TransferMoneyData
    ) -> Event:

        source_account = await self.statemgr.fetch(self.aggroot.resource, _id=data.source_account_id)
        customer1 = await self.statemgr.fetch("customer", _id=source_account.customer_id)
        if source_account.account_status_id == 0:
            raise ValueError("Your account has been lock")
        
        data_query1 = {
             "where": {
                "name": "transfer",
                "bank_id": customer1.bank_id
        }
        }
        transaction_type_data = await self.statemgr.find_one("transaction-type", data_query=data_query1)
        
        if transaction_type_data is None:
            raise ValueError("Sai transaction type")
        
       
        bank = await self.statemgr.fetch("bank", _id=customer1.bank_id)
        #query phí chuyen tien
        new_balance_source_account = source_account.avaiable_balance - data.amount - transaction_type_data.transaction_fee_amount
        #Kiểm tra đủ tiền để rút không
        if (new_balance_source_account - bank.minimum_balance) < bank.minimum_balance:
            raise ValueError("You dont have enough money to transfer")

        if data.amount > transaction_type_data.transaction_limit_amount:
            raise ValueError("You can't transfer too much money at once")

        destination_account = await self.statemgr.fetch(self.aggroot.resource, _id=data.destination_account_id)

        if destination_account.account_status_id == 0:
            raise ValueError("destination account has been lock")
        new_balance_destination_account = destination_account.avaiable_balance +  data.amount
        
        
        atm_machine_data = await self.statemgr.fetch("atm-machine", _id=data.atm_machine_id)
        
        event_data = TransferMoneyEventData.extend_pclass(
            pclass=data,
            _id = UUID_GENR(),
            new_balance_source_account=new_balance_source_account,
            new_balance_destination_account=new_balance_destination_account,
            transaction_type_id = transaction_type_data._id,
            transaction_fee = transaction_type_data.transaction_fee_amount,
            amount_atm_machine_new = atm_machine_data.amount,
        )
           

        return self.create_event(
            "money-transfered", target=self.aggroot, data=event_data
        )
    

    #internet banking
    async def do__internet_banking(
        self, data: InternetBankingData
    ) -> Event:

        source_account = await self.statemgr.fetch(self.aggroot.resource, _id=data.source_account_id)
        customer1 = await self.statemgr.fetch("customer", _id=source_account.customer_id)
        if source_account.account_status_id == 0:
            raise ValueError("Your account has been lock")
        
        data_query1 = {
             "where": {
                "name": "transfer",
                "bank_id": customer1.bank_id
        }
        }
        transaction_type_data = await self.statemgr.find_one("transaction-type", data_query=data_query1)
        
        if transaction_type_data is None:
            raise ValueError("Sai transaction type")
        
       
        bank = await self.statemgr.fetch("bank", _id=customer1.bank_id)
        #query phí chuyen tien
        new_balance_source_account = source_account.avaiable_balance - data.amount - transaction_type_data.transaction_fee_amount
        #Kiểm tra đủ tiền để rút không
        if (new_balance_source_account - bank.minimum_balance) < bank.minimum_balance:
            raise ValueError("You dont have enough money to transfer")

        if data.amount > transaction_type_data.transaction_limit_amount:
            raise ValueError("You can't transfer too much money at once")

        destination_account = await self.statemgr.fetch(self.aggroot.resource, _id=data.destination_account_id)

        if destination_account.account_status_id == 0:
            raise ValueError("destination account has been lock")
        new_balance_destination_account = destination_account.avaiable_balance +  data.amount
        

        event_data = TransferMoneyEventData.extend_pclass(
            pclass=data,
            _id = UUID_GENR(),
            new_balance_source_account=new_balance_source_account,
            new_balance_destination_account=new_balance_destination_account,
            transaction_type_id = transaction_type_data._id,
            transaction_fee = transaction_type_data.transaction_fee_amount,
        )
        
        
        
        return self.create_event(
            "money-transfered", target=self.aggroot, data=event_data
        )

    #create transaction
    async def do__create_transaction(
        self, data
    ) -> Event:
        
        event_data = CreateTransactionEventData.extend_pclass(
            pclass=None,
            _id=UUID_GENR(),
            transaction_date = datetime.utcnow(), 
            amount = data.amount,
            # transfer_content=data.transfer_content,
            transaction_type_id = data.transaction_type_id,
            atm_machine_id = data.atm_machine_id,
            source_account_id = data._id,
            # destination_account_id = data.destination_account_id,

        )

        return self.create_event(
            "transaction-created", target={'resource': 'transaction','identifier': event_data._id}, data=event_data
        )



    #update card
    async def do__update_card(
        self, data: UpdateCardData, id
    )->Event:
        event_data = UpdateCardEventData.extend_pclass(
            pclass=data,
            _id=id
        )
        return self.create_event(
            "card-updated", target={'resource': 'card','identifier': event_data._id}, data=event_data
        )
    #update pin
    async def do__update_pin(
        self, data: UpdatePinData, id
    )->Event:
        event_data = UpdateCardEventData.extend_pclass(
            pclass=data,
            _id=id
        )
        return self.create_event(
            "card-updated", target={'resource': 'card','identifier': event_data._id}, data=event_data
        )


    #update bank
    async def do__update_bank(
        self, data: UpdateBankData, id
    )->Event:
        event_data = UpdateBankEventData.extend_pclass(
            pclass=data,
            _id=id
        )
        return self.create_event(
            "bank-updated", target={'resource': 'bank','identifier': event_data._id}, data=event_data
        )

    #delete bank
    async def do__delete_bank(
        self
    )->Event:
        
        return self.create_event(
            "bank-deleted", target={'resource': 'bank','identifier': self.aggroot.identifier}, data=None
        )


  
    #update id card
    async def do__update_id_card(
        self, data: UpdateIdCardData, id
    )->Event:
        
        event_data = UpdateIdCardEventData.extend_pclass(
            pclass=data,
            _id=id
        )
       
        return self.create_event(
            "id-card-updated", target={'resource': 'id-card','identifier': event_data._id}, data=event_data
        )
        
    #delete id card
    async def do__delete_id_card(
        self
    )->Event:
        
        return self.create_event(
            "id-card-deleted", target={'resource': 'id-card','identifier': self.aggroot.identifier}, data=None
        )

    #create bank and transaction type 
    async def do__create_bank(
        self, data:CreateBankSystemData
    ) -> Event:
        data_query = {
             "where": {
                "name": data.name

        }
        }
        data_bank = await self.statemgr.find_one("bank", data_query=data_query)
        if data_bank:
            raise ValueError("Bank name already exists")

        event_data = CreateBankSystemEventData.extend_pclass(
            pclass=data,
            _id=UUID_GENR()
        )
        return self.create_event(
            "bank-created", target=self.aggroot, data=event_data
        )

    #update transaction type
    async def do__update_transaction_type_withdraw(
        self, data: UpdateTransactionTypeData, id
    )->Event:
        data_query = {
             "where": {
                "bank_id": id,
                "name": "withdraw"

        }
        }
        data_transaction_type = await self.statemgr.find_one("transaction-type", data_query=data_query)

        event_data = UpdateTransactionTypeEventData(
            _id=data_transaction_type._id,
            transaction_fee_amount=data.new_fee
        )
        return self.create_event(
            "transaction-type-updated", target={'resource': 'transaction-type','identifier': event_data._id}, data=event_data
        )
      
    #update transaction type
    async def do__update_transaction_type_deposit(
        self, data: UpdateTransactionTypeData, id
    )->Event:
        data_query = {
             "where": {
                "bank_id": id,
                "name": "deposit"

        }
        }
        data_transaction_type = await self.statemgr.find_one("transaction-type", data_query=data_query)
        
        event_data = UpdateTransactionTypeEventData(
            _id=data_transaction_type._id,
            transaction_fee_amount=data.new_fee
        )
        return self.create_event(
            "transaction-type-updated", target={'resource': 'transaction-type','identifier': event_data._id}, data=event_data
        )
    
    #update transaction type
    async def do__update_transaction_type_transfer(
        self, data: UpdateTransactionTypeData, id
    )->Event:
        data_query = {
             "where": {
                "bank_id": id,
                "name": "transfer"

        }
        }
        data_transaction_type = await self.statemgr.find_one("transaction-type", data_query=data_query)

        event_data = UpdateTransactionTypeEventData(
            _id=data_transaction_type._id,
            transaction_fee_amount=data.new_fee
        )
        return self.create_event(
            "transaction-type-updated", target={'resource': 'transaction-type','identifier': event_data._id}, data=event_data
        )


    #create atm machine
    async def do__create_atm_machine(
        self, data:CreateAtmMachineData
    ) -> Event:
        event_data = CreateAtmMachineEventData.extend_pclass(
            pclass=data,
            _id=UUID_GENR()
        )
        return self.create_event(
            "atm-machine-created", target=self.aggroot, data=event_data
        )

    #update atm machine
    async def do__update_atm_machine(
        self, data: UpdateAtmMachineData, id
    )->Event:
        event_data = UpdateAtmMachineEventData.extend_pclass(
            pclass=data,
            _id=id
        )
        return self.create_event(
            "atm-machine-updated", target={'resource': 'bank','identifier': event_data._id}, data=event_data
        )

    #delete atm machine
    async def do__delete_atm_machine(
        self
    )->Event:
        
        return self.create_event(
            "atm-machine-deleted", target={'resource': 'bank','identifier': self.aggroot.identifier}, data=None
        )