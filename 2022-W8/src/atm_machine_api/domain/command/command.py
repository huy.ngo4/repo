from fii_cqrs.command import Command, field

from atm_machine_api.domain.datadef import *

from fii import logger


from ..domain import AtmMachineDomain

#return class 
_entity = AtmMachineDomain.entity
_handler = AtmMachineDomain.command_handler

#aggproxy is an instance of the AtmMachineAggregate class
#cmd is an instance of the CreateBoilerplate class
#handler phải trả về event, reponse or Message 

@_entity("create-account")
class CreateAccount(Command):
    data = field(type=CreateAccountData)

    class Meta:
        resource = "account"
        tags = ["Account"]
        description = "Create new account"


@_handler(CreateAccount)
async def handle_create_account(aggproxy, cmd: CreateAccountData):
    event = await aggproxy.create_account(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
    )

#update account
@_entity("update-account")
class UpdateAccount(Command):
    data = field(type=UpdateAccountData)

    class Meta:
        resource = "account/{id}"
        tags = ["Account"]
        description = "Update account"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Account ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateAccount)
async def handle_update_account(aggproxy, cmd: UpdateAccountData):
    
    event = await aggproxy.update_account(cmd.data, cmd.aggroot.identifier)
    logger.warning("Oke" )
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)


# account actived
@_entity("activate-account")
class ActivateAccount(Command):
    pass

    class Meta:
        resource = "account/{id}"
        tags = ["Account"]
        description = "Activate account"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Account ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(ActivateAccount)
async def handle_activate_account(aggproxy, cmd: UpdateAccountData):
    
    event = await aggproxy.activate_account(cmd.aggroot.identifier)
    logger.warning("Oke" )
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)


# account locked
@_entity("deactivate-account")
class DeactivateAccount(Command):
    pass

    class Meta:
        resource = "account/{id}"
        tags = ["Account"]
        description = "Deactivate account"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Account ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(DeactivateAccount)
async def handle_deactivate_account(aggproxy, cmd: UpdateAccountData):
    
    event = await aggproxy.deactivate_account(cmd.aggroot.identifier)
    logger.warning("Oke" )
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)


#Delete account
@_entity("delete-account")
class DeleteAccount(Command):
    pass
    class Meta:
        resource = "account/{id}"
        tags = ["Account"]
        description = "Delete account"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Account ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(DeleteAccount)
async def handle_delete_account(aggproxy, cmd):

    event = await aggproxy.delete_account()
    logger.warning("OK")
    yield event


@_entity("create-customer")
class CreateCustomer(Command):
    data = field(type=NewCustomerData)

    class Meta:
        resource = "customer"
        tags = ["Customer"]
        description = "Create new customer"


@_handler(CreateCustomer)
async def handle_create_customer(aggproxy, cmd: NewCustomerData):
    event = await aggproxy.create_customer(cmd.data)
    yield event
    yield aggproxy.create_response(
        "customer-response", cmd, {"_id": event.data._id}
)


#Update customer
@_entity("update-customer")
class UpdateCustomer(Command):
    data = field(type=UpdateCustomerData)

    class Meta:
        resource = "customer/{id}"
        tags = ["Customer"]
        description = "Update customer"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Customer ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateCustomer)
async def handle_update_customer(aggproxy, cmd: UpdateCustomerData):
    event = await aggproxy.update_customer(cmd.data, cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "customer-response", cmd, {"_id": event.data._id}
    )



#delete customer
@_entity("delete-customer")
class DeleteCustomer(Command):
    pass
    class Meta:
        resource = "customer/{id}"
        tags = ["Customer"]
        description = "Delete customer"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Customer ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(DeleteCustomer)
async def handle_delete_customer(aggproxy, cmd):

    event = await aggproxy.delete_customer(cmd.aggroot.identifier)
    logger.warning("OK")
    yield event


#Withdraw money 
@_entity("withdraw-money")
class WithdrawMoney(Command):
    data = field(type=WithdrawMoneyData)

    class Meta:
        resource = "account"
        tags = ["transaction"]
        description = "Withdraw money"


@_handler(WithdrawMoney)
async def handle_withdraw_money(aggproxy, cmd: WithdrawMoneyData):

    event = await aggproxy.withdraw_money(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)



#Deposit money at ATM
@_entity("deposit-money")
class DepositMoney(Command):
    data = field(type=DepositMoneyData)

    class Meta:
        resource = "account"
        tags = ["transaction"]
        description = "Deposit money"


@_handler(DepositMoney)
async def handle_deposit_money(aggproxy, cmd: DepositMoneyData):

    event = await aggproxy.deposit_money(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)



#Transfer Money
@_entity("transfer-money")
class TransferMoney(Command):
    data = field(type=TransferMoneyData)

    class Meta:
        resource = "account"
        tags = ["transaction"]
        description = "Transfer money"
        

@_handler(TransferMoney)
async def handle_transfer_money(aggproxy, cmd: TransferMoneyData):

    event = await aggproxy.transfer_money(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)

#Transfer Money (Internet banking)
@_entity("internet-banking")
class InternetBanking(Command):
    data = field(type=InternetBankingData)

    class Meta:
        resource = "account"
        tags = ["transaction"]
        description = "Internet banking"
        

@_handler(InternetBanking)
async def handle_internet_banking(aggproxy, cmd: InternetBankingData):

    event = await aggproxy.internet_banking(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)



#update atm card
@_entity("update-card")
class UpdateCard(Command):
    data = field(type=UpdateCardData)

    class Meta:
        resource = "card/{id}"
        tags = ["ATM"]
        description = "Update atm card "
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Card ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateCard)
async def handle_update_card(aggproxy, cmd: UpdateCardData):

    event = await aggproxy.update_card(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)




#create bank and transaction type, withdraw money, deposit money, tranfer money

@_entity("create-bank")
class CreateBank(Command):
    data = field(type=CreateBankSystemData)

    class Meta:
        resource = "bank"
        tags = ["Bank"]
        description = "Create bank"


@_handler(CreateBank)
async def handle_create_bank(aggproxy, cmd: CreateBankSystemData):

    event = await aggproxy.create_bank(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)


#update bank
@_entity("update-bank")
class UpdateBank(Command):
    data = field(type=UpdateBankData)

    class Meta:
        resource = "bank/{id}"
        tags = ["Bank"]
        description = "Update bank "
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Bank ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateBank)
async def handle_update_bank(aggproxy, cmd: UpdateBankData):

    event = await aggproxy.update_bank(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "response", cmd, {"_id": event.data._id}
)

#delete bank
@_entity("delete-bank")
class DeleteBank(Command):
    pass
    class Meta:
        resource = "bank/{id}"
        tags = ["Bank"]
        description = "Delete bank"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Bank ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(DeleteBank)
async def handle_delete_bank(aggproxy, cmd):

    event = await aggproxy.delete_bank()
    logger.warning("OK")
    yield event




#update ID card
@_entity("update-id-card")
class UpdateIdCard(Command):
    data = field(type=UpdateIdCardData)
    #id customer
    class Meta:
        resource = "id-card/{id}"
        tags = ["Citizen Identity Card"]
        description = "Update ID card"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "ID Customer",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateIdCard)
async def handle_update_id_card(aggproxy, cmd: UpdateIdCardData):

    event = await aggproxy.update_id_card(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "response", cmd, {"_id": event.data._id}
)



#create atm-machine
@_entity("create-atm-machine")
class CreateAtmMachine(Command):
    data = field(type=CreateAtmMachineData)

    class Meta:
        resource = "atm-machine"
        tags = ["ATM"]
        description = "Create Atm Machine "


@_handler(CreateAtmMachine)
async def handle_create_atm_machine(aggproxy, cmd: CreateAtmMachineData):

    event = await aggproxy.create_atm_machine(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)

#update atm-machine
@_entity("update-atm-machine")
class UpdateAtmMachine(Command):
    data = field(type=UpdateAtmMachineData)

    class Meta:
        resource = "atm-machine/{id}"
        tags = ["ATM"]
        description = "Update Atm Machine "
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Atm Machine ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateAtmMachine)
async def handle_update_atm_machine(aggproxy, cmd: UpdateAtmMachineData):

    event = await aggproxy.update_atm_machine(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "response", cmd, {"_id": event.data._id}
)

#delete atm-machine
@_entity("delete-atm-machine")
class DeleteAtmMachine(Command):
    pass
    class Meta:
        resource = "atm-machine/{id}"
        tags = ["ATM"]
        description = "Delete Atm Machine"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Atm Machine ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(DeleteAtmMachine)
async def handle_delete_atm_machine(aggproxy, cmd):

    event = await aggproxy.delete_atm_machine()
    logger.warning("OK")
    yield event



#Update transaction type
@_entity("update-transaction-type-withdraw")
class UpdateTransactionType(Command):
    data = field(type=UpdateTransactionTypeData)

    class Meta:
        resource = "transaction-type/{id}"
        tags = ["Transaction type"]
        description = "Update withdraw fee"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Bank ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateTransactionType)
async def handle_update_transaction_type_withdraw(aggproxy, cmd: UpdateTransactionTypeData):

    event = await aggproxy.update_transaction_type_withdraw(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)


@_entity("update-transaction-type-deposit")
class UpdateTransactionType(Command):
    data = field(type=UpdateTransactionTypeData)

    class Meta:
        resource = "transaction-type/{id}"
        tags = ["Transaction type"]
        description = "Update deposit fee"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Bank ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]

@_handler(UpdateTransactionType)
async def handle_update_transaction_type_deposit(aggproxy, cmd: UpdateTransactionTypeData):

    event = await aggproxy.update_transaction_type_deposit(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)


@_entity("update-transaction-type-transfer")
class UpdateTransactionType(Command):
    data = field(type=UpdateTransactionTypeData)

    class Meta:
        resource = "transaction-type/{id}"
        tags = ["Transaction type"]
        description = "Update transfer fee"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Bank ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]
@_handler(UpdateTransactionType)
async def handle_update_transaction_type_transfer(aggproxy, cmd: UpdateTransactionTypeData):

    event = await aggproxy.update_transaction_type_transfer(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)






#create bank branch
@_entity("create-bank-branch")
class CreateBankBranch(Command):
    data = field(type=CreateBankBranchData)

    class Meta:
        resource = "bank-branch"
        tags = ["bank-branch"]
        description = "Create bank branch"


@_handler(CreateBankBranch)
async def handle_create_bank_branch(aggproxy, cmd: CreateBankBranchData):

    event = await aggproxy.create_bank_branch(cmd.data)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)

#update bank branch
@_entity("update-bank-branch")
class UpdateBankBranch(Command):
    data = field(type=UpdateBankBranchData)

    class Meta:
        resource = "bank-branch/{id}"
        tags = ["bank-branch"]
        description = "Update bank branch "
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "bank branch ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdateBankBranch)
async def handle_update_bank_branch(aggproxy, cmd: UpdateBankBranchData):

    event = await aggproxy.update_bank_branch(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)


#delete bank branch
@_entity("delete-bank-branch")
class DeleteBankBranch(Command):
    pass
    class Meta:
        resource = "bank-branch/{id}"
        tags = ["bank-branch"]
        description = "Delete bank branch"
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Bank Branch ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(DeleteBankBranch)
async def handle_delete_bank_branch(aggproxy, cmd):

    event = await aggproxy.delete_bank_branch()
    logger.warning("OK")
    yield event



#update atm card
@_entity("update-pin")
class UpdatePin(Command):
    data = field(type=UpdatePinData)

    class Meta:
        resource = "card/{id}"
        tags = ["transaction"]
        description = "Change Pin "
        parameters = [
            {
                "name": "id",
                "in": "path",   
                "description": "Card ID",
                "required": True,
                "schema": {
                    "type": "string",   
                },
            }
        ]


@_handler(UpdatePin)
async def handle_update_pin(aggproxy, cmd: UpdatePinData):

    event = await aggproxy.update_pin(cmd.data, id=cmd.aggroot.identifier)
    yield event
    yield aggproxy.create_response(
        "account-response", cmd, {"_id": event.data._id}
)