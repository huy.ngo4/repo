from pyrsistent import field
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring
from datetime import datetime

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE



class CreateIdCardEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    
    first_name = field(type=str, mandatory=True)   
    middle_name = field(type=str, mandatory=True)   
    last_name = field(type=str, mandatory=True)   
    date_of_birth = field(type=datetime, factory=parse_iso_datestring, mandatory=True)  
    number = field(type=str, mandatory=True) 
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True) 
    district = field(type=str, mandatory=True) 
    city = field(type=str, mandatory=True) 
    country = field(type=str, mandatory=True) 
    date_of_issue = field(type=datetime, factory=parse_iso_datestring, mandatory=True)
    place_of_issue = field(type=str, mandatory=True)  
    gender_id = field(type=str, mandatory=True)

class UpdateIdCardData(CommandData):
    
    first_name = field(type=str)   
    middle_name = field(type=str)   
    last_name = field(type=str)   
    date_of_birth = field(type=datetime, factory=parse_iso_datestring)  
    number = field(type=str) 
    address = field(type=str)   
    street = field(type=str) 
    district = field(type=str) 
    city = field(type=str) 
    country = field(type=str) 
    date_of_issue = field(type=datetime, factory=parse_iso_datestring)
    place_of_issue = field(type=str) 
    gender_id = field(type=str)

class UpdateIdCardEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    first_name = field(type=str)   
    middle_name = field(type=str)   
    last_name = field(type=str)   
    date_of_birth = field(type=datetime, factory=parse_iso_datestring)  
    number = field(type=str) 
    address = field(type=str)   
    street = field(type=str) 
    district = field(type=str) 
    city = field(type=str) 
    country = field(type=str) 
    date_of_issue = field(type=datetime, factory=parse_iso_datestring)
    place_of_issue = field(type=str)  
    gender_id = field(type=str)