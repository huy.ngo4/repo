from datetime import datetime
from pyrsistent import field
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE

class CreateAccountData(CommandData): 
    
    username = field(type=str, mandatory=True)   
    account_password = field(type=str, mandatory=True)       
    customer_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    #create atm card
    effective_date = field(type=datetime, factory=parse_iso_datestring)
    expiration_date = field(type=datetime, factory=parse_iso_datestring)


class CreateAccountCardEventData(CommandData): 
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    username = field(type=str, mandatory=True)   
    account_password = field(type=str, mandatory=True)       
    customer_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    #create atm card
    effective_date = field(type=datetime, factory=parse_iso_datestring)
    expiration_date = field(type=datetime, factory=parse_iso_datestring)
    
class CreateAccountEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    username = field(type=str, mandatory=True)   
    account_password = field(type=str, mandatory=True)   
    avaiable_balance = field(type=int, mandatory=True)   
    current_balance = field(type=int, mandatory=True)   
    account_type_id = field(type=str, mandatory=True)   
    account_status_id = field(type=int, mandatory=True)
    customer_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
   

class UpdateAccountData(CommandData):

    username = field(type=str)   
    account_password = field(type=str)   
     

class UpdateAccountEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid)

    username = field(type=str)   
    account_password = field(type=str)   
    account_status_id = field(type=int)
    avaiable_balance = field(type=int)   
    current_balance = field(type=int)  
 


class CreateAccountStatusData(CommandData):
    
    description = field(type=str, mandatory=True)   
    
class CreateAccountStatusEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    description = field(type=str, mandatory=True)   