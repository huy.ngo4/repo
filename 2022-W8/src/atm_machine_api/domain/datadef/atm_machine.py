
from datetime import datetime
from pyrsistent import field
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE

class CreateAtmMachineData(CommandData):
    
    amount = field(type=int, mandatory=True)   
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True)   
    district = field(type=str, mandatory=True)   
    city = field(type=str, mandatory=True)   
    country = field(type=str, mandatory=True)  
    bank_id =  field(type=UUID_TYPE, factory=to_uuid, mandatory=True)


class CreateAtmMachineEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    amount = field(type=int, mandatory=True)   
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True)   
    district = field(type=str, mandatory=True)   
    city = field(type=str, mandatory=True)   
    country = field(type=str, mandatory=True)   
    bank_id =  field(type=UUID_TYPE, factory=to_uuid,mandatory=True)

class UpdateAtmMachineData(CommandData):
    amount = field(type=int)   
    address = field(type=str)   
    street = field(type=str)   
    district = field(type=str)   
    city = field(type=str)   
    country = field(type=str)   
    bank_id =  field(type=UUID_TYPE, factory=to_uuid)

class UpdateAtmMachineEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    amount = field(type=int)   
    address = field(type=str)   
    street = field(type=str)   
    district = field(type=str)   
    city = field(type=str)   
    country = field(type=str)   
    bank_id =  field(type=UUID_TYPE, factory=to_uuid)