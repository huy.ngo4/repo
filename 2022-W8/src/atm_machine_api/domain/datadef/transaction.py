from datetime import datetime
from pyrsistent import field
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE


class WithdrawMoneyData(CommandData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    amount = field(type=int, mandatory=True)
    # transaction_type_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    atm_machine_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

class DepositMoneyData(CommandData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    amount = field(type=int, mandatory=True) 
    # transaction_type_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    atm_machine_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)


class TradingAccountEventData(CommandData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    amount = field(type=int, mandatory=True)
    transaction_fee = field(type=int, mandatory=True)
    transaction_type_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    atm_machine_id = field(type=UUID_TYPE, factory=to_uuid)
    avaiable_balance = field(type=int, mandatory=True) 
    amount_atm_machine_new = field(type=int) 

#version 1
class TransferMoneyData(CommandData):
    
    amount = field(type=int, mandatory=True) 
    transfer_content = field(type=str) 
    atm_machine_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    source_account_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    destination_account_id = field(type=UUID_TYPE, factory=to_uuid)

#internet banking
class InternetBankingData(CommandData):
    
    amount = field(type=int, mandatory=True) 
    transfer_content = field(type=str) 
    source_account_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    destination_account_id = field(type=UUID_TYPE, factory=to_uuid)


class TransferMoneyEventData(CommandData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    amount = field(type=int, mandatory=True) 
    transfer_content = field(type=str) 
    transaction_type_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    atm_machine_id = field(type=UUID_TYPE, factory=to_uuid)
    source_account_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    destination_account_id = field(type=UUID_TYPE, factory=to_uuid)
    new_balance_source_account = field(type=int, mandatory=True)
    new_balance_destination_account = field(type=int, mandatory=True)
    amount_atm_machine_new = field(type=int)
    transaction_fee = field(type=int, mandatory=True)

class CreateTransactionData(CommandData):
   

    # transaction_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True) 
    amount = field(type=int, mandatory=True) 
    transfer_content = field(type=str) 
    transaction_type_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    atm_machine_id = field(type=UUID_TYPE, factory=to_uuid)
    source_account_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    destination_account_id = field(type=UUID_TYPE, factory=to_uuid)

class CreateTransactionEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    transaction_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True) 
    amount = field(type=int, mandatory=True) 
    transaction_fee = field(type=int, mandatory=True) 
    transfer_content = field(type=str) 
    transaction_type_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    atm_machine_id = field(type=UUID_TYPE, factory=to_uuid)
    source_account_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    destination_account_id = field(type=UUID_TYPE, factory=to_uuid)


class CreateTransactionTypeEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    
    transaction_fee_amount = field(type=int, mandatory=True) 
    transaction_limit_amount = field(type=int, mandatory=True) 
    name = field(type=str, mandatory=True) 
    description = field(type=str) 
    bank_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    
class UpdateTransactionTypeData(EventData):
    
    new_fee = field(type=int) 
    new_limit_amount = field(type=int)
    
class UpdateTransactionTypeEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    
    transaction_fee_amount = field(type=int)
    transaction_limit_amount = field(type=int) 