from pyrsistent import field
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring
from datetime import datetime

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE

class CreateCardData(CommandData):
    
    pin = field(type=str, mandatory=True)    
    bank_account_id = field(type=UUID_TYPE,factory=to_uuid, mandatory=True) 
    effective_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True)
    expiration_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True)

class CreateCardEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    pin = field(type=str, mandatory=True)    
    bank_account_id = field(type=UUID_TYPE,factory=to_uuid, mandatory=True) 
    effective_date = field(type=datetime, factory=parse_iso_datestring)
    expiration_date = field(type=datetime, factory=parse_iso_datestring)

class UpdateCardData(CommandData):
    
    pin = field(type=str)    
    effective_date = field(type=datetime, factory=parse_iso_datestring)
    expiration_date = field(type=datetime, factory=parse_iso_datestring)
   

class UpdatePinData(CommandData):
    
    pin = field(type=str)    
   
class UpdateCardEventData(CommandData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    pin = field(type=str)    
    effective_date = field(type=datetime, factory=parse_iso_datestring)
    expiration_date = field(type=datetime, factory=parse_iso_datestring)