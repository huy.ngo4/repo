
from datetime import datetime
from pyrsistent import field
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE

class NewCustomerData(CommandData):
    first_name = field(type=str, mandatory=True)   
    middle_name = field(type=str, mandatory=True)   
    last_name = field(type=str, mandatory=True)   
    date_of_birth = field(type=datetime, factory=parse_iso_datestring, mandatory=True)   
    email = field(type=str, mandatory=True)    
    phone_number = field(type=str, mandatory=True) 
    
    bank_id = field(type=UUID_TYPE,factory=to_uuid)
    #Nơi ở hiện tai
    current_address = field(type=str, mandatory=True)
    current_stresst = field(type=str, mandatory=True)
    current_district = field(type=str, mandatory=True)
    current_city = field(type=str, mandatory=True)
    current_country = field(type=str, mandatory=True)
   
    #account default
     
    username =  field(type=str, mandatory=True)
    account_password =  field(type=str, mandatory=True)
   
    

    #citizen identity card
    address = field(type=str, mandatory=True)
    street = field(type=str, mandatory=True)
    district = field(type=str, mandatory=True)
    city = field(type=str, mandatory=True)
    country = field(type=str, mandatory=True)
    date_of_issue = field(type=datetime, factory=parse_iso_datestring, mandatory=True)
    place_of_issue = field(type=str, mandatory=True)
    number = field(type=str, mandatory=True)
    gender_id = field(type=str, mandatory=True)

    #card
   
    effective_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True)   
    expiration_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True)  

    
class NewCustomerEventData(EventData):
    _id = field(type=UUID_TYPE,factory=to_uuid) #new
    
    first_name = field(type=str, mandatory=True)   
    middle_name = field(type=str, mandatory=True)   
    last_name = field(type=str, mandatory=True)   
    date_of_birth = field(type=datetime, factory=parse_iso_datestring, mandatory=True)   
    email = field(type=str, mandatory=True)    
    phone_number = field(type=str, mandatory=True) 
    
    bank_id = field(type=UUID_TYPE,factory=to_uuid)
    #Nơi ở hiện tai
    is_primary = field(type=bool, mandatory=True) #new

    current_address = field(type=str, mandatory=True)
    current_stresst = field(type=str, mandatory=True)
    current_district = field(type=str, mandatory=True)
    current_city = field(type=str, mandatory=True)
    current_country = field(type=str, mandatory=True)
   
    #account default
    
    username =  field(type=str, mandatory=True)
    account_password =  field(type=str, mandatory=True)
 
    

     #citizen identity card
    address = field(type=str, mandatory=True)
    street = field(type=str, mandatory=True)
    district = field(type=str, mandatory=True)
    city = field(type=str, mandatory=True)
    country = field(type=str, mandatory=True)
    date_of_issue = field(type=datetime, factory=parse_iso_datestring, mandatory=True)
    place_of_issue = field(type=str, mandatory=True)
    number = field(type=str, mandatory=True)
    gender_id = field(type=str, mandatory=True)

    #card
    pin = field(type=str, mandatory=True)
    effective_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True)   
    expiration_date = field(type=datetime, factory=parse_iso_datestring, mandatory=True)   
    

class CreateCustomerData(CommandData):
    
    first_name = field(type=str, mandatory=True)   
    middle_name = field(type=str, mandatory=True)   
    last_name = field(type=str, mandatory=True)   
    email = field(type=str, mandatory=True)    
    phone_number = field(type=str, mandatory=True) 

    id_number = field(type=UUID_TYPE,factory=to_uuid)
    bank_id = field(type=UUID_TYPE,factory=to_uuid, mandatory=True)


class CreateCustomerEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    first_name = field(type=str, mandatory=True)   
    middle_name = field(type=str, mandatory=True)   
    last_name = field(type=str, mandatory=True)   
    date_of_birth = field(type=datetime, factory=parse_iso_datestring, mandatory=True) 
    email = field(type=str, mandatory=True)    
    phone_number = field(type=str, mandatory=True) 
    id_number = field(type=UUID_TYPE,factory=to_uuid, mandatory=True)
    bank_id = field(type=UUID_TYPE,factory=to_uuid, mandatory=True)


class UpdateCustomerData(CommandData):
    first_name = field(type=str)   
    middle_name = field(type=str)   
    last_name = field(type=str)   
    phone_number = field(type=str) 
    email = field(type=str)    
    

class UpdateCustomerEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid)

    first_name = field(type=str)   
    middle_name = field(type=str)   
    last_name = field(type=str)   
    email = field(type=str)    
    phone_number = field(type=str) 
   

class DeleteCustomerData(CommandData):
    _id = field(type=UUID_TYPE, factory=to_uuid)

    

class DeleteCustomerEventData(EventData):
    # _id = field(type=UUID_TYPE, factory=to_uuid)
    pass
    

#customer address

class CreateCustomerAddressEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True) 
    district = field(type=str, mandatory=True) 
    city = field(type=str, mandatory=True) 
    country = field(type=str, mandatory=True) 
    customer_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    is_primary = field(type=bool, mandatory=True)
    