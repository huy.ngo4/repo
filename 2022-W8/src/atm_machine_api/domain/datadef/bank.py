
from datetime import datetime
from pyrsistent import field
from sanic_validator.pyrsistent.factory import to_uuid, parse_iso_datestring

from fii_cqrs.command import CommandData
from fii_cqrs.event import EventData
from fii_cqrs.identifier import UUID_TYPE

class CreateBankSystemData(CommandData):
    
    name = field(type=str, mandatory=True)   
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True)   
    district = field(type=str, mandatory=True)   
    city = field(type=str, mandatory=True)   
    country = field(type=str, mandatory=True)
    minimum_balance=field(type=int,mandatory=True)
    #transaction fee
    withdrawal_fee = field(type=int, mandatory=True) 
    deposit_fee = field(type=int, mandatory=True)
    transfer_fee = field(type=int, mandatory=True)
    #transaction limit
    withdrawal_limit = field(type=int, mandatory=True) 
    deposit_limit = field(type=int, mandatory=True)
    transfer_limit = field(type=int, mandatory=True)


class CreateBankSystemEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    name = field(type=str, mandatory=True)   
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True)   
    district = field(type=str, mandatory=True)   
    city = field(type=str, mandatory=True)   
    country = field(type=str, mandatory=True)   
    minimum_balance = field(type=int, mandatory=True)

    #transaction fee
    withdrawal_fee = field(type=int, mandatory=True) 
    deposit_fee = field(type=int, mandatory=True)
    transfer_fee = field(type=int, mandatory=True)
    #transaction limit
    withdrawal_limit = field(type=int, mandatory=True) 
    deposit_limit = field(type=int, mandatory=True)
    transfer_limit = field(type=int, mandatory=True)

class CreateBankEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    name = field(type=str, mandatory=True)   
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True)   
    district = field(type=str, mandatory=True)   
    city = field(type=str, mandatory=True)   
    country = field(type=str, mandatory=True) 
    minimum_balance = field(type=int, mandatory=True)
class UpdateBankData(CommandData):
    name = field(type=str)   
    address = field(type=str)   
    street = field(type=str)   
    district = field(type=str)   
    city = field(type=str)   
    country = field(type=str)   
    

class UpdateBankEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    name = field(type=str)   
    address = field(type=str)   
    street = field(type=str)   
    district = field(type=str)   
    city = field(type=str)   
    country = field(type=str)   

class CreateBankBranchData(CommandData):
   
    bank_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    name = field(type=str, mandatory=True)   
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True)   
    district = field(type=str, mandatory=True)   
    city = field(type=str, mandatory=True)   
    country = field(type=str, mandatory=True) 

class CreateBankBranchEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    bank_id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)
    name = field(type=str, mandatory=True)   
    address = field(type=str, mandatory=True)   
    street = field(type=str, mandatory=True)   
    district = field(type=str, mandatory=True)   
    city = field(type=str, mandatory=True)   
    country = field(type=str, mandatory=True) 
    
class UpdateBankBranchData(CommandData):
   
    bank_id = field(type=UUID_TYPE, factory=to_uuid)
    name = field(type=str)   
    address = field(type=str)   
    street = field(type=str)   
    district = field(type=str)   
    city = field(type=str)   
    country = field(type=str) 

class UpdateBankBranchEventData(EventData):
    _id = field(type=UUID_TYPE, factory=to_uuid, mandatory=True)

    bank_id = field(type=UUID_TYPE, factory=to_uuid)
    name = field(type=str)   
    address = field(type=str)   
    street = field(type=str)   
    district = field(type=str)   
    city = field(type=str)   
    country = field(type=str) 