from sanic_cqrs import (
    PostgresCommandStore,
    PostgresContextStore,
    PostgresEventStore,
)
from sanic_cqrs.domain import GinoDomain

from atm_machine_api import __version__, config
from .aggregate import Aggregate


class AtmMachineDomain(GinoDomain):
    __namespace__ = config.ATM_MACHINE_API_DOMAIN
    __aggregate__ = Aggregate
    __revision__ = 1
    __version__ = __version__

    EventStore = PostgresEventStore
    CommandStore = PostgresCommandStore
    ContextStore = PostgresContextStore
