
from sanic_query.resource import register_resource

from atm_machine_api import config, logger
from . import account
from . import customer
from . import citizen_identity_card
from . import card

from . import transaction_type
from . import bank

from . import transaction
from . import view
from . import atm_machine


def configure_query(app):
    register_view = register_resource(
        app,
        api_endpoint=config.ATM_MACHINE_API_ENDPOINT,
    
        url_prefix=config.ATM_MACHINE_API_DOMAIN,
        default_schema=config.ATM_MACHINE_SCHEMA,
        auth_decorator=False,
    )

    register_view(account.AccountQuery)
    register_view(customer.CustomerQuery)
    register_view(citizen_identity_card.CitizenIdentityCard)
    register_view(card.CardQuery)
    register_view(transaction_type.TransactionTypeQuery)
    register_view(bank.BankQuery)
    register_view(transaction.TransactionLogQuery)
    register_view(view.ViewAccountQuery)
    register_view(view.ViewTransactionQuery)
    register_view(account.AccountStatusQuery)
    register_view(view.ViewAccountActivedQuery)
    register_view(atm_machine.AtmMachineQuery)
    register_view(account.BalanceQuery)
    register_view(bank.BankBranchQuery)
    register_view(view.DatapackUserQuery)

    
    