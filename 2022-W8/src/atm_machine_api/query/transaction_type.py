from sanic_query import field as f
from .basequeryresource import BaseQueryResource

class TransactionTypeQuery(BaseQueryResource):
    __table__= "transaction--type"

    _id = f.UUIDField("ID", identifier=True)
    name = f.StringField("name")
    transaction_fee_amount = f.StringField("transaction_fee_amount")
    description = f.StringField("description")
    bank_id = f.UUIDField("bank_account_id")
   
    class Meta:
        tags = ["Transaction"]
        description = "Get Transaction-type data"

