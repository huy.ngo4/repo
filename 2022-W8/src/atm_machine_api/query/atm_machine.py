from sanic_query import field as f
from .basequeryresource import BaseQueryResource

class AtmMachineQuery(BaseQueryResource):
    __table__= "atm--machine"

    _id = f.UUIDField("ID", identifier=True)
    amount = f.StringField("amount")
    address = f.StringField("address")
    street = f.StringField("street")
    district = f.StringField("district")
    city = f.StringField("city")
    country = f.StringField("country")

   
    class Meta:
        tags = ["ATM"]
        description = "Get ATM Machine data"

