from sanic_query import field as f
from .basequeryresource import BaseQueryResource

class CardQuery(BaseQueryResource):
    __table__= "atm--card"

    _id = f.UUIDField("ID", identifier=True)
    pin = f.StringField("pin")
    effective_date = f.StringField("effective_date")
    expiration_date = f.StringField("expiration_date")
    bank_account_id = f.UUIDField("bank_account_id")
   
    class Meta:
        tags = ["ATM"]
        description = "Get Atm Card data"

