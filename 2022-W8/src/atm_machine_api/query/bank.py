from sanic_query import field as f
from .basequeryresource import BaseQueryResource

class BankQuery(BaseQueryResource):
    __table__= "bank"

    _id = f.UUIDField("ID", identifier=True)
    name = f.StringField("name")
    address = f.StringField("address")
    street = f.StringField("street")
    district = f.StringField("district")
    city = f.StringField("city")
    country = f.StringField("country")

   
    class Meta:
        tags = ["Bank"]
        description = "Get Bank data"

class BankBranchQuery(BaseQueryResource):
    __table__= "bank--branch"

    _id = f.UUIDField("ID", identifier=True)
    bank_id = f.UUIDField("bank_id")
    name = f.StringField("name")
    address = f.StringField("address")
    street = f.StringField("street")
    district = f.StringField("district")
    city = f.StringField("city")
    country = f.StringField("country")

   
    class Meta:
        tags = ["Bank"]
        description = "Get Bank branch data"

