from sanic_query import field as f
from .basequeryresource import BaseQueryResource

class CustomerQuery(BaseQueryResource):
    __table__= "customer"

    _id = f.StringField("ID", identifier=True)
    first_name = f.StringField("First Name")
    middle_name = f.StringField("middle_name")
    last_name = f.StringField("last_name")
    date_of_birth = f.StringField("date_of_birth")
    email = f.StringField("email")
    phone_number = f.StringField("phone_number")
    id_number = f.UUIDField("ID_number")
    bank_id = f.UUIDField("bank_id")

    class Meta:
        tags = ["Customer"]
        description = "Get customer data"

