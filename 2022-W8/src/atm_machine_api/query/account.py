from sanic_query import field as f
from .basequeryresource import BaseQueryResource
from sanic_query.resource import QueryResource

class AccountQuery(BaseQueryResource):
    __table__= "bank--account"

    

    _id = f.StringField("ID", identifier=True)
    username = f.StringField("Username")
    account_password = f.StringField("account_password")
    avaiable_balance = f.StringField("avaiable_balance")
    current_balance = f.StringField("current_balance")
    account_type_id = f.StringField("account_type_id")
    account_status_id = f.UUIDField("account_status_id")
    customer_id = f.UUIDField("customer_id")
    

    class Meta:
        tags = ["Account"]
        description = "Get account data"

class AccountStatusQuery(BaseQueryResource):
    __table__= "view--account--status"
    __endpoint__= "account_status"
    

    _id = f.StringField("ID", identifier=True)
    description = f.StringField("description")
    username = f.StringField("description")
    
    class Meta:
        tags = ["Account"]
        description = "Get account status data"

class BalanceQuery(BaseQueryResource):
    __table__= "bank--account"
    __endpoint__= "balance"
    

    _id = f.StringField("ID", identifier=True)
    avaiable_balance = f.StringField("avaiable_balance")
    
    class Meta:
        tags = ["transaction"]
        description = "Get balance data"
