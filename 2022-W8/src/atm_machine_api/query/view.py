from .basequeryresource import BaseQueryResource
from sanic_query import field as f

class ViewAccountQuery(BaseQueryResource):
    __table__ = "view--account"
    __endpoint__ = "view_account"

    _id = f.UUIDField("ID", identifier=True)
    username = f.StringField("Username")
    account_password = f.StringField("account_password")
    avaiable_balance = f.StringField("avaiable_balance")
    name_bank = f.StringField("name")
    first_name = f.StringField("First Name")
    middle_name = f.StringField("middle_name")
    last_name = f.StringField("last_name")
    date_of_birth = f.StringField("date_of_birth")
    phone_number = f.StringField("phone_number")
    email = f.StringField("email")

    class Meta:
        tags = ["Customer"]
        description = "Get customer infomation data"

class ViewTransactionQuery(BaseQueryResource):
    __table__ = "view--transaction"
    __endpoint__ = "view_transaction"

    _id = f.StringField("ID", identifier=True)
    transaction_date = f.StringField("transaction_date")
    amount = f.StringField("amount")
    transfer_content = f.StringField("transfer_content")
    atm_machine_id = f.StringField("atm_machine_id")
    source_account_id = f.StringField("source_account_id")
    fn_of_source_account = f.StringField(" first name of customer 1")
    md_of_source_account = f.StringField("middle name of customer 1")
    ls_of_source_account = f.StringField("last name of customer 1")
    avaiable_balance_of_source_account = f.StringField("avaiable balance of source account")

    destination_account_id = f.StringField("destination_account_id")
    fn_of_destination_account = f.StringField("first name of customer 2")
    md_of_destination_account = f.StringField("middle name of customer 2")
    ls_of_destination_account = f.StringField("last name of customer 2")
    avaiable_balance_of_destination_account = f.StringField("avaiable balance of destination account")


    class Meta:
        tags = ["Customer"]
        description = "Get Transaction data"

class ViewAccountActivedQuery(BaseQueryResource):
    __table__ = "account--actived"
    __endpoint__ = "account_actived"

    _id = f.StringField("ID", identifier=True)
    description = f.StringField("Description")
    username = f.StringField("usename")
    

    class Meta:
        tags = ["Account"]
        description = "Get Account Actived data"


class DatapackUserQuery(BaseQueryResource):
    __table__ = "bank--account"
    __endpoint__ = "customer/<username>/account/"
    

    _id = f.StringField("ID", identifier=True)
    username = f.StringField("Username")
    account_password = f.StringField("account_password")
    avaiable_balance = f.StringField("avaiable_balance")
    current_balance = f.StringField("current_balance")
    account_type_id = f.StringField("account_type_id")
    account_status_id = f.UUIDField("account_status_id")
    customer_id = f.UUIDField("customer_id")

    class Meta:
        tags = ["Account"]
        description = "Get account by username"
        parameters = [
            {
                "name": "username",
                "in": "path",
                "description": "username",
                "required": True,
                "schema": {"type": "string"},
            },
           
        ]
    
    @classmethod
    def base_query(cls, parsed_query, user=None):
        return {
            "username": parsed_query.url_params["username"],
        }

