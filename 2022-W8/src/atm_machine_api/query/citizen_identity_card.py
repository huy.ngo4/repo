from sanic_query import field as f
from .basequeryresource import BaseQueryResource

class CitizenIdentityCard(BaseQueryResource):
    __table__= "citizen--identity--card"

    _id = f.StringField("ID", identifier=True)
    address = f.StringField("address")
    street = f.StringField("street")
    district = f.StringField("district")
    city = f.StringField("city")
    country = f.StringField("country")
    date_of_issue = f.StringField("date_of_issue")
    place_of_issue = f.StringField("place_of_issue")
    gender_id = f.UUIDField("gender_id")
    
    class Meta:
        tags = ["Citizen Identity Card"]
        description = "Get citizen_identity_card data"
