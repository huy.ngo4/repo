from sanic_query import field as f
from .basequeryresource import BaseQueryResource

class TransactionLogQuery(BaseQueryResource):
    __table__= "transaction--log"

    _id = f.StringField("ID", identifier=True)
    transaction_date = f.StringField("transaction_date")
    amount = f.StringField("amount")
    transfer_content = f.StringField("transfer_content")
    transaction_fee = f.StringField("transaction_fee")
    transaction_type_id  = f.StringField("transaction_type_id")
    atm_machine_id = f.StringField("atm_machine_id")
    source_account_id = f.UUIDField("source_account_id")
    destination_account_id = f.UUIDField("destination_account_id")

   
    class Meta:
        tags = ["Transaction"]
        description = "Get Transaction Log data"
