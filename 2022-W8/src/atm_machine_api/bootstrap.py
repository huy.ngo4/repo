from sanic_cqrs.helper import create_app
from sanic_kcauth import configure_kcauth


import atm_machine_api
from atm_machine_api.domain import configure_domain
from atm_machine_api.query import configure_query

app = create_app(atm_machine_api)

configure_kcauth(app)
configure_domain(app)
configure_query(app)
