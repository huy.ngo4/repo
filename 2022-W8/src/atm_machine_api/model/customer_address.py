from tokenize import Number
from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class CustomerAddressModel(GinoBaseModel):
    __tablename__ = "customer--address"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    address = db.Column(db.String)  
    street = db.Column(db.String)  
    district = db.Column(db.String)  
    city = db.Column(db.String)  
    country = db.Column(db.String)  

    is_primary = db.Column(db.Boolean)
    customer_id = db.Column(UUID)
       
