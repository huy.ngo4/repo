from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class CustomerModel(GinoBaseModel):
    __tablename__ = "customer"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    first_name = db.Column(db.String) 
    middle_name = db.Column(db.String)  
    last_name = db.Column(db.String)    
    email = db.Column(db.String)  
    phone_number = db.Column(db.String)
    id_number = db.Column(UUID)
    date_of_birth = db.Column(db.DateTime)    
    bank_id = db.Column(UUID)