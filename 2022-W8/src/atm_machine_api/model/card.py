from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class CardModel(GinoBaseModel):
    __tablename__ = "atm--card"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    pin = db.Column(db.String)
    effective_date = db.Column(db.DateTime)
    expiration_date = db.Column(db.DateTime)
    bank_account_id = db.Column(UUID)