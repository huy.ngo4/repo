from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class TransactionTypeModel(GinoBaseModel):
    __tablename__ = "transaction--type"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    name = db.Column(db.String) 
    transaction_fee_amount = db.Column(db.Integer) 
    transaction_limit_amount = db.Column(db.Integer) 
    description = db.Column(db.String) 
    bank_id = db.Column(UUID)