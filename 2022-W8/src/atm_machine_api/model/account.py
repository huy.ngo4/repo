from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class AccountModel(GinoBaseModel):
    __tablename__ = "bank--account"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
   
    username = db.Column(db.String) 
    account_password = db.Column(db.String)  
    avaiable_balance = db.Column(db.Integer)    
    current_balance = db.Column(db.Integer)    
    account_type_id = db.Column(db.String)   
    account_status_id = db.Column(db.Integer)
    customer_id = db.Column(UUID)
    