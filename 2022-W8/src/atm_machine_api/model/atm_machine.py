from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class AtmMachineModel(GinoBaseModel):
    __tablename__ = "atm--machine"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    amount = db.Column(db.Integer) 
    address = db.Column(db.String)  
    street = db.Column(db.String)    
    district = db.Column(db.String)  
    city = db.Column(db.String)   
    country = db.Column(db.String) 
    bank_id = db.Column(UUID) 