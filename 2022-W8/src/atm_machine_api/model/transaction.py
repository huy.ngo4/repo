from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class TransactionModel(GinoBaseModel):
    __tablename__ = "transaction--log"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    transaction_date = db.Column(db.DateTime)
    amount = db.Column(db.Integer)   
    transaction_fee = db.Column(db.Integer)   
    transfer_content = db.Column(db.String)   
    transaction_type_id = db.Column(UUID)   
    atm_machine_id = db.Column(UUID)   
    source_account_id = db.Column(UUID)   
    destination_account_id = db.Column(UUID)   
