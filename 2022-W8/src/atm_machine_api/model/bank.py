from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class BankModel(GinoBaseModel):
    __tablename__ = "bank"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    name = db.Column(db.String) 
    address = db.Column(db.String)  
    street = db.Column(db.String)    
    district = db.Column(db.String)  
    city = db.Column(db.String)   
    country = db.Column(db.String) 
    minimum_balance = db.Column(db.Integer)

class BankBranchModel(GinoBaseModel):
    __tablename__ = "bank--branch"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    bank_id = db.Column(UUID)
    name = db.Column(db.String) 
    address = db.Column(db.String)  
    street = db.Column(db.String)    
    district = db.Column(db.String)  
    city = db.Column(db.String)   
    country = db.Column(db.String) 

    