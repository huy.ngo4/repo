from tokenize import Number
from sqlalchemy.dialects.postgresql import UUID

from fii_cqrs.backend.gino import GinoBaseModel, db

from atm_machine_api import config


class IdCardModel(GinoBaseModel):
    __tablename__ = "citizen--identity--card"
    __table_args__ = dict(schema=config.ATM_MACHINE_SCHEMA)

    _id = db.Column(UUID, primary_key=True)
    
    date_of_birth = db.Column(db.DateTime)    
    first_name = db.Column(db.String) 
    middle_name = db.Column(db.String)  
    last_name = db.Column(db.String) 
    number = db.Column(db.String)  
    address = db.Column(db.String)  
    street = db.Column(db.String)  
    district = db.Column(db.String)  
    city = db.Column(db.String)  
    country = db.Column(db.String)  
    date_of_issue = db.Column(db.DateTime)
    place_of_issue = db.Column(db.String)   
    gender_id = db.Column(db.String)
