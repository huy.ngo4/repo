# Sanic CQRS

### Query by using resource

- Có thể sử dụng resource để query dữ liệu bằng cách sử dụng các hàm có sẵn của thư viện như: fetch, find, find_one,..

### `Fecth` :

- Mục đích sử dụng: Có được \_id của 1 record thuộc 1 table nào đó và muốn lấy tất cả thông tin của cả record đó
  --> Lưu ý những thông tin muốn lấy phải được khai báo trong resource và model
- Gọi bằng 1 resource: `MemeberResource.fetch(_id, **kwargs) -> CqrsResource`
- Gọi từ aggregate: `self.statemgr.fetch(resource, _id, **kwargs) -> CqrsResource`
- Gọi từ command: `aggproxy.state.fetch(resource, _id, **kwargs) -> CqrsResource`
  --> Đây là 1 async function nên khi gọi phải sử dụng await

### `Find` :

- Mục đích sử dụng: Có được 1 giá trị của 1 field thuộc 1 table nào đó và muốn lấy tất cả thông tin của tất cả record thỏa mãn yêu cầu
  --> Lưu ý những thông tin muốn lấy phải được khai báo trong resource và model
- `data_query = { "where": {"field_name:operator":"value"} }`
- Gọi bằng 1 resource: `MemeberResource.find(data_query, **kwargs) -> list(CqrsResource)`
- Gọi từ aggregate: `self.statemgr.find(resource, data_query, **kwargs) -> list(CqrsResource)`
- Gọi từ command: `aggproxy.state.find(resource, data_query, **kwargs) -> list(CqrsResource)`

### `Find_one` :

- Mục đích sử dụng: Có được 1 giá trị của 1 field thuộc 1 table nào đó và muốn lấy tất cả thông tin của tất cả record thỏa mãn yêu cầu
  --> Lưu ý những thông tin muốn lấy phải được khai báo trong resource và model
- `data_query = { "where": {"field_name:operator":"value"} }`
- Gọi bằng 1 resource: `MemeberResource.find_one(data_query, **kwargs) -> CqrsResource`
- Gọi từ aggregate: `self.statemgr.find_one(resource, data_query, **kwargs) -> CqrsResource`
- Gọi từ command: `aggproxy.state.find_one(resource, data_query, **kwargs) -> CqrsResource`

### `Select` :

- Mục đích sử dụng: Có được 1 giá trị của 1 field thuộc 1 table nào đó và muốn lấy tất cả thông tin của tất cả record thỏa mãn yêu cầu
  --> Lưu ý những thông tin muốn lấy phải được khai báo trong resource và model
- `data_query = { "select" : ["field1", "field2",...] ,"where": {"field_name:operator":"value"} }`
- Gọi bằng 1 resource: `MemeberResource.select(data_query, **kwargs) -> list(CqrsResource)`
- Gọi từ aggregate: `self.statemgr.select(resource, data_query, **kwargs) -> list(CqrsResource)`
- Gọi từ command: `aggproxy.state.select(resource, data_query, **kwargs) -> list(CqrsResource)`

### `Fetch_from-state` :

- Thực chất hàm này chính là hàm fetch phía trên. Khi ta fetch 1 record thì đầu tiên nó sẽ kiểm tra trong state - tức bộ nhớ hiện tại
  đã có reocrd thỏa mãn hay chưa, nếu chưa nó sẽ xuống database để tìm, sau đó sẽ lưu record này vào state.
- Cách gọi như hàm fetch
  --> Chúng ta thường dùng fetch

### `Fetch_from_db` :

- Đây là hàm mà fetch sẽ gọi khi trong state chưa có record thỏa mãn yêu cầu thì sẽ gọi hàm này để lấy từ database
- Hàm này thường được sử dụng khi user fetch data từ database chứ không phải từ state
