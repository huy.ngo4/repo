https://engineering.tiki.vn/thiet-ke-he-thong-quan-ly-danh-muc-san-pham-trong-he-thong-ecommerce/

https://postgrest.org/en/stable/api.html
https://api.dn01-tecq.gotecq.net/~dev/#user-management

not null
ssl là gì


# Các bước cấu hình.

#link foder env gotectq-intership-boilerplate 
#cấu hình dommain hosts
#chạy docker postgrest, check connect
#create schema, chạy lại docker-compose up postgrest để nhận lại schema
#đăng nhập https://api.local.gotecq.net/gotecq.boilerplate/swagger/index.html 
	Trong tmpl/openapi.yml thêm server của mình
		- url: https://api.local.gotecq.net/
    		- description: Localhost server	
	check database, tạo table boilerplate trong foder schema. gotecq--internship.sql

# Sanic CQRS
- Hỗ trợ thư viện cho phía lệnh trong CQRS  (Command Query Responsibility Segregation).
- Mục tiêu của thư viện này là cung cấp một cách đơn giản để đăng ký điểm cuối( register endpoint)  cho mỗi miền và thư viện này cũng triển khai các interface được định nghĩa trong thư viện `fii-cqrs`. 
Hiện tại, chúng tôi đang sử dụng PostgresSQL để lưu trữ dữ liệu, vì vậy chúng tôi đã sử dụng thư viện `Gino` (GINO - GINO Is Not ORM - là một ORM không đồng bộ nhẹ được xây dựng dựa trên` SQLAlchemy`core cho Python asyncio.) Để triển khai các giao diện . Có bốn giao diện bao gồm:
    - For resource: `PostgrestCqrsResource` (` resource / gino.py`) triển khai giao diện `CqrsResource` (giao diện này được định nghĩa trong` resource.py` trong `fii-cqrs`)
    - For command store: `PostgresCommandStore` (` storage / gino.py`) thực hiện `CommandStore` (` command_store.py` trong `fii-cqrs`)
    - For event store: `PostgresEventStore` (` storage / gino.py`) triển khai EventStore (`event_store` trong` fii-cqrs`)
    - For context store: ` `PostgresContextStore` (` storage / gino.py`) triển khai `ContextStore` (` context_store.py` trong `fii-cqrs`)

## Đăng ký Điểm cuối Miền
* Cách sử dụng: Register domain handler for specific command endpoint url.
*  Endpoint URL: 
     * Đối với đường dẫn chung (generic path): `/ {domain_prefix}: {cmd_key} / {resource}`
     * Đối với đường dẫn mục cụ thể: `/ {generic_path} / {_ id}`
     * Trong đó:
         * `{domain_prefix}`: Không gian tên miền (tức là: mjl.qmx)
         * `{cmd_key}`: Phím lệnh, phân tách bằng ký tự gạch nối (tức là: tạo hồ sơ)
         * `{resource}`: Tên tài nguyên gốc (tức là: nhân viên)
         * `{_id}`: định danh của tài nguyên gốc.
     * Thí dụ:
         * Đường dẫn chung: `/mjl.qmx: create-profile / staff` với phương thức:` POST`
         * Đường dẫn mục cụ thể: `/mjl.qmx: update-profile / worker / 123` với phương thức:` PATCH`
         
## PostgrestCqrsResource
- This class implement the interface `CqrsResource`.
- Nó hỗ trợ một số phương pháp để tương tác với PostgresSQL:
     - Store (lưu trữ): `insert_one`,` remove_one`, `update_one`.
     - Truy vấn: `find`,` find_one`, `select`,` fetch`, `lookup`,` fetch`, `fetch_by_intra_id`.
- `Lưu ý`:
     - Trình quản lý dữ liệu (datalayer) thường được gọi trong `aggregate` hoặc` event` bởi state manager của Gino (`statemgr`) /
     - Hãy nhớ rằng tất cả các phương thức trong datalayer đang được state manager sử dụng. Nó không phải là cuộc gọi trực tiếp.
     - Cách sử dụng đúng:
         ``
         đang chờ statemgr.fetch (resource = "resource", _id = "_ id")
         ``
     - Cách sử dụng sai:
         ``
         await statemgr.datalayer.fetch (resource = "resource", _id = "_ id")
         ``         
#statemgr là một đối tượng lưu trữ phiên trao đổi với database của Gino với phương thưc db.transaction() thông qua domain, statemgr chưa nhiều phuơng thức tương tác với database 


Gino db type
    (dict,): p(db.JSON),
    (bool,): p(db.Boolean),
    (str,): p(db.String),
    (float,): p(db.Float),
    (int,): p(db.Integer),
    (date,): p(db.Date()),
    (datetime,): p(db.DateTime(timezone=False)),
    (datetime, str): p(db.DateTime(timezone=False)),
    (uuid.UUID,): p(UUID),
    (uuid.UUID, type(None)): p(UUID),
    (type(None), uuid.UUID): p(UUID),
    (): p(db.String)
    


lam create cho idcard, mới chỉ tạo model
tạo thêm resource, datadef, command , aggeater, event
tao du lieu cho test truoc, lam 3 query cho bank account
sua lai rut tien, gioi han giao dich




Create account
{
  "account_password": "huy",
  "account_status_id": 1,
  "account_type_id": "fda5311b-8bd7-4631-a5a1-1e3ecbbc87b0",
  "avaiable_balance": 200000000,
  "bank_branch_id": "fda5311b-8bd7-4631-a5a1-1e3ecbbc87b0",
  "current_balance": 200000000,
  "customer_id": "fda5311b-8bd7-4631-a5a1-1e3ecbbc87b0",
  "username": "huyngo"
}

create customer
{
  "date_of_birth": "2000-2-4",
  "email": "huy@gmail.com",
  "first_name": "Huy",
  "gender_id": "F",
  "id_number": "5304bd48-f52d-4a34-ada9-f768c9b6fccd",
  "last_name": "Ngo",
  "middle_name": "Ngoc",
  "phone_number": "0905606287"
}

update customer
{
  "date_of_birth": "2000-5-4"
}

test withdraw
{
  "_id": "f2cc238e-8cf6-46c1-b929-306a4c93544f",
  "amount": 2000000,
  "atm_machine_id": "f2cc238e-8cf6-46c1-b929-306a4c93544f",
  "transaction_type_id": "f2cc238e-8cf6-46c1-b929-306a4c93544f"
}




