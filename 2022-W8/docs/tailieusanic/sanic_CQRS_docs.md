CQRS 
https://edwardthienhoang.wordpress.com/2018/01/26/command-query-responsibility-segregation-cqrs/
# Sanic CQRS

- Library support for command side in CQRS (Command Query Responsibility Segregation).
- The goal of this library is provide a simple way to register endpoint for each domain and this library also implement the interfaces which is defined in the library `fii-cqrs`. Currently, we are using the PostgresSQL for storing the data, so we used the `Gino` library (GINO - GINO Is Not ORM - is a lightweight asynchronous ORM built on top of `SQLAlchemy`core for Python asyncio.) to implement the interfaces. There are four interfaces include:
    - For resource: `PostgrestCqrsResource` (`resource/gino.py`) implement the interface `CqrsResource` (this interface is defined in  `resource.py` in `fii-cqrs`)
    - For command store: `PostgresCommandStore` (`storage/gino.py`) implement `CommandStore` (`command_store.py` in `fii-cqrs`)
    - For event store: `PostgresEventStore` (`storage/gino.py`) implement EventStore (`event_store` in `fii-cqrs`)
    - For context store: `PostgresContextStore` (`storage/gino.py`) implement `ContextStore` (`context_store.py` in `fii-cqrs`)

## Register Domain Endpoint
* Usage: Register domain handler for specific command endpoint url.
* Endpoint URL:  
    * For generic path: `/{domain_prefix}:{cmd_key}/{resource}`
    * For specific-item path: `/{generic_path}/{_id}`
    * In which:  
        * `{domain_prefix}`: Domain namespace (i.e: mjl.qmx)
        * `{cmd_key}`: Command key, which separate with hyphen character (i.e: create-profile)
        * `{resource}`: Root resource's name (i.e: employee)
        * `{_id}`: identifier of root resource.
    * Example:
        * Generic path: `/mjl.qmx:create-profile/employee` with method: `POST`
        * Specific-item path: `/mjl.qmx:update-profile/employee/123` with method: `PATCH`

## PostgrestCqrsResource
- This class implement the interface `CqrsResource`.
- It supports some methods to interact with the PostgresSQL:
    - Store: `insert_one`, `remove_one`, `update_one`.
    - Query: `find`, `find_one`, `select`, `fetch`, `lookup`, `fetch`, `fetch_by_intra_id`.
- `Note`:
    - The datalayer is often called in `aggregate` or `event` by state manager (`statemgr`)/  
    - Remember that all method in datalayer is using by state manager. It shouldn't be directed call.
    - Correctly usage:
        ```
        await statemgr.fetch(resource="resource", _id="_id")
        ```
    - Wrong usage:
        ```
        await statemgr.datalayer.fetch(resource="resource", _id="_id")
        ```
### Store:
...
### Query:
#### The structure we built for input data query like this:
```python
class BackendQuery():
    select: list
    limit: int
    offset: int
    sort: list
    text: str
    where: (dict, list)
    show_deleted: bool
    gino_extension: {
        join: {
            "table": str,
            "localField": str,
            "foreignfield": str
        }
    }
```
- `Note`: For more detail you can view it in `resource/param.py`.

#### For intuition how to use the methods and what is the output data.
- We assumed that we have the simple data like this:
- The table `child` has field `parent_id` is foreign key from table `parent`.
- `child` and `parent` are design in a domain. We assumed `parent` is a `root` and `child` is a sub resource.
- Example data: 
    ```python
    ParentResource(PostgresCqrsResource):
        name = field(type=str)
        class __backend__(db.Model):
            _id = db.Column(db.Integer)
            name = db.Column(db.String)
    ChildResource(PostgresCqrsResource):
        name = field(type=str)
        class __backend__(db.Model):
            _id = db.Column(db.Integer)
            intra_id = db.Column(db.Integer)
            parent_id = db.Column(db.Integer)
            name = db.Column(db.String)
    # Example data that we have in the database
    parent = {
        _id: 1,
        name: "parent"
    }
    child = {
        _id: 1,
        intra_id: 1,
        parent_id: 1,
        name: "child"
    }
    ```
- `Note`: Any sub resource in a domain are always have two field:
    - aggroot_id: the identifier of the root table
    - intra_id (intra domain ID): the identifier of the sub resource in a domain.
    - Example: 
    ```python
        Follow the above example:
        - We have table 'chird' is a sub resource and the table 'parent' is a root in domain.
        - So the schema of `chird` will have two field: `parent_id`, `intra_id`
    ```

#### Retrieve a record with `_id` or `intra_id`.
###### `Fetch`: 
- In statemgr : `fetch(resource, _id, **kwargs) -> CqrsResource`
- usage: In case, you have `_id` and you want to retreive a record from database.
- output: is a CqrsResource.
    ```python
    # You want to retreive record in `parent` table. You have the `_id = 1`.
    data = await statemgr.fetch("parent", _id=1, **kwargs)
    # Output: ParentResource(_id=1, name="parent")
    # If not found: it'll raise a exception ItemNotFoundError()
    ```
###### `lookup`:
- In statemgr: `lookup(self, resource, _id=None, **kwargs) -> CqrsResource`
- usage: it's such as the `fetch` method. But in base the record is not found, the output will be 
None.
- output: is a CqrsResource or None
    ```python
    # You want to retreive record in `parent` table. You have the `_id = 1`.
    data = await statemgr.lookup("parent", _id=1, **kwargs)
    # Output: ParentResource(_id=1, name="parent")
    # If not found: None
    ```
###### `fetch_by_intra_id`:
- In statemgr: `fetch_by_intra_id(self, resource, intra_id=None, **kwargs) -> CqrsResource`
- usage: In case. You have intra_id, root_id in a domain and you want to retreieve a record from database.
- output: is a CqrsResource.
    ```python
    # You want to retreive record in `child` table. You have the `intra_id = 1` and `parent_id = 1`.
    data = await statemgr.fetch_by_intra_id("child", intra_id=1, parent_id=1)
    # Output: ChildResource(_id=1, intra_id=1, name="child", parent_id=1)
    # If not found: it'll raise a exception ItemNotFoundError()
    ```

#### Retrieve record if you want to input a data_query BackendQuery.
##### `find`:
- In statemgr: `find(self, resource, data_query, **kwargs) -> list(CqrsResource)`.  

##### `find_one`:
- In statemgr: `find_one(self, resource, data_query, **kwargs) -> list(CqrsResource)`.  

##### `select`:
- In statemgr: `select(self, resource, data_query, **kwargs) -> list(list of fields)`.  

##### `Usage`:
###### - Where:
- usage: You want to retrieve records in the database with some conditions.
    ```python
    # You want to retrieve record from `child` table with name='child' and parent_id=1
    data_query = {
        "where": {
            "name": "child",
            "parent_id": 1
        }
    }
    # Use find method
    data = await statemgr.find("child", data_query=data_query)
    # Output: [ChildResource(_id=1, intra_id=1, name="child", parent_id=1), ]
    # If not found: []

    # Use find_one method
    data = await statemgr.find_one("child", data_query=data_query)
    # Output: ChildResource(_id=1, intra_id=1, name="child", parent_id=1)
    # If not found: None
    ```
##### - Show_deleted
- usage: Default `show_deleted` is False. Everything method will lookup record is `not deleted` in the database.
    ```python
    # You want to retrieve record from `child` table with name='child' and parent_id=1
    data_query = {
        "where": {
            "name": "child",
            "parent_id": 1
        },
        "show_deleted": True
    }
    # Use find method
    data = await statemgr.find("child", data_query=data_query)
    # Output: [ChildResource(_id=1, intra_id=1, name="child", parent_id=1), ]
    # If not found: []
    ```
##### - Join
- usage: In case, you want to retrieve the record with some fields and some conditions from another table (foreign table).
    ```python
    # You want to retrieve record from `child` table with child's name='child' and parent's name='parent'
    data_query = {
        "gino_extension": {
            "join": {
                "table": "parent",
                "localField": "parent_id",
                "foreignField": "_id"
            }
        },
        "where": {
            "child.name": "child",
            "parent.name": "parent"
        }
    }
    # Use find method
    data = await statemgr.find("child", data_query=data_query)
    # Output: [ChildResource(_id=1, intra_id=1, name="child", parent_id=1), ]
    # If not found: []
    ```
##### select:
- Usage: In case, you want to retrieve some fields of the record.
    ```python
    # You want to retrieve child's name.
    data_query = {
        "select": ["name"]
    }
    # Use find method
    data = await statemgr.select("child", data_query=data_query)
    # Output: [("child"), ()]
    # If not found: []

    # You want to retrieve child's name and parent's name from `child` table with child's name='child' and parent's name='parent'
    data_query = {
        "select": ["child.name", "parent.name"]
        "gino_extension": {
            "join": {
                "table": "parent",
                "localField": "parent_id",
                "foreignField": "_id"
            }
        },
        "where": {
            "child.name": "child",
            "parent.name": "parent"
        }
    }
    # Use find method
    data = await statemgr.select("child", data_query=data_query)
    # Output: [("child", "parent"), ()]
    # If not found: []
    ```

