CREATE TABLE people1 (
    height_cm numeric,
    height_in numeric GENERATED ALWAYS AS (height_cm / 2.54) STORED
);

CREATE SCHEMA "atm--huyngo"
drop table "atm--huyngo".boilerplate 

CREATE TABLE "atm--huyngo"."bank--account" (
    _id uuid PRIMARY KEY,
    username character varying(255),
    account_password character varying,
    avaiable_balance int4,
   	current_balance int4,
   	account_type_id uuid,
    account_status_id int4,
    customer_id uuid,
    bank_id uuid,
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);


CREATE TABLE "atm--huyngo"."customer" (
    _id uuid PRIMARY KEY,
    first_name character varying(255),
    middle_name character varying(255),
    last_name character varying(255),
	date_of_birth timestamp without time zone,
	email character varying(255),
 	phone_number character varying(255) ,
    gender_id character varying(255),
    id_number uuid,
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

ALTER TABLE "atm--huyngo"."bank--account" 
ADD CONSTRAINT bank_account_to_customer_fk 
FOREIGN KEY (customer_id) 
REFERENCES "atm--huyngo".customer(_id)

CREATE TABLE "atm--huyngo"."citizen--identity--card" (
    _id uuid PRIMARY KEY,
    address character varying(255),
    street character varying(255),
    district character varying(255),
    city character varying(255),
	country character varying(255),
	date_of_issue timestamp without time zone,
 	place_of_issue character varying(255),
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

CREATE TABLE "atm--huyngo"."gender" (
    _id character varying(255)PRIMARY KEY,
    gender_type character varying(255),
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

ALTER TABLE "atm--huyngo".customer 
ADD CONSTRAINT customer_to_gender_fk 
FOREIGN KEY (gender_id) 
REFERENCES "atm--huyngo".gender(_id)

CREATE TABLE "atm--huyngo"."atm--card" (
    _id uuid PRIMARY KEY,
    pin character varying(255),
    atm_card_type_id int4,
    bank_account_id uuid,
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

CREATE TABLE "atm--huyngo"."transaction--log" (
    _id uuid PRIMARY KEY,
    transaction_date timestamp without time zone,
    amount int4,
    transfer_content character varying(255),
    transaction_type_id uuid,
    atm_machine_id uuid,
    source_account_id uuid,
    destination_account_id uuid,
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);
ALTER TABLE "atm--huyngo"."transaction--log" 
ADD CONSTRAINT transaction_log_to_account_bank_fk 
FOREIGN KEY (source_account_id) 
REFERENCES "atm--huyngo"."bank--account"("_id");

ALTER TABLE "atm--huyngo"."transaction--log" 
ADD CONSTRAINT transaction_log_to_account_bank_fk_1 
FOREIGN KEY (destination_account_id) 
REFERENCES "atm--huyngo"."bank--account"("_id");


CREATE TABLE "atm--huyngo"."atm--machine" (
    _id uuid PRIMARY KEY,
    amount int4,
    address character varying(255),
    street character varying(255),
    district character varying(255),
    city character varying(255),
	country character varying(255),
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);
ALTER TABLE "atm--huyngo"."transaction--log" 
ADD CONSTRAINT transaction_log_to_atm_machine_fk
FOREIGN KEY (atm_machine_id) 
REFERENCES "atm--huyngo"."atm--machine"("_id");

CREATE TABLE "atm--huyngo"."bank" (
    _id uuid PRIMARY KEY,
    name character varying(255),
    address character varying(255),
    street character varying(255),
    district character varying(255),
    city character varying(255),
    country character varying(255),
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);
CREATE TABLE "atm--huyngo"."account--status" (
    _id int4 PRIMARY KEY,
    description character varying(255),
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

CREATE TABLE "atm--huyngo"."atm--card" (
	"_id" uuid NOT NULL,
	pin varchar(255) NULL,
	effective_date timestamp without time zone,
	expiration_date timestamp without time zone,
	bank_account_id uuid NULL,
	"_created" timestamp NULL,
	"_updated" timestamp NULL,
	"_deleted" timestamp NULL,
	"_etag" varchar(255) NULL,
	"_creator" uuid NULL,
	"_updater" uuid NULL,
	CONSTRAINT "atm--card_pkey" PRIMARY KEY (_id)
);
CREATE TABLE "atm--huyngo"."account--type" (
	_id uuid PRIMARY KEY ,
	description varchar(255) ,
	_created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

ALTER TABLE "atm--huyngo"."atm--card" 
ADD CONSTRAINT atm_card_to_bank_account_fk1 
FOREIGN KEY (bank_account_id) 
REFERENCES "atm--huyngo"."bank--account"("_id");

ALTER TABLE "atm--huyngo"."bank--account" 
ADD CONSTRAINT bank_account_to_account_status_fk 
FOREIGN KEY (account_status_id) 
REFERENCES "atm--huyngo"."account--status"("_id");

ALTER TABLE "atm--huyngo"."bank--account" 
ADD CONSTRAINT bank_account_to_bank_fk 
FOREIGN KEY (bank_id) 
REFERENCES "atm--huyngo"."bank"("_id");

ALTER TABLE "atm--huyngo"."bank--account" 
ADD CONSTRAINT bank_account_to_account_type_fk 
FOREIGN KEY (account_type_id) 
REFERENCES "atm--huyngo"."account--type"("_id");

CREATE TABLE "atm--huyngo"."customer--address" (
	_id uuid PRIMARY KEY ,
	is_primary boolean,
	address character varying(255),
    street character varying(255),
    district character varying(255),
    city character varying(255),
    country character varying(255),
    customer_id uuid,
	_created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);
ALTER TABLE "atm--huyngo"."customer--address" 
ADD CONSTRAINT customer_address_to_customer_fk 
FOREIGN KEY (customer_id) 
REFERENCES "atm--huyngo".customer("_id");

CREATE TABLE "atm--huyngo"."transaction--limit" (
	_id uuid PRIMARY KEY ,
	amount int4,
	account_type_id uuid,
    transaction_type_id uuid,
	_created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

CREATE TABLE "atm--huyngo"."transaction--type" (
	_id uuid PRIMARY KEY ,
	name character varying(255),
	transaction_fee_amount int4,
    description character varying(255),
    bank_id uuid,
	_created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

CREATE TABLE "atm--huyngo"."minimum--balance" (
	_id uuid PRIMARY KEY ,
	balance int4,
    bank_id uuid,
	_created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);

CREATE TABLE "atm--huyngo"."bank--branch" (
    _id uuid PRIMARY KEY,
    bank_id uuid,
    name character varying(255),
    address character varying(255),
    street character varying(255),
    district character varying(255),
    city character varying(255),
    country character varying(255),
    _created timestamp without time zone,
    _updated timestamp without time zone,
    _deleted timestamp without time zone,
    _etag character varying(255),
    _creator uuid,
    _updater uuid
);
ALTER TABLE "atm--huyngo"."transaction--limit" 
ADD CONSTRAINT transaction_limit_to_account_type_fk 
FOREIGN KEY (account_type_id) 
REFERENCES "atm--huyngo"."account--type"("_id");

ALTER TABLE "atm--huyngo"."transaction--limit" 
ADD CONSTRAINT transaction_limit_to_transaction_type_fk 
FOREIGN KEY (transaction_type_id) 
REFERENCES "atm--huyngo"."transaction--type"("_id");


ALTER TABLE "atm--huyngo".customer 
ADD CONSTRAINT customer_to_gender_fk 
FOREIGN KEY (gender_id) 
REFERENCES "atm--huyngo".gender("_id");

ALTER TABLE "atm--huyngo".customer 
ADD CONSTRAINT customer_to_id_card_fk 
FOREIGN KEY (id_number) 
REFERENCES "atm--huyngo"."citizen--identity--card"("_id");

ALTER TABLE "atm--huyngo"."transaction--log" 
ADD CONSTRAINT transaction_log_to_account_fk1 
FOREIGN KEY (source_account_id) 
REFERENCES "atm--huyngo"."bank--account"("_id");

ALTER TABLE "atm--huyngo"."transaction--log" 
ADD CONSTRAINT transaction_log_to_account_fk2 
FOREIGN KEY (destination_account_id) 
REFERENCES "atm--huyngo"."bank--account"("_id");

ALTER TABLE "atm--huyngo"."transaction--log" 
ADD CONSTRAINT transaction_log_to_atm_machine_fk
FOREIGN KEY (atm_machine_id) 
REFERENCES "atm--huyngo"."atm--machine"("_id");

ALTER TABLE "atm--huyngo"."transaction--log" 
ADD CONSTRAINT transaction_log_to_transaction_type_fk
FOREIGN KEY (transaction_type_id) 
REFERENCES "atm--huyngo"."transaction--type"("_id");

ALTER TABLE "atm--huyngo"."bank--branch" 
ADD CONSTRAINT bank_branch_fk 
FOREIGN KEY (bank_id) 
REFERENCES "atm--huyngo".bank("_id");

ALTER TABLE "atm--huyngo"."minimum--balance" 
ADD CONSTRAINT minimum_balance_fk 
FOREIGN KEY (bank_id) 
REFERENCES "atm--huyngo".bank("_id");

ALTER TABLE "atm--huyngo"."transaction--type" 
ADD CONSTRAINT transaction_type_fk 
FOREIGN KEY (bank_id) 
REFERENCES "atm--huyngo".bank("_id");

create view "atm--huyngo"."view--account" as 
select acc."_id" id_account, 
	acc .username ,
	acc .account_password ,
	acc .avaiable_balance ,
	b."name" name_bank,
	b.address ,
	b.street ,
	b.district ,
	b.city ,
	b.country,
	c.first_name ,
	c.middle_name ,
	c.last_name ,
	c.date_of_birth ,
	c.phone_number ,
	c.gender_id ,
	ac."_id" id_atm_card,
	ac.pin ,
	ac.effective_date ,
	ac.expiration_date 
from "atm--huyngo"."bank--account" acc 
inner join "atm--huyngo"."bank" b on acc.bank_id = b._id
inner join "atm--huyngo".customer c ON acc.customer_id = c."_id" 
inner join "atm--huyngo"."atm--card" ac on acc ."_id" = ac.bank_account_id ;  

create view "atm--huyngo"."view--transaction" as
select tl."_id" id_transaction,
		tl.transaction_date ,
		tl.amount ,
		tl.transfer_content ,
		tl.atm_machine_id ,
		tl.source_account_id,
		c.first_name fn_of_source_account ,
		c.middle_name md_of_source_account ,
		c.last_name ls_of_source_account ,
		ba.avaiable_balance  avaiable_balance_of_source_account,
		tl.destination_account_id ,
		c1.first_name fn_of_destination_account ,
		c1.middle_name md_of_destination_account,
		c1.last_name ls_of_destination_account ,
		ba2.avaiable_balance avaiable_balance_of_destination_account
from "atm--huyngo"."transaction--log" tl 
inner join "atm--huyngo"."bank--account" ba on tl.source_account_id = ba."_id" 
left join "atm--huyngo"."bank--account" ba2 on tl.destination_account_id = ba2."_id" 
inner join "atm--huyngo".customer c on c."_id" = ba.customer_id  
left join "atm--huyngo".customer c1 on c."_id" = ba2.customer_id 

create view "atm--huyngo"."account--actived" as

from ""













