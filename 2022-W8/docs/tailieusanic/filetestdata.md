test create account
{
  "account_password": "huy",
  "account_type_id": "1111bd48-f52d-4a34-ada9-f768c9b6fccd",
  "bank_id": "d336b5d9-c709-4940-b62a-448e856b879e",
  "customer_id": "3a87da86-f45e-4f39-8197-67eeeada02bc",
  "username": "huydo",
  "effective_date": "2022-9-1",
  "expiration_date": "2040-9-1"
}


create bank


{
  "address": "35",
  "city": "Đà nẵng",
  "country": "Việt Nam",
  "district": "Liên Chiều",
  "name": "ABC",
  "street": "Hoàn Diệu",
  "transfer_fee": 3000,
  "transfer_limit": 1000000000,
  "withdrawal_fee": 2000,
  "withdrawal_limit": 5000000,
  "deposit_fee": 0,
  "deposit_limit": 0,
  "minimum_balance": 50000
}


create customer 1
{
  "account_password": "huy12345",
  "address": "30",
  "bank_id": "56463b90-26e6-4994-b21a-fe5c9a35d776",
  "city": "Da Nang",
  "country": "Viet Nam",
  "current_address": "35",
  "current_city": "Ho Chi Minh",
  "current_country": "Viet Nam",
  "current_district": "Quan 2",
  "current_stresst": "Vo Chi Cong",
  "date_of_birth": "2000-6-5",
  "date_of_issue": "2018-9-8",
  "district": "Hoa Vang",
  "effective_date": "2022-9-1",
  "email": "huyngo.05042000@gmail.com",
  "expiration_date": "2044-9-1",
  "first_name": "Huy",
  "gender_id": "F",
  "last_name": "Ngo",
  "middle_name": "Ngoc",
  "number": "201899898979788",
  "phone_number": "0905606287",
  "place_of_issue": "Cong an TP Da Nang",
  "street": "14B1",
  "username": "huyngo",
  "pin": "huy12345"
}
create customer 2

{
  "account_password": "huy12345",
  "address": "30",
  "bank_id": "b54fdecf-87d5-4b80-bcf6-9d7391aed5a9",
  "city": "Da Nang",
  "country": "Viet Nam",
  "current_address": "35",
  "current_city": "Ho Chi Minh",
  "current_country": "Viet Nam",
  "current_district": "Quan 2",
  "current_stresst": "Vo Chi Cong",
  "date_of_birth": "2000-6-5",
  "date_of_issue": "2018-9-8",
  "district": "Hoa Vang",
  "effective_date": "2022-9-1",
  "email": "huyngo.05042000@gmail.com",
  "expiration_date": "2044-9-1",
  "first_name": "Huy",
  "gender_id": "F",
  "last_name": "Ngo",
  "middle_name": "Ngoc",
  "number": "201899898979780",
  "phone_number": "0905606287",
  "place_of_issue": "Cong an TP Da Nang",
  "street": "14B1",
  "username": "huyngo123",
  "pin": "huy12345"
}

Create account
{
  "account_password": "huy",
  "account_status_id": 1,
  "account_type_id": "1111bd48-f52d-4a34-ada9-f768c9b6fccd",
  "avaiable_balance": 200000000,
  "bank_id": "5304bd48-f52d-4a34-ada9-f768c9b6fccd",
  "current_balance": 200000000,
  "customer_id": "be3c6c7d-c290-4ad5-a9f8-264e96942cd9",
  "username": "huyngo"
}



create atm machine
{
  "address": "40",
  "amount": 10000000,
  "city": "Đà Nẵng",
  "country": "Việt Nam",
  "district": "Sơn Trà",
  "street": "Hoàng Hoa Thắm"
}

test withdraw
{
  "_id": "9b2917e3-6df6-4eae-9755-4ad39a729f40",
  "amount": 200000,
  "atm_machine_id": "5304bd48-f52d-4a34-ada9-f768c9b60000",
  "transaction_type_id": "0001bd48-f52d-4a34-ada9-f768c9b6fccd"
}



create card

{
  "bank_account_id": "f255e697-0eb4-4612-b33d-2cbf2516f1b4",
  "pin": "12876737",
  "effective_date": "2022-9-1",
  "expiration_date": "2032-1-1"
}


create id card
{
  "address": "35",
  "city": "Da nang",
  "country": Viet Nam",
  "date_of_birth": "2000-3-1",
  "date_of_issue": "2018-9-1",
  "district": "Hoa Vang",
  "first_name": "Huy",
  "last_name": "Ngo",
  "middle_name": "Ngoc",
  "number": "03788788827276",
  "place_of_issue": "Canh Sat",
  "street": "Trường Chinh"
}

rut tien

{
   "_id": "cec8d806-9056-4b36-b5de-5ff3e546c8a4",
   "amount": 200000,
   "atm_machine_id": "5304bd48-f52d-4a34-ada9-f768c9b60000",
   "transaction_type_id": "0001bd48-f52d-4a34-ada9-f768c9b6fccd"
}

Nap tien
{
   "_id": "cec8d806-9056-4b36-b5de-5ff3e546c8a4",
   "amount": 200000,
   "atm_machine_id": "5304bd48-f52d-4a34-ada9-f768c9b60000",
   "transaction_type_id": "0002bd48-f52d-4a34-ada9-f768c9b6fccd"
}


tranfer
{
  "source_account_id": "cec8d806-9056-4b36-b5de-5ff3e546c8a4",
  "destination_account_id": "9b2917e3-6df6-4eae-9755-4ad39a729f40",
  "amount": 2000000,
  "atm_machine_id": "5304bd48-f52d-4a34-ada9-f768c9b60000",
  "transaction_type_id": "0003bd48-f52d-4a34-ada9-f768c9b6fccd",
  "transfer_content": "tiền mua vũ khí hạt nhân"
}


view account




