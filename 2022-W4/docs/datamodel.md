b. Luồng giao dịch của hệ thống ATM
- Màn hình đợi (màn hình hiển thị quảng cáo của ngân hàng)
8

- Cho thẻ vào ATM và nhập số PIN
- Kiểm tra số thẻ: kiểm tra số check digit, số CVV/CVC
- Kiểm tra PIN: kiểm tra số PIN đƣợc nhập vào với PIN đƣợc lƣu
trong CSDL Corebank của ngân hàng, nếu đúng sẽ hiển thị các loại giao dịch
để chủ thẻ lựa chọn
- Thực hiện giao dịch: Khi thực hiện thành công, thì tùy theo từng loại
giao dịch mà ATM nhả thẻ hoặc không (thƣờng thì rút tiền xong ATM sẽ
nhả thẻ)
- Trở về màn hình đợi: Khi không thực hiện các giao dịch nữa (khi
nhả thẻ hoặc nuốt thẻ) màn hình ATM trở về trạng thái ban đầu.



Các chức năng có của ATM
Đăng nhập

Rút tiền mặt

Đăng nhập

thẻ trả trước định danh rút tiền ở đâu
https://thebank.vn/blog/14705-the-tra-truoc-dinh-danh-de-dang-dang-ky-ma-khong-can-tai-khoan.html#:~:text=Kh%C3%A1ch%20h%C3%A0ng%20c%C3%B3%20th%E1%BB%83%20th%E1%BB%B1c,c%C3%A1c%20ATM%20ch%E1%BA%A5p%20nh%E1%BA%ADn%20th%E1%BA%BB


ngân hàng nào đang có thẻ trả trước
https://thebank.vn/blog/13627-the-tra-truoc-la-gi-co-cac-loai-the-tra-truoc-nao.html
https://websosanh.vn/tin-tuc/co-nhung-loai-the-tra-truoc-ngan-hang-nao-c78-20180905034345732.htm

thẻ trả trước
https://www.practicalmoneyskills.com.vn/nguon/chuyengia/articles/article_100714.php?print=y

https://thebank.vn/blog/17706-may-atm-la-gi-the-atm-la-gi-3-phut-nam-ro-chuc-nang-cua-tung-loai.html#:~:text=M%C3%A1y%20ATM%2C%20hay%20c%C3%B2n%20%C4%91%C6%B0%E1%BB%A3c,ti%E1%BB%81n%20h%C3%A0ng%20h%C3%B3a%20d%E1%BB%8Bch%20v%E1%BB%A5.

Debit Card vs. Credit Card là gì
withdraw cash
view account balance
deposit funds
deposit card
available balance
total balance 
lock

Số dư khả dụng (Available Balance) là gì?
https://vietnambiz.vn/so-du-kha-dung-available-balance-la-gi-20200309172550365.htm#:~:text=S%E1%BB%91%20d%C6%B0%20kh%E1%BA%A3%20d%E1%BB%A5ng%20(ti%E1%BA%BFng,%2C%20r%C3%BAt%20ti%E1%BB%81n%2C%20chuy%E1%BB%83n%20kho%E1%BA%A3n.

file:///home/huy/Downloads/DesignExercise.pdf
https://www.davuniversity.org/images/files/study-material/UML-ATM.pdf
What does the id card include?

xem thẻ id
https://sotp.thainguyen.gov.vn/tin-noi-bat/-/asset_publisher/hEnG94ajEooh/content/mot-so-ieu-can-biet-ve-the-can-cuoc-cong-dan/pop_up?_101_INSTANCE_hEnG94ajEooh_viewMode=print&_101_INSTANCE_hEnG94ajEooh_languageId=vi_VN

quá trình thực hiện giao dịch
https://text.xemtailieu.net/tai-lieu/nghien-cuu-tim-hieu-he-thong-rut-tien-tu-dong-atm-va-van-de-an-toan-thong-tin-cua-he-thong-352156.html


đọc thêm quy tắc đặt tên
https://www.sqlshack.com/learn-sql-naming-conventions/

tham khao chính
https://itsourcecode.com/uml/atm-management-system-er-diagram-entity-relationship-diagram/

tham khảo thêm database bank
https://soft-builder.com/bank-management-system-database-model/
database bank

shttps://timo.vn/blogs/so-du-kha-dung-la-gi/
https://tableplus.com/blog/2018/04/postgresql-how-to-grant-access-to-users.html


#generated column
https://www.postgresql.org/docs/current/ddl-generated-columns.html

streaming replication
#docker run --rm --name airbyte-destination -e POSTGRES_PASSWORD=Huy12345 -p 3000:5432 -d  postgres (tạo một máy chủ mới)
#psql -h 127.0.0.0 -U postgres -p 3000 (truy cập vào postgresql docker)
#truy cập foder airbyte run câu lệnh để chạy airbyte
docker-compose up

psql: check schema \dn
      check table  \dt
      check role   \du
      check database \l
      check foreign table     \det

#set search path đọc thêm
https://www.vertica.com/docs/9.3.x/HTML/Content/Authoring/SQLReferenceManual/Statements/SET/SETSEARCH_PATH.htm?TocPath=SQL%20Reference%20Manual%7CSQL%20Statements%7CSET%20Statements%7C_____6

#postgres_fdw
Create EXTENSION :
    CREATE EXTENSION postgres_fdw;
Create SERVER :
    CREATE SERVER server_name FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'host_ip', 
    dbname 'db_name', port 'port_number');
Create USER MAPPING:
    CREATE USER MAPPING FOR CURRENT_USER
    SERVER server_name
    OPTIONS (user 'user_name', password 'password');
Create new schema to access schema of server DB:
   CREATE SCHEMA schema_name;
Import server schema:
     IMPORT FOREIGN SCHEMA schema_name_to_import_from_remote_db
     FROM SERVER server_name
     INTO schema_name;
Chỉ định thứ tự Vertica tìm kiếm các lược đồ khi câu lệnh SQL chỉ định tên bảng không đủ tiêu chuẩn bằng tên lược đồ.     
SET search_path = remotedb;
Access any table of server schema:
    SELECT * FROM schema_name.table_name; 

#ví dụ
CREATE EXTENSION postgres_fdw;
CREATE SERVER postgrestest FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host '127.0.0.0', 
    dbname 'postgres', port '3000:5432');
CREATE USER MAPPING FOR postgres
    SERVER postgrestest
    OPTIONS (user 'postgres', password 'Huy12345');dụ

#customer: lưu thông tin khách hàng
#bank_account: lưu tài khoản ngân hàng khách hàng
#account_type: loại tài khoản, sẽ ảnh hưởng đến giao dịch
#account_status: trạng thái của tài khoản, có những trường hợp khách hàng muốn đóng tài khoản hoặc tài khoản này không còn dùng nữa.
#bank: tên ngân hàng
#account_balance: số dư tài khoản, 
  + cột current_balance: số dư hiện tại, bao gồm cả các giao dịch đang được xử lý(cái này chưa làm) chỉ đơn giãn là bảng là chưa tính số dư tối thiểu, tùy vào loại thẻ, ngân hàng sẽ có số dư tối thiểu riêng. 
#minimum_balance: số dư tối thiểu
#atm_card : lưu thẻ atm cùng mã pin để dùng khi thực hiện giao dịch tại atm
#atm_card_type: lưu loại thẻ, các loại thẻ không phù hợp có thẻ bị từ chối ở atm
#transaction_log: lưu giao dịch, số tiền đã giao dịch, kết hợp với bảng loại giao dịch sẽ tính ra được số dư mới, số dư này được tính theo công thưc: new_balance = available balance + hoặc - số tiền giao dịch - phí giao dịch thuộc vào mỗi loại giao dịch. 
#transaction_type: loại giao dịch
#tranfer: lưu số tài khoản của người nhận trong loại giao dịch chuyển tiền. Có thẻ quản lý nhận và gửi của một tài khoản, phát hiện sự bất thường.


    







