#!/bin/bash
echo "Seclect theme which you want to install"


echo "1. Update latest package via Package manager"
echo "2. Terminal yakuake"
echo "3. Shell "
echo "4. Git"
echo "5. Text editor"
echo "6. Browser"
echo "7. Zulip"
echo "8. Ibus-bamboo"
echo
echo "Enter your choice"
read p;
  if [ $p -eq 1 ];
    then
      echo "------Installation of Update latest package via Package manager"
      sudo apt update
  elif [ $p -eq 2 ];
  then
      echo "------Install yakuake------------ "
      sudo apt -y install yakuake
  elif [ $p -eq 3 ];
  then
      echo "------Install Shell------------ "
      sudo apt install shellinabox
  elif [ $p -eq 4 ];
  then
      echo "------Install Git------------ "
      sudo apt-get install git
  elif [ $p -eq 5 ];
  then
      echo "------Install Vim Text Editor------------ "
       sudo apt-get install vim -y
  elif [ $p -eq 6 ];
  then
      echo "------Install Browser------------ "
      wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
      sudo dpkg -i google-chrome-stable_current_amd64.deb

  elif [ $p -eq 7 ];
  then
      echo "------Install Zulip------------ "
      sudo curl -fL -o /etc/apt/trusted.gpg.d/zulip-desktop.asc \
         https://download.zulip.com/desktop/apt/zulip-desktop.asc
      echo "deb https://download.zulip.com/desktop/apt stable main" | \
      sudo tee /etc/apt/sources.list.d/zulip-desktop.list
      sudo apt update
      sudo apt install zulip
  elif [ $p -eq 8 ];
  then
      echo "------Ibus-bamboo------------ "
      sudo apt-get install ibus-bamboo
      ibus restart
  fi