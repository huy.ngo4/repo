## 1. install **neofetch**.
huy@Huy-Ngo:~$ sudo apt list -a neofetch
Listing... Done
neofetch/jammy,jammy,now 7.1.0-3 all [installed]
##########

## 2. install **neovim**.
huy@Huy-Ngo:~$ nvim --version
NVIM v0.7.2
Build type: RelWithDebInfo
LuaJIT 2.1.0-beta3
Compilation: /usr/bin/gcc-11 -U_FORTIFY_SOURCE -D_FORTIFY_SOURCE=1 -DNVIM_TS_HAS_SET_MATCH_LIMIT -DNVIM_TS_HAS_SET_ALLOCATOR -O2 -g -Og -g -Wall -Wextra -pedantic -Wno-unused-parameter -Wstrict-prototypes -std=gnu99 -Wshadow -Wconversion -Wdouble-promotion -Wmissing-noreturn -Wmissing-format-attribute -Wmissing-prototypes -Wimplicit-fallthrough -Wsuggest-attribute=pure -Wsuggest-attribute=const -Wsuggest-attribute=malloc -Wsuggest-attribute=cold -Wvla -fstack-protector-strong -fno-common -fdiagnostics-color=always -DINCLUDE_GENERATED_DECLARATIONS -D_GNU_SOURCE -DNVIM_MSGPACK_HAS_FLOAT32 -DNVIM_UNIBI_HAS_VAR_FROM -DMIN_LOG_LEVEL=3 -I/home/runner/work/neovim/neovim/build/config -I/home/runner/work/neovim/neovim/src -I/home/runner/work/neovim/neovim/.deps/usr/include -I/usr/include -I/home/runner/work/neovim/neovim/build/src/nvim/auto -I/home/runner/work/neovim/neovim/build/include
Compiled by runner@fv-az395-591

Features: +acl +iconv +tui
See ":help feature-compile"

   system vimrc file: "$VIM/sysinit.vim"
  fall-back for $VIM: "
/home/runner/work/neovim/neovim/build/nvim.AppDir/usr/share/nvim"

Run :checkhealth for more info

## 3. create new directory name: `homework`.
huy@Huy-Ngo:~$ mkdir homework

## 4. create new file name `system_information` in `homework` directory.
huy@Huy-Ngo:~$ cd homework
huy@Huy-Ngo:~/homework$ touch system_information.txt

## 5. Write down the `neofetch` information into `system_information`.
huy@Huy-Ngo:~$ cp -i ./.config/neofetch/config.conf ./homework/system_information.txt
cp: overwrite './homework/system_information.txt'? y

## 6. create directory name `system` in `homework` directory.
huy@Huy-Ngo:~/homework$ mkdir system
## 7. move file `system_information` into `system` folder.
huy@Huy-Ngo:~/homework$ ls
system  system_information.txt
huy@Huy-Ngo:~/homework$ mv system_information.txt system
huy@Huy-Ngo:~/homework$ ls
system

## 8. duplicate `system` folder.
huy@Huy-Ngo:~/homework$ mkdir B
huy@Huy-Ngo:~/homework$ cd system

huy@Huy-Ngo:~/homework/system$ cp -a ./ ../B

or

huy@Huy-Ngo:~/homework$ cp -R system test


## 9. set new folder name `archived`.
huy@Huy-Ngo:~/homework$ ls
B  system

huy@Huy-Ngo:~/homework$ mv B archived

huy@Huy-Ngo:~/homework$ ls
archived  system

## 10. create new directory name: `script` into `system`.

huy@Huy-Ngo:~/homework/system$ mkdir script


## 11. create new file name `echo`
huy@Huy-Ngo:~/homework/system$ echo > newfile.txt


## 12. write some `echo` command into `echo` file with **neovim**.
huy@Huy-Ngo:~/homework$ nvim echo.txt


## 13. set permission `execute` for `echo` file.
huy@Huy-Ngo:~/homework$ chmod 777 echo.txt
huy@Huy-Ngo:~/homework$ ls -l echo.txt


## 14. run `echo` file.
huy@Huy-Ngo:~/homework$ sudo ./echo.txt


## 15. create new folder name `command_meaning` in `system` folder.
huy@Huy-Ngo:~/homework/system$ mkdir command_meaning


## 16. writedown the user manual of any command (`man` command) you learn into file. (Ex: The `ls` manual command will be written to the file name `ls`).
## **Automatic this step will get big bonus point**


## 17. Move the `command_meaning` folder to `archived` folder.
huy@Huy-Ngo:~/homework$ ls
archived  echo.txt  filename.txt  system

huy@Huy-Ngo:~/homework$ mv ./system/command_meaning archived























