<!Để giúp bạn nghiên cứu tốt về Git về mặt lý thuyết và thực hành. Biến máy bạn thành một
Repository.
Làm cách nào? → Tạo thư mục rỗng, thực hiện lệnh command line tại thư mục đó: git init
Thư mục bạn vừa tạo đã trở thành một Repository và bắt đầu tìm hiểu Git
Online documents:
Tiếng Anh: https://git-scm.com/book/en/v2
Chú ý:
- Bạn phải cài đặt Git và thiết lập môi trường trước khi tìm hiểu về các khái niệm và
tính năng trong Git
- Sử dụng command line
- Bạn nên ghi chú lại những lệnh git đã tìm hiểu để sử dụng sau này>
1. Repository là gì? Phân biệt Remote repository và Local repository?
Repository là gì?
Khi bạn gõ git commit thì các thay đổi sẽ được lưu tại đây.
Repository được hiểu đơn giản là nơi chứa tất cả những thông tin cần thiết để duy trì và quản lý các sửa đổi và lịch sử của toàn bộ project.
Tất cả dữ liệu của Repository đều được chứa trong working directory dưới dạng folder ẩn có tên là .git
Repository của Git được phân thành 2 loại là remote repository và local repository.

- Remote repository
Remote repository là repository để chia sẻ giữa nhiều người và bố trí trên server chuyên dụng.

- Local repository
Local repository là repository bố trí trên máy dev của bạn, dành cho một người dùng sử dụng.

2. Working directory hay Working tree là gì? Index là gì?
- Working directory hay còn gọi là working tree chính là project bạn đang code, hay đây là folder ở trên máy tính của bạn, có thể là sau khi bạn clone một repository nào đó trên github (hoặc cái khác cũng được) về, hoặc đơn giản hơn là tạo thư mục rồi gõ git init ở trong đó. Nói chung là sẽ có folder .git bên trong.
Mọi thao tác thêm, sửa, xóa file bạn làm trên folder đó là bạn đang thực hiện trên working directory

- Staging area hay còn gọi là index, sau khi bạn hoàn tất các thao tác trên file của mình ở working directory, bạn sẽ mong muốn là commit và push lên git repository ở remote. Nhưng để đi đến commit thì bạn cần phải thêm các file đã chỉnh sửa vào trong staging area bằng command git add trước, hay người ta thường gọi là stage các file đó và các file được stage sẽ có trạng thái là staged và ngược lại sẽ là unstaged.

Tóm lại, Staging area chính là nơi lưu trữ các thay đổi trước khi commit. Khi bạn gõ git add thì các thay đổi sẽ được lưu tại đây. Những file nằm trong staging area là những file sẽ được commit tới repository 
3. Clone là gì? Để thực hiện clone cần yếu tố gì và sử dụng câu lệnh gì?
Clone la tạo một bản sao của repo tại một thư mục mới, tại một vị trí khác.
Yếu tố cần:
Cân phải có kho lưu trữ gốc(original repository) có thể được đặt trên hệ thống tệp cục bộ hoặc trên các giao thức được hỗ trợ có thể truy cập được trên máy từ xa. Lệnh git clone sao chép kho lưu trữ Git hiện có.

- Sử dụng git clone
Copy Repo từ thư mục này sang thư mục khác
Trên máy của bạn có một Git Repo ở đường dẫn path-git, bạn có thể copy sang vị trí khác bằng lệnh:

	git clone path-git
Có thể chỉ rõ thư mục cần copy về thay vì tại thư mục hiện tại

	git clone path-git path-des
Copy Repo từ server về bằng giao thức ssh
Vị dụ Server có kết nối ssh: user@host, trên đó lưu một Repo ở đường dẫn /path/to/repo, thì có thể copy về bằng lệnh

	git clone user@host:/path/to/repo.git
Copy Repo bằng giao thức https
Nhiều dịch vụ Git cung cấp kết nối bằng giao thức (https) ví dụ GitHub, GitLab thì copy về bằng lệnh:

	git clone url-repo


4. Phân biệt các trạng thái của file sau:
	■ Committed       =>  tệp đã commit và sẽ được đẩy đến kho lưu trữ từ xa.
	■ Unmodified - Modified    => Đây là bất kỳ tệp nào chưa được sửa đổi kể từ lần cam kết cuối cùng - Khi bạn chỉnh sửa tệp, Git coi chúng là đã được sửa đổi, vì bạn đã thay đổi chúng kể từ lần cam kết cuối cùng của bạn
	■ Untracked           => bất kỳ tệp nào trong thư mục làm việc(working directory) không có trong  ảnh chụp nhanh cuối cùng(last snapshot) và không có trong staging area. 
	■ Unstaged – Staged    
- Có nghĩa là tệp được theo dõi đã được sửa đổi trong thư mục làm việc nhưng chưa được phân giai đoạn(staged)
- Phiên bản mới nhất của tệp sau khi dùng lệnh git add

	■ Tracked    => Các tệTp được theo dõi là các tệp có trong ảnh chụp nhanh cuối cùng (last snapshot) và có trong staging area. Đây là những tệp mà Git biết.
	
	
** Và sử dụng lệnh gì để kiểm tra trạng thái file hiện tại?
	git status filename
5. Cách để thêm (add) một file vào trạng thái Staged là gì? Cách hủy bỏ chức năng add
vừa rồi là gì? (undo add)
	git add <path/to/file>
	git reset <file> : để đưa file về unstaged


6. Lệnh git status -s để xem trạng thái của file dưới dạng ngắn gọn. Giải thích ý nghĩa
của các ký hiệu trạng thái trước tên file:
	M   a.txt       =>   file có sửa đổi so với lần commit cuối cùng,và được add vào staging area 
	 M  b.txt       =>   file có sửa đổi so với lần commit cuối cùng,và được add vào staging area 
	MM  c.txt      =>    file có sửa đổi so với lần commit cuối cùng,và được add vào staging area => sao đó có sửa đổi trong cây làm việc nhưng thay đổi đối với tập không phải là theo giai đoạn(not staged)
	A   d.txt       =>   tệp chưa được theo dõi =>tệp đã được thêm vào vùng tổ chức(staging area)
	AM  e.txt      =>   tệp chưa được theo dõi =>tệp đã được thêm vào vùng tổ chức(staging area) => tệp đã được thêm vào vùng tổ chức, sau đó đã có chỉnh sửa file,nội dung của tệp đã được sửa đổi trong cây làm việc(working tree), nhưng các thay đổi đối với tệp không phải là theo giai đoạn(not staged)
	?? f.txt      => Tệp chưa được theo dõi(untracked file)

#‘ ’	Unmodified
#M	Modified
#A	Added
#D	Deleted
#R	Renamed
#C	Copied
#U	Updated but unmerged]
#??     untracked file

7. Giải thích ý nghĩa của lệnh git diff và git diff --staged
git diff =>  xem sự thay đổi của thư mục làm việc mà thay đổi không theo giai đoạn với commit cuối
git diff --staged =>   Kiểm tra sự thay đổi của index (staging) với commit cuối
8. Commit là gì? Câu lệnh đơn giản để thực hiện Commit là gì?
Commit nghĩa là một action để Git lưu lại một snapshot của các sự thay đổi trong thư mục làm việc. Và các tập tin, thư mục được thay đổi đã phải nằm trong Staging Area. Mỗi lần commit nó sẽ được lưu lại lịch sử chỉnh sửa của code kèm theo tên và địa chỉ email của người commit.

	git commit -m "Text commit"
9. Giải thích ý nghĩa của lệnh sau:
	■ git rm -f file.txt        =>     xóa tệp khỏi khu vực staging/index và thư mục làm việc 
	■ git rm --cached file.txt   =>  xóa một tệp khỏi khu vực staging/index
(biết rằng file.txt không phải là trạng thái untracked)
10. Nêu các cách thay đổi tên file và di chuyển file?
	git mv options oldFilename newFilename
	[-f] : ghi đè ngay cả khi tệp đích đã tồn tại
	
	git mv filename foldername
	
11. Mục đích Ignore là gì? Nêu cách Ignore file hoặc folder?
Ignored: Các tệp,foder thuộc trường hợp yêu cầu Git bỏ qua.
Tạo file .gitignore, là file định nghĩa những file, folder nào sẽ bị bỏ qua (ignore) và không thêm nó vào git,
sẽ có tác dụng cho những file/folder cùng cấp với nó và trở xuống (các folder con của folder chứa nó). Thông thường, mỗi dự án ta chỉ cần một file gitignore đặt ở thư mục gốc là đủ. Viết vào file đó  danh sách những file,foder sẽ bỏ qua theo các pattern format.  
Đó mới chỉ là điều kiện cần, điều kiện đủ là files không có trong git cache nữa thì git nó mới bỏ qua, chứ files mà nằm trong git cache thì .gitignore sẽ vô tác dụng.
	git rm -r --cached .
12. Giải thích ý nghĩa của lệnh sau:
	■ git log       => Hiển thị tất cả nhật ký commit
	■ git log --oneline    =>  được sử dụng để hiển thị đầu ra dưới dạng một commit trên mỗi dòng. Nó cũng hiển thị đầu ra một cách ngắn gọn như bảy ký tự đầu tiên của commit SHA và tin nhắn commit.
	■ git show       =>  dùng để xem chi tiết một commit cuối cùng của nhánh đó.
	■ gitk           =>  Thực thi lệnh gitk sẽ khởi chạy giao diện người dùng Gitk trông giống như sau: Khung phía trên bên trái hiển thị các cam kết đối với kho lưu trữ, với phần mới nhất ở trên cùng. Phía dưới bên phải hiển thị danh sách các tệp bị ảnh hưởng bởi cam kết đã chọn.
13. Giải thích ý nghĩa của lệnh sau:
	■ git remote add origin <url>    một điều khiển từ xa mới được tạo có tên là origin.
	■ git remote add <shortname> <url>  : Để thêm một kho lưu trữ Git từ xa mới làm tên viết tắt, 
	■ git remote -v  : Hiển thị URL của kho lưu trữ từ xa khi liệt kê các kết nối từ xa hiện tại của bạn. Theo mặc định, việc liệt kê các kho lưu trữ từ xa chỉ hiển thị cho bạn tên viết tắt của chúng
	■ git remote remove <short name> : Lệnh loại bỏ từ xa git xóa một điều khiển từ xa khỏi kho lưu trữ cục bộ.
	
14.Branch là gì? Mục đích sử dụng Branch là gì?
Một nhánh đại diện cho một dòng phát triển độc lập. Các nhánh đóng vai trò như một phần trừu tượng cho quá trình  edit/stage/commit . Bạn có thể coi chúng như một cách để yêu cầu một thư mục làm việc, khu vực tổ chức và lịch sử dự án hoàn toàn mới. Branch đã phân nhánh sẽ không ảnh hưởng đến branch khác nên có thể tiến hành nhiều thay đổi đồng thời trong cùng 1 repository.Hơn nữa, branch đã phân nhánh có thể chỉnh sửa tổng hợp lại thành 1 branch bằng việc hợp lại (merge) với branch khác

15. Nêu cách thực hiện:
	■ Liệt kê các branch hiện tại    => git branch
	■ Tạo mới một branch              => git branch <branchname> 
	■ Chuyển tới một branch để làm việc     => git checkout <branchname>
	■ Xóa branch                      =>   git branch -d <branchname> 
	■ Đổi tên branch                 =>    git branch -m <old> <new>
	
	Tạo mới và chuyển nhánh => git checkout -b <new-feature>
16. Merge là gì? Nêu cách thực hiện
Git Merge là một lệnh dùng để hợp nhất các chi nhánh độc lập thành một nhánh duy nhất trong Git.
Khi sử dụng lệnh hợp nhất trong Git, chỉ có nhánh hiện tại được cập nhật để phản ánh sự hợp nhất, còn nhánh đích sẽ không bị ảnh hưởng.

Việc hợp nhất trong Git sẽ diễn ra theo quy trình sau:

	Thực thi git status để đảm bảo rằng nó đang trỏ HEAD đến đúng nhánh nhận hợp nhất. Chạy git checkout để chuyển sang nhánh nhận.
	Tìm nạp các remote commit mới nhất bằng lệnh git fetch. Đảm bảo rằng chi nhánh nhận và chi nhánh hợp nhất được cập nhật những thay đổi từ xa mới nhất.
	Kết thúc quá trình tìm nạp, sử dụng lệnh git pull để cập nhật nhánh chính.
	Cuối cùng, thực hiện lệnh git merge <branch name>, trong đó branch name là tên tên của nhánh sẽ được ghép vào nhánh nhận.

17. Conflict là gì? Conflict xảy ra trong những trường hợp nào và nêu cách giải quyết?

Khi làm việc nhóm, pull, push, merge code, sẽ có trường hợp hai người cùng chỉnh sửa một file, một dòng code, khi đồng bộ sẽ xảy ra xung đột (conflict).

” xung đột hợp nhất ” hoàn toàn có thể xảy ra trong quy trình tích hợp những commit từ một nguồn khác .Tuy nhiên, hãy nhớ rằng ” tích hợp ” không chỉ số lượng giới hạn ở ” hợp nhất những nhánh “. Nó cũng hoàn toàn có thể xảy ra khi phục sinh hoặc phục sinh tương tác, khi triển khai một cherry-pick hoặc pull, hoặc thậm chí còn khi vận dụng lại một Stash .Tất cả những hành vi này thực thi một số ít loại tích hợp – và đó là khi xung đột hợp nhất hoàn toàn có thể xảy ra .
Cách giải quyết xung đột:
#Hoàn tác xung đột trong Git và bắt đầu lại khi giải quyết đã đi vào ngõ cụt
Với mục đích này, hầu hết các lệnh đi kèm với một tùy chọn --abort, ví dụ 
	git merge --abort và 
	git rebase --abort

#xem xung đột thực sự trông như thế nào dưới code, quyết định hành động mã nào thực sự đúng chuẩn.

18.Bạn có thể làm việc trên nhánh master không? Vì sao? Nếu bạn được giao làm một
tính năng trong game, bạn sẽ sử dụng quy trình làm việc của git như thế nào?

19. So sánh lệnh Fetch và Pull?
Trong Git, Git Pull và Git Fetch là 2 lệnh có chức năng tương đồng với nhau. Cả hai đều được sử dụng để tải xuống dữ liệu mới từ một Remote repository. Tuy nhiên, Git Fetch thường được coi là một phiên bản an toàn hơn của Git Pull.

Git Fetch chỉ tải xuống nội dung từ Remote repository mà không làm thay đổi trạng thái của Local repository. Trong khi đó, Git Pull sẽ tải xuống nội dung và cố gắng thay đổi trạng thái của Local repository cho phù hợp với nội dung đó. 
Bạn có thể kiểm tra các remote branch bằng các lệnh git checkout và git log thông thường. Nếu bạn chấp nhận những thay đổi của một remote branch, bạn có thể hợp nhất nó thành một local branch bằng cách merge git bình thường. 

20. Push là gì?
Lệnh git push được sử dụng để đẩy các commit mới ở máy trạm (local repo) lên server (remote repo). Nguồn để đẩy lên là nhánh mà con trỏ HEAD đang trỏ tới (nhánh làm việc).

Đích mà nó đẩy lên (ghi vào nhánh nào) có thể chỉ định trong tùy chọn của lệnh, tuy nhiên cũng không cần chỉ định nếu có thiết lập liên hệ giữa nguồn và đích trước. Lệnh git push cũng có thể xóa đi các nhánh của remote

Một số tham số hay dùng như:

--all đẩy tất cả các nhánh lên server
--tags đẩy tất cả các tag lên server
--delete xóa một nhánh chỉ ra trên server
-u đẩy và tạo một upstream (luồng upload tương ứng với nhánh của local), hay sử dụng cho lần đầu đẩy lên server
21. Merge request là gì? Sử dụng khi nào?
Là một yêu cầu từ một người nào đó để hợp nhất trong mã từ nhánh này sang nhánh khác. 
Yêu cầu hợp nhất (MR) là cách bạn kiểm tra các thay đổi mã nguồn thành một nhánh. Khi bạn mở một yêu cầu hợp nhất, bạn có thể trực quan hóa và cộng tác với các thay đổi mã trước khi hợp nhất. Các yêu cầu hợp nhất bao gồm:

	Mô tả yêu cầu.
	Thay đổi mã và đánh giá mã nội tuyến.
	Thông tin về đường ống CI / CD.
	Một phần bình luận cho các chủ đề thảo luận.
	Danh sách các cam kết.
22. Revert là gì? Nêu cách hủy bỏ (revert) những gì đang thay đổi của một file, một số
file, toàn bộ dự án?
Trong Git, thuật ngữ hoàn lại, hoàn lại, hồi lại(revert) được sử dụng để hoàn lại một số thay đổi. Lệnh git revert được sử dụng để áp dụng thao tác hoàn lại. Nó là một loại lệnh hoàn tác. Tuy nhiên, nó không phải là một thay thế huỷ bỏ truyền thống. Nó không xóa bất kỳ dữ liệu nào trong quá trình này, thay vào đó, nó sẽ tạo ra sự thay đổi với tác dụng ngược lại, đó là hoàn lại commit đã chỉ định nào đó. Nói chung, git revert là một commit.

	git checkout file_name.rb 
	

23. Giả sử có các commit theo thứ tự thời gian từ mới đến cũ:

	commit_Id_4  
	commit_Id_3
	commit_Id_2
	commit_Id_1T
	■ Làm thế nào để revert tới commit_Id_2?    git revert commit_Id_2
	■ Làm thế nào để revert những gì đã thay đổi trong commit_Id_1?    git revert commit_Id_2 
24. Phân biệt
	■ git reset --hard    : hủy(undo) commit => nội dung của commit  bị hủy hoàn toàn
	■ git reset --soft    : hủy(undo) commit  => nội dung của commit vãn lưu lại trong staging
	■ git reset –mix      :  hủy commit => các file trở lại local directory 
	■ git reset		: undo lenh add tat ca cac file

25. Giả sử Repository của bạn có 3 branch: master, issue1, issue2. Bạn đang ở issue1 để
làm việc và đang có những sự thanh đổi dang dở. Trong khi đó issue2 cần ưu tiên đểirectory
hoàn thiện trước. Làm thế nào để lưu lại những gì đang làm ở issue1 và chuyển qua
issue2, sau đó trở lại issue1 để tiếp tục với những gì đang làm?

Tạo một commit ở nhánh issue1, sBau đó thì chuyển nhánh sang issue2 làm việc
26. Giải thích ý nghĩa của lệnh sau:
	■ git clean -f       => để buộc xóa tệp không theo dõi;
	■ git clean -f -d    => để xóa các thư mục không được theo dõi;
	■ git clean -n -d    => Để xem tất cả các tệp chưa được theo dõi và các thư mục chưa được theo dõi sẽ bị xóa, chúng ta có thể sử dụng lệnh git clean với -n và -d. Điều này liệt kê tất cả các tệp và thư mục sẽ bị xóa bằng lệnh git clean
27. Rebase là gì? Nên sử dụng rebase trong trường hợp nào?
Rebase sẽ lấy tất cả thay đổi của một nhánh khác để apply cho nhánh đang đứng.
Bạn đang làm việc trên một branch feature và cùng lúc đó, branch master có nhiều sự phát triển, tahy đổi. Bạn muốn cập nhật branch của mình theo branch master, nhưng không lưu lại các thông tin cập nhật trong branch history. Đây chính là lúc bạn cần sử dụng Git Rebase


