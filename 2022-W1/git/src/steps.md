Step 1: Go to your GitLab link and create a repository with name GitPractice
New Project -> Create blank project
Step 2: Get your Repository

1. git clone git@gitlab.com:huy.ngo4/gitpractice.git
2. git branch setup_trunk
   git checkout  setup_trunk
   git add .
   git commit -m "fetch: add foder trunk-main"
   git push -u origin setup_trunk
3.  
4. Rebase
	git clone git@gitlab.com:huy.ngo4/gitpractice.git
	git checkout -b feature_a
	git rebase origin/setup_trunk
	
5. Revert
	git revert HEAD
6. [Modify]
	git add .
	git commit -m "feat: add text to the end of the file applyPatches.bat"
7. [Ignore]
git reset
git add .
git commit -m "feat: create file gitignore"
git push origin setup_trunk
8. [Submodule]
git submodule add https://gitlab.com/gitlab-examples/docker
git submodule add https://gitlab.com/gitlab-examples/ssh-private-key
git add .
git commit -m "Add a Git Submodule"
git push origin setup_trunk
9. [Create Tag]
git tag -a 0.0.1 -m "setup trunk"
10. [Merge request]
11. [Accept merge request]
12. [Resolve conflict 1]
git merge origin/feature_a
git commit -m "Resolve conflict 1"

git push origin feature_b
13.[ Update]
git add .
git commit -m "Modify file another_data.txt"
git push origin feature_b
14. [Merge to master]

git checkout main
git merge feature_b
git commit -m "Merge feature/b branch into master branch"
git push origin main
15. [Move]
mv data/csv/* data/text/* data/file
rm -r data/csv
rm -r data/text
16. [Create Patch]
git rebase feature_a main
git reset --hard





