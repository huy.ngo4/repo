from package import *
from pprint import pprint
import time
import math

# pprint(dir())
#da import decorator o package

@calculate_time
def factorial(num):
    # sleep 2 seconds because it takes very less time
    # so that you can see the actual difference
    time.sleep(2)
    print(math.factorial(num))

if __name__ == '__main__':
    # calling the function.
    factorial(3)
