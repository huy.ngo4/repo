from .sub_package_1 import *
from .sub_package_2 import *

#import tat ca nhung gi co trong all o sub_package-1 va package_2
__all__ = [
    *sub_package_1.__all__,
    *sub_package_2.__all__
]
