

__all__ = [
    'module_2_function'
]

def module_2_function():
    print("Module 2 Function")

def private_test_module_2():
    print("private test of module 2")
