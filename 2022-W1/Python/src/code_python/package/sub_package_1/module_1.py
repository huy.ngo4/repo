import time


__all__ = [
    'module_1_function', 'calculate_time'
]

def module_1_function():
    print("Module 1 Function")

def private_test_module_1():
    print("private test of module 1")

#Dinh nghi ham decorator
def calculate_time(func):
    # added arguments inside the inner1,
    # if function takes any arguments,
    # can be added like this.
    def inner1(*args, **kwargs):
        # storing time before function execution
        begin = time.time()

        func(*args, **kwargs)

        # storing time after function execution
        end = time.time()
        print("Total time taken in : ", func.__name__, end - begin)

    return inner1
