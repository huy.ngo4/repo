Phần 1: Giới thiệu

1.0. Giới thiệu về Python
1.1. Python là ngôn ngữ lập trình thể hệ mới, các tính năng cực chất của nó và lý do nó hot?
1.2. Các ưu điểm và ứng dụng của ngôn ngữ Python hiện nay
1.3. Hướng dẫn tải, cài đặt và chạy chương trình Python”
1.4. Namespace và Phạm vi trong Python
1. Namespace và Phạm vi trong Python
Namespace là hệ thống có một tên duy nhất cho mỗi đối tượng trong Python. Một đối tượng có thể là một biến hoặc một phương thức. Bản thân Python duy trì một namespace dưới dạng từ điển(dictionary) trong Python.
Các loại namespace:
Namespace tích hợp sẵn bao gồm namespace toàn cục và namespace toàn cục bao gồm các namespace cục bộ.
1.5. Cấu trúc của một chương trình trong Python
1.6. Python là ngôn ngữ lập trình thể hệ mới, các tính năng cực chất của nó và lý do nó hot?
1.7. Cách gán giá trị cho các biến trong Python qua ví dụ
1.8. Cách keyword thường cần biết trong python - phần 1
1.9. Cách keyword thường cần biết trong python - phần 2
1.10. Làm cách nào để kiểm tra xem một chuỗi nào đó có phải là một từ khóa hợp lệ trong Python hay không?
1.11. Làm thế nào để in mà không có dòng mới trong Python?
1.12. Tìm hiểu về câu lệnh, thụt lề, và comment trong python
2.0. Nhập dữ liệu đầu vào trong Python
2.1	Lấy dữ liệu đầu vào từ chương trình console trong Python
2.2	Lấy nhiều dữ liệu đầu vào từ người dùng trong Python
2.3	Phương thức nhập liệu trong Python để nâng cao hiệu suất
2.4	Lỗ hổng trong hàm input() trong Python 2.x
2.5	Đầu ra sử dụng hàm print() trong Python
2.6	Làm thế nào để in mà không có dòng mới trong Python?
2.7	Tham số end ở hàm print() trong Python
2.8	Tham số sep ở hàm print() trong Python
2.9	Định dạng output trong Python
3.0	Bộ 3 (Strings, Lists, Tuples, Iterations) trong Python
3.1	String trong Python
3.2	List trong Python
3.3	Tuple trong Python
3.4	Set trong Python
3.5	Kiểu Dictionary(Từ điển) trong Python
3.6	Array trong Python
4.0	Biến, biểu thức,các điều kiện và hàm trong python
4.1	Giá trị tối đa có thể có của một số nguyên trong Python là gì?
4.2	Biến toàn cục và biến cục bộ trong Python
4.3	Đóng gói và giải nén đối số trong Python
4.4	Tham số kết thúc của hàm print() trong Python
4.5	Chuyển đổi kiểu trong Python
4.6	Đối tượng Byte vs String trong Python
4.7	In một biến và nhiều biến trong Python
4.8	Hoán đổi hai biến trong một dòng trong Python
4.9	Các biến private trong Python
4.10	__name__ (Một biến đặc biệt) trong Python
__name__ là một trong những biến đặc biệt. Nếu tệp nguồn được thực thi dưới dạng chương trình chính, trình thông dịch đặt biến __name__ có giá trị “__main__”. Nếu tệp này đang được import từ một mô-đun khác, __name__ sẽ được đặt thành tên của mô-đun.


5.0	Tìm hiểu về If else trong python
5.1	Vòng lặp while trong python
5.2	Vòng lặp trong Python
5.3	Áp dụng vòng lặp trong việc in tam giác qua các ví dụ
5.4	Cách tạo một Switch Case trong Python
5.5	Hiểu rõ về Kỹ thuật lặp trong Python
5.6	Sử dụng iterator để lặp hiệu quả trong Python
5.7	Sử dụng câu lệnh điều kiện else với vòng lặp for trong python
5.8	Hiểu rõ về Kỹ thuật lặp trong Python
5.9	So sánh Range() so với xrange() trong Python
5.10	Hiểu về Itertools trong Python
#Terminating iterators
Trình lặp kết thúc được sử dụng để làm việc trên các chuỗi đầu vào ngắn và tạo ra đầu ra dựa trên chức năng của phương thức được sử dụng.

#islice(iterable, start, stop, step): Trình lặp này in chọn lọc các giá trị được đề cập trong vùng chứa có thể lặp lại của nó được truyền dưới dạng đối số. Trình lặp này nhận 4 đối số, vùng chứa có thể lặp lại, vị trí bắt đầu, vị trí kết thúc và bước.
#filterfalse (func, seq): Như tên cho thấy, trình lặp này chỉ in các giá trị trả về false cho hàm đã truyền.
	li = [2, 4, 5, 7, 8]
	print (list(itertools.filterfalse(lambda x : x % 2 == 0, li)))
	
	>> output
	The values that return false to function are : [5, 7]
#starmap (func., tuple list): Trình vòng lặp này nhận một hàm và danh sách tuple làm đối số và trả về giá trị theo hàm từ mỗi bộ danh sách.
	li = [ (1, 10, 5), (8, 4, 1), (5, 4, 9), (11, 10, 1) ]
	# selects min of all tuple values
	print (list(itertools.starmap(min, li)))
	
	>>ouput
	The values acc. to function are : [1, 1, 4, 1]	

5.9	Tìm hiểu __iter__() và __next__() | Chuyển đổi một đối tượng thành một trình lặp qua ví dụ
#Hàm __iter __ () trả về một đối tượng trình lặp đi qua từng phần tử của đối tượng đã cho. Phần tử tiếp theo có thể được truy cập thông qua hàm __next __ ()
6.0	Các toán tử cơ bản trong Python
6.1	Toán tử phủ định logic (Logical Not) và Toán tử phủ định chuỗi bits (Bitwise Not)
6.2	Toán tử ba ngôi trong Python
#Tenary operators (Toán tử ba ngôi) còn được gọi là các biểu thức điều kiện
Phương pháp đơn giản để sử dụng tenary operator
		a, b = 10, 20
		min = a if a < b else b 
Phương pháp trực tiếp bằng cách sử dụng tuple, kiểu Dictionary và lambda
		# (if_test_false,if_test_true)[test] 
		print((lambda: b, lambda: a)[a < b]())   => lambda
		>>output   =>>>>>>>  10
	
6.3	Toán tử chia trong Python
6.4	Nạp chồng toán tử trong Python
Một toán tử hoặc một hàm đã được tích hợp sẵn, nhưng chúng sẽ có những chức năng khác nhau dành cho các đối tượng thuộc các kiểu class khác nhau, điều này được gọi là nạp chồng toán tử (Operator Overloading).
	# + operator for different purposes.
	print(1 + 2) 
	# concatenate two strings 
	print("Cafedevn"+"For")  
Chúng ta có thể nạp chồng tất cả các toán tử hiện có, nhưng không thể tạo ra toán tử mới. Để thực hiện nạp chồng toán tử, Python cung cấp một số hàm đặc biệt hoặc hàm ma thuật (magic function), có khả năng tự động được gọi khi nó được liên kết (associated) với một toán tử cụ thể nào đó. 

6.5	Any và All trong Python
#Hàm any() là tương đương với việc thực hiện một cuỗi các phép logic OR liên tiếp nhau
#Hàm all() là tương đương với việc thực hiện một cuỗi các phép logic AND liên tiếp nhau, trên một tập hợp các giá trị boolean được cung cấp

6.6	Toán tử Inplace và toán tử Standard trong Python
6.7	Các hàm toán tử trong Python(Phần 1)
6.8	Các hàm toán tử trong Python(Phần 2)
6.9	Toán tử In-place trong Python | Phần 1 (iadd(), isub(), iconcat()…)
6.10	Toán tử In-place trong Python | Phần 2 (ixor(), iand(), ipow()…)
6.11	Các Cổng Logic trong Python
6.12	a += b không phải luôn luôn là a = a + b
6.13	Sự khác nhau giữa == và toán tử is trong Python
#Toán tử == sẽ so sánh giá trị của hai toán hạng xem chúng có bằng nhau hay không. Trong khi đó toán tử is sẽ kiểm tra liệu rằng cả hai toán hạng có đang tham chiếu tới cùng một đối tượng hay không
6.14	Các toán tử kiểm tra sự tồn tại và toán tử kiểm tra kiểu dữ liệu | in, not in, is, is not
6.15	Kết hợp các toán tử so sánh trong Python
7.0	Hàm trong Python
#Hàm là một tập các câu lệnh mà nhận vào các inputs – dữ liệu đầu vào, thực hiện một số tính toán cụ thể và tạo ra output – kết quả.

7.1	Phương thức của Class và Phương thức static trong Python
#Class Method – Các hàm/phương thức thuộc về class
Một class method sẽ nhận vào chính class (mà nó nằm bên trong class đó) làm đối số ngầm định đầu tiên (implicit first argument), 
#Static Method
Trong Python, các Static Method sẽ không nhận vào đối số đầu tiên ngầm định nào.
Chúng ta sử dụng decorator @classmethod trong Python để tạo ra một class method (hàm thuộc về class) và sử dụng decorator @staticmethod để tạo ra một static method (hàm tĩnh) trong Python
#Class Method/Static Method tạo ra gì:
Chúng ta thường sử dụng Class Method để tạo ra các Factory Method (hàm chế tạo). Các Factory Methods sẽ trả về class object – đối tượng của class (cách thức hoạt động này thì giống với hàm constructor) cho các trường hợp sử dụng khác nhau. Dùng nạp chồng

Bên cạnh đó, chúng ta sẽ thường sử dụng các Static Method để tạo ra các utility functions – hàm tiện ích.
7.2	Cách viết một empty function – hàm rỗng trong Python và câu lệnh pass
7.3	Khi nào nên sử dụng câu lệnh yield thay vì câu lệnh return trong Python?
7.4	Việc trả về nhiều giá trị trong ngôn ngữ Python
7.5	Hàm bộ phận – Partial functions trong Python
Hàm bộ phận trong Python sẽ cung cấp các giá trị mặc định cho một số tham số của hàm, nếu chúng không có giá trị mặc định nào 
		from functools import partial 
	  
		# A normal function 
		def f(a, b, c, x): 
		    return 1000*a + 100*b + 10*c + x 
		# A partial function that calls f with 
		# a as 3, b as 1 and c as 4. 
		g = partial(f, 3, 1, 4) 
		  
		# Calling g() 
		print(g(5)) 
7.6	First Class functions – Hàm hạng nhất trong Python
#Các First class objects (đối tượng hạng nhất) trong một ngôn ngữ lập trình là những thành phần được xử lý thông suốt một cách nhất quán. Chúng có thể được lưu trữ trong các cấu trúc dữ liệu, được truyền làm đối số cho hàm, hoặc được sử dụng trong các cấu trúc điều khiển (control structures). Một ngôn ngữ lập trình được cho là có hỗ trợ các first-class functios (hàm hạng nhất) nếu nó có khả năng coi các hàm như các các first-class objects (đối tượng hạng nhất). Và  ngôn ngữ lập trình Python thì có hỗ trợ khái niệm First Class functions.
Các tính chất của hàm hạng nhất trong Python:

		– Một hàm là một thể hiện (instance) của một kiểu dữ liệu Đối tượng
		– Bạn có thể lưu giữ một hàm bên trong một biến.
		– Bạn có thể truyền một hàm vào làm tham số cho một hàm khác.
		– Bạn có thể trả về một hàm từ một hàm cụ thể.
		– Bạn có thể lưu trữ các hàm bên trong các cấu trúc dữ liệu như hash table (bảng băm), list (danh sách), v.v…
		Các tính chất của hàm hạng nhất trong Python:
		– Một hàm là một thể hiện (instance) của một kiểu dữ liệu Đối tượng
		– Bạn có thể lưu giữ một hàm bên trong một biến.
		– Bạn có thể truyền một hàm vào làm tham số cho một hàm khác.
#Các hàm là đối tượng
Các hàm bên trong ngôn ngữ lập trình Python đều là các first class objects – các đối tượng hạng nhất. chúng ta có thể gán hàm cho một biến. Việc gán này sẽ không gọi đến hàm. Nó nhận vào đối tượng hàm (function object) được tham chiếu tới bởi một biến và tạo ra một cái tên thứ hai để trỏ đến đối tượng hàm đó
		def shout(text): 
		    return text.upper() 
		  
		print shout('Hello') 
		  
		yell = shout 
		  
		print yell('Hello') 
#Các hàm có thể được truyền vào làm đối số cho hàm khác
7.7	Xử lý độ chính xác trong Python 
round(x, n)  <=>   format() <=>  Sử dụng ký hiệu “%”

		a = 3.4536
		print (round(a,2)) => 3.45
7.8	*args và **kwargs trong Python
#Cú pháp *args, ký hiệu * tức là  nhận vào một số lượng đối số thay đổi được và các đối số đều là non-keyworded, tức là chúng đều chưa được định từ khóa.
#**kwargs được tạo thành bởi kwargs đi kèm với hai dấu sao ở phía trước. Lý do phải sử dụng ** là bởi vì hai dấu sao cho phép chúng ta có thể truyền dữ liệu vào hàm thông qua các đối số từ khóa – keyword arguments, với số lượng tùy ý.

7.9	Closure trong Python
1. Hàm lồng trong Python
#Một hàm được định nghĩa bên trong một hàm khác thì được gọi là hàm lồng – nested function. 

2. Closure trong Python
#Hàm đóng là một hàm lồng nhau 
Closure là một nested function – hàm được lồng ở bên trong một/nhiều enclosing function khác (hàm mà chứa chính cái closure – nested function ở bên trong nó) mà có quyền truy cập đến một free variable – biến tự do của một/nhiều enclosing function đã thực thi xong. Ba đặc điểm của closure trong Python là:
		– Nó là một nested function – hàm được lồng ở bên trong một hàm khác

		– Nó có truyền truy cập đến các free variables – biến tự do của enclosing function chứa nó.

		– Nó được trả về từ enclosing function chứa nó.

Một free variable – biến tự do là một biến mà không bị ràng buộc trong phạm vi cục bộ. Để các closure có thể làm việc với các immutable variables – biến không thể thay đổi được chẳng hạn như kiểu number, kiểu string, chúng ta phải sử dụng từ khóa nonlocal

Closure trong Python giúp chúng ta tránh được việc phải sử dụng tới các giá trị toàn cục (global values) và tăng tính che giấu dữ liệu.
Một closure thì không giống với hàm bình thường, nó cho phép hàm có thể truy cập đến các biến đã được bắt (captured variables) bởi closure, các biến được bắt có thể chứa hoặc là các bản sao giá trị hoặc là tham chiếu, ngay cả khi hàm đó được gọi ở bên ngoài phạm vi vùng code chứa closure.

7.10	Decorator trong Python
		Trong Python, các hàm đều là các first class objects – các đối tượng hạng nhất, điều này có nghĩa là:

		– Các hàm đều là đối tượng nên chúng có thể được tham chiếu tới, được truyền cho một biến, và được trả về từ các hàm khác.

		– Các hàm có thể được khai báo bên trong một hàm khác và cũng có thể được truyền làm đối số cho một hàm khác.

		Decorator là công cụ hữu ích và mạnh mẽ trong Python bởi vì nó cho phép các lập trình viên có thể sửa đổi hành vi của hàm hoặc lớp (class). Các decorators cho phép chúng ta đóng gói một hàm để mở rộng hành vi của hàm đã được đóng gói đó, mà không cần phải chỉnh sửa nó.
		Trong Decorator, các hàm được nhận vào làm đối số cho một hàm khác, và sau đó được gọi ở bên trong hàm đóng gói đó (wrapper function)
#Decorator có thể thay đổi hành vi của hàm
def hello_decorator(func): 	  
 
	def inner1(): 
		print("Hello, this is before function execution") 

	# calling the actual function now 
	# inside the wrapper function. 
		func() 

		print("This is after function execution") 
	  
	return inner1 
  
# defining a function, to be called inside wrapper 
def function_to_be_used(): 
    print("This is inside the function !!") 
		
7.11	Các Decorators có tham số trong Python
7.12	Function Decorators trong Python
7.13	Kỹ thuật Memoization bằng cách sử dụng các Decorators trong Python
#Memoization là một kỹ thuật ghi nhớ các kết quả trung gian sao cho chúng có thể được sử dụng để tránh lặp lại việc xử lý các bài toán đã từng được giải quyết xong rồi, nhằm tăng tốc các chương trình. Nó có thể được sử dụng để tối ưu các chương trình sử dụng đệ quy. Trong Python, kỹ thuật Memoization có thể được thực hiện với sự trợ giúp của các function decorators.
7.14	Hàm help() trong Python
#Hàm help trong Python được sử dụng để hiện thị tài liệu của các mô-đun, các hàm, các class, các từ khóa, v.v…
7.15	Hàm __import() trong Python
#Hàm __import__() được tích hợp sẵn của Python có thể giúp bạn giải quyết được các băn khoăn này, nó giúp nạp các mô-đun vào dự án tại runtime.

		Cú pháp: __import__(name, globals, locals, fromlist, level)
		Giải thích các tham số:

		– name: Tên của mô-đun muốn nạp vào dự án
		– globals và locals: Các tên diễn giải
		– formlist: Các đối tượng hoặc mô-đun phụ được nạp vào (dưới dạng một danh sách)
		– level: Chỉ định liệu rằng sẽ sử dụng nạp vào tuyệt đối hay nạp vào tương đối. Mặc định là -1 (tuyệt đối và tương đối).
		
code mẫu:
	np = __import__('numpy', globals(), locals(), [], 0) 
	  
	# array from numpy 
	a = np.array([1, 2, 3]) 
	# prints the type 
	print(type(a)) 
	-------------------------------------
	
	np = __import__('numpy', globals(), locals(), ['complex', 'array'], 0) 
	comp = np.complex
	arr = np.array 
	
7.16	Hàm range() không trả về iterator (**)

7.17	Coroutine trong Python
7.18	Các hàm về bit dành cho kiểu int trong Python (bit_length, to_bytes, và from_bytes)
8.0	Python3 – Class
8.1	Lập trình hướng đối tượng trong Python | Phần 1 (lớp, đối tượng và các thành viên của lớp)
8.2	Lập trình hướng đối tượng trong Python | Phần 2 (che giấu dữ liệu và in đối tượng)
8.3	Lập trình hướng đối tượng trong Python | Phần 3 (Kế thừa, ví dụ về đối tượng, hàm issubclass() và super())
8.4	Đa hình trong Python
8.5	Class variable và Static variable trong Python
8.6	Class method và static method trong Python
8.7	Thay đổi các biến thành viên trong Python
8.8	Hàm constructor trong Python
8.9	Hàm destructor trong Python
8.10	Hàm str() và hàm repr() trong Python
8.11	Metaprogramming bằng các Meta-classes trong Python
8.12	Các thuộc tính của Lớp và Các thuộc tính của Thể hiện trong Python
8.13	Reflection trong Python
8.14	Barrier Object trong Python
8.15	Timer objects trong Python
8.16	Garbage Collection trong Python
9.0	Xử lý ngoại lệ trong Python – Exception Handling
9.1	Các ngoại lệ do người dùng tự định nghĩa trong Python
9.2	Tổng hợp ngoại lệ – Exceptions có sẵn trong Python
9.3	Khai báo các Clean Up Actions – các hành động dọn dẹp trong Python
9.4	Lỗi NZEC trong Python
9.5	Try và Except trong Python
9.6	Lỗi(Errors) and ngoại lệ(Exceptions) trong Python


Đọc thêm
https://thaitpham.com/tim-hieu-ve-iterable-iterator-generator-trong-python/ 


