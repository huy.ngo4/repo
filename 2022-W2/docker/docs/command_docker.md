#Docker
Docker là một dự án mã nguồn mở giúp tự động triển khai các ứng dụng Linux và Windows vào trong các container ảo hóa.

Docker cung cấp một lớp trừu tượng và tự động ảo hóa dựa trên Linux. Docker sử dụng những tài nguyên cô lập của Linux như cgroups, kernel, quản lý tệp để cho phép các container chạy độc lập bên trong một thực thể Linux

#Tại sao docker lại được sử dụng
+ Cải tiến cùng với sự liền mạch
Container Docker chạy mà không cần phải cải biến trên bất kỳ máy tính để bàn, trung tâm dữ liệu và môi trường đám mây nào.

+ Trọng lượng nhẹ hơn và cập nhật chi tiết hơn
Với LXC, nhiều quy trình có thể được kết hợp trong một container duy nhất. Với container Docker, chỉ một tiến trình mới có thể chạy trong mỗi container. Điều này giúp bạn có thể xây dựng một ứng dụng có thể tiếp tục chạy trong khi một trong các phần của nó bị gỡ xuống để cập nhật hoặc sửa chữa.

+ Lập phiên bản container
Docker có thể theo dõi các phiên bản của image container, quay trở lại các phiên bản trước và theo dõi ai đã tạo một phiên bản và cách thức tạo ra nó. Nó thậm chí còn có thể chỉ tải lên các delta giữa phiên bản hiện có và phiên bản mới.

+ Thư viện container được chia sẻ
Các nhà phát triển có thể truy cập sổ đăng ký mã nguồn mở chứa hàng nghìn container do người dùng đóng góp.

Docker Client
Đây là thành phần mà bạn có thể tương tác với Docker thông qua command line. Docker client sẽ gửi lệnh tới Docker Deamon thông qua REST API
Docker Engine: Đây là thành phần chính của Docker như một công cụ để đóng gói ứng dụng.
Docker Deamon: Dùng để lắng nghe các request từ Docker Client để quản lý các đối tượng như container, image, network và volume thông qua REST API.
Docker Machine: Tạo ra các docker engine trên máy chủ.
#Triển khai docker
Cài đặt docker
Tải về image, hoặc tạo image 
Tạo và chạy container

						Command
#basic 
docker version
docker -v
docker info
docker --help
docker login
---------------

Images
docker images
docker pull
docker rmi

---------------
Containers
docker ps
docker run <tên container>
docker start <tên container>
docker stop <tên container>
docker rm <tên container>

----------------
System
docker stats
docker system df
docker system prune => xoa cac container da stop,  -a : xoa tat ca cac container da dung va image khong lien ket

---------------------
#start docker and stop
sudo service docker start
sudo service docker stop - sudo systemctl stop docker.socket

#check docker run 
docker info
# xem lai
sudo usermod -a -G docker huy
---------------------------------------------------------------------
						Docker Images

1. What are images
2. How to pull images
3. How to run a container using an images
4. Basic Commands

[What are images]
Docker Images là templates(các mẫu) được sử dụng để tạo Docker container.
Về cơ bản đây là tệp có thông tin về tất cả những thứ sẽ được yêu cầu để tạo Container
Container là một thể hiện, phiên bản(instance) có thể chạy được của một Image. Ban có thể create, start, stop, move, or delete 1 container sử dụng Docker API or CLI.
Where are Images Stored
Images có thể được lưu tại local hoặc remote locations(Registries như docker hub)

	docker images --help => giup chung ta xem tat ca cac tuy chon co the su dung voi lenh docker images
	docker pull <tên image>
	docker images : liệt kê tất cả image mà chúng ta có
	docker images -q : chỉ liệt kê ID images
	docker images -f : dùng để lọc

	docker run <tên image> : tạo container 
	docker run -it <tên image> : cho chế độ tương tác và đi vào bên trong container 
	docker inspect <tên image| ID> : cung cấp tất cả chi tiết về image đó
	docker rmi <tên image | ID> : xóa đi image, nếu container vẫn chưa được xoas nó sẽ báo lôi
	docker rmi -f <tên image | ID> : xóa đi image 
	docker history <ten Image> : Hiển thị lịch sử của một Imageage
	
	docker rmi $(docker images -a -q) : xóa tất cả imagesimages
---------------------------------------------------------------------
						Docker Containers
1. What are Containers
2. How to create Contrainers
3. How to start/ stop Containers
4. Basic commands

[What are Containers]
Container là một thể hiện, phiên bản(instance) có thể chạy được của một Image. Ban có thể create, start, stop, move, or delete 1 container sử dụng Docker API or CLI.

	docker run <ten image> : tao container tu image
	docker run --name <ten tu khai bao> -it <ten image> : tao container va truy cap vao container
	docker start <ten container | ID container> : chay container
	docker stop  <ten container | ID container>  : dung container

	docker pause <ten container | ID container> : sẽ tạm dừng tất cả các tiến trình trong các container được chỉ định.
	docker unpause <ten container | ID container> : hủy bỏ tạm dừngdừng

	docker top <ten container | ID container> : Hiển thị các quy trình đang chạy của một container.
	docker stats <ten container | ID container>:  trả về luồng dữ liệu trực tiếp cho các container đang chạy.

	docker attach <ten container | ID container>: vào một container đang chạy bằng cách sử dụng ID hoặc tên của container. Điều này cho phép bạn xem đầu ra đang diễn ra của nó hoặc điều khiển 			nó một cách tương tác, như thể các lệnh đang chạy trực tiếp trong teminal của bạn.
	docker kill <ten container | ID container>:Tiêu diệt một hoặc nhiều container đang chạy.
	docker rm <ten container | ID container> : Xoa container
	
	docker rm $(docker ps -a -q) : xóa tất cả Containers
	docker rm $(docker ps -a -q -f status=exited) 
	
Lệnh này xóa tất cả các vùng chứa có trạng thái exited. Trong trường hợp bạn đang thắc mắc, -q là chỉ trả về ID số và -f đầu ra bộ lọc dựa trên các điều kiện được cung cấp. 
	docker container prune lệnh có thể được sử dụng để đạt được hiệu quả tương tự.
	
#chay 1 lenh khi khong ở tron 1 container, yêu cầu container đang chạy, cú pháp
docker exec <tên container> command
#lưu container thành image với commit, xuất image ra file
docker commit <tên container> <tên image: tên version>
docker save --output <myimage.tar> IDimage
#load file image
docker load -i <tên file>

---------------------------------------------------------------------
						Jenkins on Docker 
1. How to start Jenkins on Docker Container
2. Start and Stop Jenkins Container
3. How to set Jenkins home on Docker Volume and Host Machine

docker pull jenkins/jenkins
docker run -p 8080:8080 --name=jenkins-master -d jenkins/jenkins

---------------------------------------------------------------------
						Dockerfile
1. What is Dockerfile
2. How to create Dockerfile
3. How to build image from Dockerfile
4. Basic Commands

[Dockerfile]
Là một tệp văn bản đơn giản với hướng dẫn để xây dựng image. Cung cấp một số hướng dẫn cho trình tạo image và khi chúng ta docker build cho dockerfile => Image sẽ được tạo.
Tự động hóa tạo Docker Image 

			Step1: Tạo 1 file có tên Dockerfile , chỉnh sửa file
	FROM scratch : Tạo image hoàn toàn trống và sau đó thêm vào nó
	HOẶC FROM <tên image> : Nên dùng tạo image 

	- TÙY CHỌN: MAINTAINER huy <huy.ngo@05042000@gmail.com> 
	: TÊN NGƯỜI VIẾT VÀ GMAIL
	
	- RUN <CÂU LỆNH> 
	: sẽ được chạy khi build image
	
	- ADD <thư_mục_nguồn thư_mục_đích> 
	: sao chép
	Trong đó thư_mục_nguồn là thư mục ở máy chạy Dockerfile, chứa dữ liệu cần thêm vào. thư_mục_đích là nơi dữ liệu được thêm vào ở container.

	- VOLUME <path> 
	Chỉ thị tạo một ổ đĩa chia sẻ được giữa các container.
	- USER private
	đặt tên người dùng cho các lệnh RUN / CMD / ENTRYPOINT sau.
	- WORKDIR <path_current_dir>
	Thiết lập thư mục làm việc.
	
	- ENV <biến giá_trị> 
	Chỉ thị này dùng để thiết lập biến môi trường, như biến môi trường PATH ..., tùy hệ thống hay ứng dụng yêu cầu biến môi trường nào thì căn cứ vào đó để thiết lập.
	
	- EXPOSE <port>
	Để thiết lập cổng mà container lắng nghe, cho phép các container khác trên cùng mạng liên lạc qua cổng này hoặc ánh xạ cổng host vào cổng này.
	
-------------
	CMD cho phép ta set default command, có nghĩa là command này sẽ chỉ được chạy khi run container mà không chỉ định một command.

Nếu docker run với một command thì default command sẽ được ignore. Nếu dockerfile có nhiều hơn một lệnh CMD thì tất cả sẽ bị ignore ngoại trừ lệnh CMD cuối cùng.

CMD có 3 dạng form:
	- CMD ["executable", "param1", "param2"]   (exec form)
	- CMD ["param1", "param2"]  (đặt các tham số mặc định cho ENTRYPOINT ở dạng exec form)
	- CMD command param1 param2   (shell form)
	
	
	Cách thứ 2 được sử dụng cùng với ENTRYPOINT trong exec form. Nó set default parameters được thêm vào ENTRYPOINT nếu container chạy mà không truyền đối số nào, ngược lại nó sẽ bị ignore.Ví dụ sử dụng đoạn mã dưới trong Dockerfile. 
	FROM ubuntu:latest
	CMD echo "Xin chào các bạn"
Chúng ta build image từ Dockerfile và khởi tạo container từ image vừa tạo docker run -it <tên_image> sẽ ra kết quả là:

	Xin chào các bạn
nhưng khi chạy docker run -it <tên_image> /bin/bash, CMD sẽ bị ignored và bash sẽ được thay thế, output:

Kết quả không có gì được in ra cả.
---------------------
	ENTRYPOINT
Lệnh ENTRYPOINT cho phép ta cấu hình container sẽ chạy dưới dạng thực thi. Nó tương tự như CMD, vì nó cũng cho phép ta chỉ định một lệnh với các tham số. Sự khác biệt là lệnh ENTRYPOINT và các tham số không bị ignore khi Docker container chạy.

ENTRYPOINT có 2 dạng form:

	- ENTRYPOINT ["executable", "param1", "param2"] (exec form)
	- ENTRYPOINT command param1 param2 (shell form)
	Exec form của ENTRYPOINT cho phép ta đặt các lệnh và tham số và sau đó sử dụng một trong hai dạng : một là CMD để đặt các tham số bổ sung, hai là các đối số ENTRYPOINT (luôn được sử dụng), trong khi các đối số CMD có thể được ghi đè bằng các tham số dòng lệnh được cung cấp khi Docker container chạy. Ví dụ sử dụng đoạn mã dưới trong dockerfile
	
	FROM ubuntu:latest
	ENTRYPOINT ["/bin/echo", "Xin chào"]
	CMD ["các bạn"]
	
Build image và chạy thử câu lệnh sau docker run -it <tên_image> sẽ cho kết quả là:

	Xin chào các bạn
Bây giờ lại chạy với lệnh sau docker run -it <tên_image> Nguyễn Văn A sẽ cho kết quả là:

	Xin chào Nguyễn Văn A
Shell form của ENTRYPOINT ignore tất CMD và các tham số dòng lệnh
	
			Step2: Buil file dockerfile
	- docker build -t imageName:tagName "vi tri luu tep dockerfile da viet ở trên"
	- docker build -t imageName:tagName -f "tên tệp" "thư mục"

	ví dụ:
	docker build -t imageName:tagName -f dockerfiile .  : . nghĩa là thư mục hiện tại
			
			Step3: Tạo Container
	- docker run imageID
	Tạo - chạy một container từ image mới
	ví dụ:
		docker run -it --name mywebserver -p 8888:80 -h firstserver i-firstserver:version1

	
*** 
lưu ý: Mặc dù ADD và COPY giống nhau về mặt chức năng, nói chung, COPY được ưa thích hơn. Đó là bởi vì nó minh bạch hơn ADD. COPY chỉ hỗ trợ sao chép cơ bản các tệp cục bộ vào vùng chứa, trong khi ADD có một số tính năng (như trích xuất tar chỉ cục bộ và hỗ trợ URL từ xa) không rõ ràng ngay lập tức.

---------------------------------------------------------------------
						Docker COMPOSE
						
1. What| Why -Docker Compose
2. How to install
3. How to create docker compose
4. How to use docker compose file to create services
5. Basic Command
Tham khao thêm
https://www.youtube.com/watch?v=6s9VqKyG1Ig&list=PLwJr0JSP7i8At14UIC-JR4r73G4hQ1CXO&index=12
[Docker Compose]
: là một công cụ để xác định và chạy các ứng dụng Docker sử dụng nhiều container (multi-container)
: sử dụng file <yaml> để tạo các tệp cấu hình dịch vụ  ứng dụng . Tệp cấu hình được đặt tên là docker-compose.ylm
: chúng ta có thể khởi động tất cả các dịch vụ bằng một dòng lệnh duy nhất : docker compose up
: chúng ta có thể dừng tất cả các dịch vụ bằng một dòng lệnh duy nhất : docker compose down
: có thể mở rộng quy mô các dịch vụ đã chọn bất cứ khi nào được yêu cầu 

					Step1: Install docker compose
					
sudo apt  install docker-compose
					Step2: Create docker compose file at any location on your system
docker-compose.yml
					Step 3: Check the validity of file by command
docker-compose config

					Step 4: Run docker-compose.yml file by command
docker-compose up -d

					Step 5: Bring down application by command

docker-compose down

Trợ giúp
docker-compose --help

Đề bài một máy chủ web và 4 database:
docker-compose up -d --scale database=4 => nhân 4 database

file docker-compose.yml
services:database
  database:
    image: redis
  web:
    image: nginx
    ports:
    - published: 8080
      target: 80
version: '3.9'

---------------------------------------------------------------------
						Docker Volumes
1. What are Volumes
2. How to create/list/delete volumes
3. How to attach volume to a container
4. How to share volume among container
5. What are bind mounts

[Volumes]
Docker volume là hệ thống tệp được gắn trên Docker Container để bảo toàn dữ liệu được tạo bởi container đang chạy. Các volum được lưu trữ trên host, không phụ thuộc vào vòng đời của container. Điều này cho phép người dùng sao lưu dữ liệu và chia sẻ hệ thống tập tin giữa các container một cách dễ dàng.

docker volume // get information
#Liệt kê danh sách các ổ đĩa:

docker volume ls
#Tạo một ổ đĩa:

docker volume create name_volume
#Xem thông tin chi tiết về đĩa:

docker volume inspect name_volume
#Xóa một ổ đĩa:

docker volume rm name_volume

#Xóa tất cả các ổ đĩa không được sử dụng bởi container nào:
docker volume prune

==============

**** Bind Mount: một tệp hoặc thư mục trên máy chủ được gắn vào container

#Mount một ổ đĩa vào container (--mount)

# Tạo ổ đĩa có tên firstdisk
docker volume create firstdisk

# Mount ổ đĩa vào container
# container truy cập tại /home/firstdisk

docker run -it --mount source=firstdisk,target=/home/firstdisk  ubuntu

#Chia sẻ dữ liệu giữa các Container
Có container với id hoặc name là container_first, trong đó nó có mount thư mục Host vào (hoặc đã được chia sẻ tử Container khác)

Giờ chạy, tạo container khác cũng nhận thư mục chia sẻ dữ liệu như container_first. Để làm điều đó thêm vào tham số --volumes-from container_first

Ví dụ:

docker run -it --volumes-from container_first ubuntu

docker run --name <MyJenkins1> -v myvol1://var/jenkins_home -p 9090:8080 -p 50000:50000 jenkins/jenkins


#Container - ánh xạ thư mục máy Host
Thông tin:

Thư mục cần chia sẻ dữ liệu trên máy host là: path_in_host
Khi chạy container thư mục đó được mount - ánh xạ tới path_in_container của container
Để có kết quả đó, tạo - chạy container với tham số thêm vào -v path_to_data:path_in_container

Ví dụ:

docker run -it -v /home/sitesdata:/home/data ubuntu
docker run --name <MyJenkins1> -v /User/huy/Desktop/Jenkins_Home://var/jenkins_home -p 9090:8080 -p 50000:50000 jenkins/jenkins






						Docker Network

Docker network sẽ đảm nhiệm nhiệm vụ kết nối mạng giữa các container với nhau, kết nối giữa container với bên ngoài, cũng như kết nối giữa các cụm (swarm) docker containers.

docker network ls
docker network inspect <tên network> : kiểm tra xem mạng đó đang có container nào kết nối vào 
docker inspect <tên container>|ID> :  kiểm tra xem container đang kết nôi với mạng nào
docker network create --driver bridge name-network: Tạo một bridge network với tên network là name-network.
docker network rm <tên network>
Khi tạo một container, ấn định nó nối vào network có tên là name-network thì thêm vào tham số --network name-netowrk trong lệnh docker run. Khi một container có tên name-container đã được tạo, để thiết lập nó nối vào network có tên name-network.
docker network connect name-network name-container
#nếu muốn kết nối cổng của container với máy host thì dùng -p 8888 là máy host và 80 là container
docker run -it --name B2 -p 8888:80 busyboxbusybox



-------------------
Ví dụ chạy docker
#Busybox
docker pull busybox
docker run busybox
docker run busybox echo "hello from busybox"
docker run -it busybox sh
docker rm $(docker ps -a -q -f status=exited)
docker container prune

#chạy một trang web tĩnh đơn giản

docker run --rm -it prakhar1989/static-site
Có thể tải xuống và chạy hình ảnh trực tiếp trong một lần sử dụng docker run.
--rm cờ tự động loại bỏ vùng chứa khi nó thoát và -it cờ chỉ định một thiết bị đầu cuối tương tác giúp dễ dàng xóa vùng chứa bằng Ctrl + C (trên cửa sổ).

docker run -d -P --name static-site prakhar1989/static-site
Trong lệnh trên, -d sẽ tách thiết bị đầu cuối của chúng tôi, -P sẽ xuất bản tất cả các cổng tiếp xúc với các cổng ngẫu nhiên và cuối cùng --nametương ứng với một tên mà chúng tôi muốn đặt.
có thể thấy các cổng bằng cách chạy docker port [CONTAINER] 

docker run -p 8888:80 prakhar1989/static-site
Chỉ định một cổng tùy chỉnh
---------------
Ví dụ phần network
tạo 2 container Busybox B1, B2,
Kiểm tra có kết nối với mạng Bridge không 
B2: trong var/www/httpd tạo file index.html với nội dung tùy ý














