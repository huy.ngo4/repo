from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Boolean
from sqlalchemy import Integer
from sqlalchemy import SmallInteger
from sqlalchemy import String
from sqlalchemy import ForeignKey
from sqlalchemy import func
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import relationship


Base = declarative_base()


class Category(Base):
    __tablename__ = "category"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    pets = relationship("Pet")
    __mapper_args__ = {"eager_defaults": True}


class Pet(Base):
    __tablename__ = "pet"
    id = Column(Integer, primary_key=True)

    name = Column(String)
    status = Column(String(255), nullable=False)
    photoUrls =Column(String(255), nullable=True)
    #relationship to category
    category_id = Column(Integer, ForeignKey("category.id"))
    #relationship to tag
    tags = relationship("Tag", back_populates="pet", cascade="all, delete-orphan", passive_deletes=True)
    #relationship to order
    order = relationship("Order", back_populates="pet", uselist=False)

    __mapper_args__ = {"eager_defaults": True}

    def to_dict(self):
        return {"name": self.name,
                "tags": [{"id": tag.id,
                          "name": tag.name} for tag in self.tags],
                "status": self.status,
                "category_id": self.category_id,
                "photoUrls": self.photoUrls,
                }


class Tag(Base):
    __tablename__ = "tag"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    pet_id = Column(Integer, ForeignKey("pet.id", ondelete="CASCADE"))
    pet = relationship("Pet", back_populates="tags")
    __mapper_args__ = {"eager_defaults": True}


class Order(Base):
    __tablename__ = "order"

    id = Column(Integer, primary_key=True)
    pet_id = Column(Integer, ForeignKey("pet.id"), unique=True)
    pet = relationship("Pet", back_populates="order")
    quantity = Column(SmallInteger, nullable=True)
    shipDate = Column(DateTime, server_default=func.now())
    status = Column(String(255), nullable=False)
    complete = Column(Boolean)
    __mapper_args__ = {"eager_defaults": True}

    def to_dict(self):
        return {"id": self.id,
                "petId": self.pet_id,
                "quantity": self.quantity,
                "shipDate": str(self.shipDate),
                "status": self.status,
                "complete": self.complete
                }


class User(Base):
    __tablename__ = "users"
    # __table_args__ = {"schema": "private"}
    id = Column(Integer, primary_key=True)
    username = Column(String(255), nullable=False, index=True)
    firstName = Column(String(255), nullable=True)
    lastName = Column(String(255), nullable=True)
    email = Column(String(255), nullable=False, index=True)
    password = Column(String(255), nullable=False)
    phone = Column(String(255), nullable=True)
    userStatus = Column(SmallInteger, nullable=False)
    __mapper_args__ = {"eager_defaults": True}

    def to_dict(self):
        return {"id": self.id,
                "username": self.username,
                "firstName": self.firstName,
                "lastName": self.lastName,
                "email": self.email,
                "password": self.password,
                "phone": self.phone,
                "userStatus": self.userStatus,
                }
