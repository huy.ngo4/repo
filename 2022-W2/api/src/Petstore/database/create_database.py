import asyncio
from database.model import Base

from sqlalchemy.ext.asyncio import create_async_engine


engine = create_async_engine(
        "postgresql+asyncpg://huy:Huy12345@localhost/postgresql1",
        echo=True,
)
async def create_database():
        async with engine.begin() as conn:
                await conn.run_sync(Base.metadata.drop_all)
                await conn.run_sync(Base.metadata.create_all)

        await engine.dispose()


if __name__ == "__main__":
        asyncio.run(create_database())