from sanic import Blueprint,  response
from sanic.response import json, text
from database.model import Order, Pet
from sqlalchemy import select
from sqlalchemy import delete
from datetime import datetime


storebp = Blueprint("store", url_prefix="/store")


@storebp.route("/", methods=['POST'])
async def createOrder(request):
    session = request.ctx.session

    async with session.begin():
        stmt = select(Pet).where(Pet.id == request.json['petId'])
        result = await session.execute(stmt)
        pet = result.scalar()
        if pet:
            order = Order(pet_id=request.json['petId'],
                          quantity=request.json['quantity'],
                          status=request.json['status'],
                          shipDate=datetime.strptime(request.json['shipDate'], '%Y-%m-%dT%H:%M:%S.%fZ'),
                          complete=request.json['complete'])
            session.add(order)
            return json(order.to_dict())
        else:
            return json("Pet yêu cầu không có trong cửa hàng!")


@storebp.route("/<orderId:int>", methods=['GET'])
async def getOrder(request, orderId):
    session = request.ctx.session
    async with session.begin():
        stmt = select(Order).where(Order.id == orderId)
        result = await session.execute(stmt)
        order = result.scalar()

    if not order:
        return json({
            "Code": 404,
            "Description": "Pet not found"
        })
    return json({
        "Code":200,
        "Description": "successful operation",
        "value": order.to_dict()
    })


@storebp.route("/<orderId:int>", methods=['DELETE'])
async def deleteOrder(request, orderId):
    session = request.ctx.session
    async with session.begin():

        stmt = select(Order).where(Order.id == orderId)
        result = await session.execute(stmt)
        pet = result.scalar()

        if not pet:
            return json("Không có đối tượng pet")
        else:
            stmt = (
                delete(Order).
                where(Order.id == orderId)
            )
            await session.execute(stmt)
            return json("delete Success")
#
