# api/pet/pet.py
from sanic import Blueprint,  response
from sanic.response import json, text
from database.model import Tag, Pet, Category
from sqlalchemy import select
from sqlalchemy import update
from sqlalchemy import delete
from sqlalchemy.orm import selectinload


petbp = Blueprint("pet", url_prefix="/")


@petbp.route("/", methods=['POST'])
async def create_pet(request):
    session = request.ctx.session
    async with session.begin():
        tag = Tag(name=request.json['tags'][0]['name'])
        pk = request.json['category']['id']
        stmt = select(Category).where(Category.id == pk)
        result = await session.execute(stmt)
        category = result.scalar()
        if category:
            pet = Pet(name=request.json['name'],
                      status=request.json['status'],
                      photoUrls=request.json['photoUrls'][0],
                      category_id=request.json['category']['id'],
                      tags=[tag])
            session.add(pet)
        else:
            pet = Pet(name=request.json['name'],
                      status=request.json['status'],
                      photoUrls=request.json['photoUrls'][0],
                      tags=[tag])
            category = Category(
                name=request.json['category']['name'],
                pets=[pet]
            )
            session.add(category)

    return json(pet.to_dict())


@petbp.route("/", methods=['PUT'])
async def create_pet(request):
    session = request.ctx.session
    async with session.begin():
        stmt = select(Pet).where(Pet.id == request.json['id'])
        result = await session.execute(stmt)
        pet = result.scalar()
        if pet:
            stmt = (
                update(Pet).
                where(Pet.id == request.json['id']).
                values({
                    "name": request.json['name'],
                    "status": request.json['status'],
                    "photoUrls": request.json['photoUrls'][0],
                })
            )
            await session.execute(stmt)
            return json("Câp nhập thành công")
        else:
            tag = Tag(name=request.json['tags'][0]['name'])
            pet = Pet(name=request.json['name'],
                      status=request.json['status'],
                      photoUrls=request.json['photoUrls'][0],
                      tags=[tag])
            category = Category(
                name=request.json['category']['name'],
                pets=[pet]
            )
            session.add(category)
    return json(pet.to_dict())

@petbp.route("/<petID:int>", methods=['POST'])
async def update_pet(request, petID):
    session = request.ctx.session
    async with session.begin():
        stmt = select(Pet).where(Pet.id == petID)
        result = await session.execute(stmt)
        pet = result.scalar()

        if not pet:
            return json("Không có đối tượng pet")
        else:
            stmt = (
                update(Pet).
                where(Pet.id == petID).
                values({
                    "name": request.json['name'],
                    "status": request.json['status'],
                })
            )
            await session.execute(stmt)
    return json("Cập nhập thành công")


@petbp.route("/<petID:int>", methods=['GET'])
async def get_pet(request, petID):
    session = request.ctx.session
    async with session.begin():
        stmt = select(Pet).where(Pet.id == petID).options(selectinload(Pet.tags))
        result = await session.execute(stmt)
        pet = result.scalar()

    if not pet:
        return text("Không có đối tượng pet")

    return json(pet.to_dict())


@petbp.route("/<petID:int>", methods=['DELETE'])
async def delete_pet(request, petID):
    session = request.ctx.session
    async with session.begin():

        stmt = select(Pet).where(Pet.id == petID)
        result = await session.execute(stmt)
        pet = result.scalar()

        if not pet:
            return json("Không có đối tượng pet")
        else:
            stmt = (
                delete(Pet).
                where(Pet.id == petID)
            )
            await session.execute(stmt)
    return json("delete Success")


