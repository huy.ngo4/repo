# api/pet/__init__.py
from sanic import Blueprint
from .store import storebp
from .pet import petbp

content = Blueprint.group(storebp, petbp, url_prefix="/pet")
