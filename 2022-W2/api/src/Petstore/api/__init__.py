# api/__init__.py
from sanic import Blueprint
from .pet import content
from .user import userapi

api = Blueprint.group(content, userapi, url_prefix="/api")
