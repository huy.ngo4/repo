import jwt
from functools import wraps

from sanic import Blueprint, text
from database.model import User
from sqlalchemy import select
from sanic import json

loginbp = Blueprint("loginapi", url_prefix="/")


def check_token(request):
    if not request.cookies.get('token'):
        return False
    try:
        jwt.decode(
            request.cookies.get('token'), request.app.config.SECRET, algorithms=["HS256"]
        )
    except jwt.exceptions.InvalidTokenError:
        return False
    else:
        return True


def protected(wrapped):
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            is_authenticated = check_token(request)

            if is_authenticated:
                response = await f(request, *args, **kwargs)
                return response
            else:
                return text("You are unauthorized.", 401)
        return decorated_function
    return decorator(wrapped)


@loginbp.route('/login', methods=['GET'])
async def do_login(request):
    username = request.json['username']
    password = request.json['password']
    session = request.ctx.session
    async with session.begin():
        stmt = select(User).where(User.username == request.json['username'])
        result = await session.execute(stmt)
        print(type(result))
        user = result.scalar()
        if not user:
            return json({
                "Code": 404,
                "Description": "User not found"
        })
        if username == user.username and password == user.password:
            token = jwt.encode({}, request.app.config.SECRET)
            response = text(token)
            response.cookies['token'] = token
            response.cookies['username'] = user.username
            response.cookies['password'] = user.password
            return response


