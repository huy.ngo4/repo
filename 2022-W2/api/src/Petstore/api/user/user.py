from sanic import Blueprint
from sanic.response import json
from database.model import User
from sqlalchemy import select
from sqlalchemy import delete
from sqlalchemy import update
from .loginuser import protected
from sanic import text

userbp = Blueprint('userapi', url_prefix='/')


@userbp.route('/', methods=['POST'])
@protected
async def createUser(request):
    session = request.ctx.session

    async with session.begin():
        user = User(
            username=request.json['username'],
            firstName=request.json['firstName'],
            lastName=request.json['lastName'],
            email=request.json['email'],
            password=request.json['password'],
            phone=request.json['phone'],
            userStatus=request.json['userStatus']
        )
        session.add(user)

    return json(user.to_dict())

@userbp.route('/createWithList', methods=['POST'])
async def createUserWithList(request):
    session = request.ctx.session
    result = []
    for i in range(0, len(request.json)):
        async with session.begin():
            user = User(
                username=request.json[i]['username'],
                firstName=request.json[i]['firstName'],
                lastName=request.json[i]['lastName'],
                email=request.json[i]['email'],
                password=request.json[i]['password'],
                phone=request.json[i]['phone'],
                userStatus=request.json[i]['userStatus']
            )
            session.add(user)
        result.append(user.to_dict())
    return json(result)


@userbp.route("/<username:str>", methods=['GET'])
async def getUser(request, username):
    session = request.ctx.session
    async with session.begin():
        stmt = select(User).where(User.username == username)
        result = await session.execute(stmt)
        user = result.scalar()

    if not user:
        return json({
            "Code": 404,
            "Description": "User not found"
        })
    return json({
        "Code": 200,
        "Description": "successful operation",
        "value": user.to_dict()
    })


@userbp.route("/logout")
@protected
async def logout(request):
    response = text("Logout")

    del response.cookies['username']
    del response.cookies['password']
    del response.cookies['token']
    return response


@userbp.put("/<username:str>")
@protected
async def updateUser(request, username):
    usernamecurrent = request.cookies.get('username')
    password = request.cookies.get('password')
    session = request.ctx.session
    async with session.begin():
        stmt = select(User).where(User.username == usernamecurrent)
        result = await session.execute(stmt)
        user = result.scalar()

        if not user:
            return json({
                "Code": 404,
                "Description": "User not found"
            })
        else:
            stmt = (
                update(User).
                where(User.username == usernamecurrent).
                values({
                    "username": username
                })
            )
            result = await session.execute(stmt)
            print(result)
            response = text("Update success")
            del response.cookies['username']
            del response.cookies['password']
            del response.cookies['token']
            return response


@userbp.route("/<username:str>", methods=['DELETE'])
async def deleteUser(request, username):
    session = request.ctx.session
    async with session.begin():

        stmt = select(User).where(User.username == username)
        result = await session.execute(stmt)
        user = result.scalar()

        if not user:
            return json({
                "Code": 404,
                "Description": "User not found"
            })
        else:
            stmt = (
                delete(User).
                where(User.username == username)
            )
            await session.execute(stmt)
            return json("delete Success")



