from sanic import Blueprint
from .user import userbp
from .loginuser import loginbp

userapi = Blueprint.group(loginbp, userbp, url_prefix='/user')


