# API and Sanic Framework

## Concept

1. [What is API](https://vietnix.vn/api-la-gi/)
2. [What is RESTful API](https://viblo.asia/p/restful-api-la-gi-1Je5EDJ4lnL)
3. [Sanic Framework](https://sanic.dev/en/)
   1. [Tutorial](https://sanic.dev/en/guide/getting-started.html)
   2. [](https://www.geeksforgeeks.org/introduction-to-sanic-web-framework-python/)

## Exercise

[Clone this api](https://petstore.swagger.io/) with Sanic framework

1. Clone data model
2. Build data table with model
   1. Create schema
   2. Create table in schema
3. Connect database with app
4. Build API with Sanic

orm la gi
https://hocspringboot.net/2021/08/09/orm-la-gi-tong-quan-ve-orm-framework-2/#:~:text=ORM%20(Object%20Relational%20Mapping)%2C,%2C%20'is%20a').
https://viblo.asia/p/object-relational-mapping-djeZ1PQ3KWz

cấu trúc đường dẫn trong xây dựng api
version_prefix + version + url_prefix + URI definition
 

https://sanic.dev/en/guide/deployment/configuration.html#keep-alive-timeout
https://docs.sqlalchemy.org/en/14/tutorial/index.html#unified-tutorial
https://docs.sqlalchemy.org/en/14/index.html


https://github.com/honnisha/sanic-sqlalchemy-example


basic_relationships
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html#one-to-many


xem trước
https://docs.sqlalchemy.org/en/14/orm/extensions/asyncio.html#sqlalchemy.ext.asyncio.create_async_engine

schema
https://www.postgresql.org/docs/current/sql-createschema.html
https://dizibrand.com/database-schema/#:~:text=Database%20Schema%20c%C3%B3%20ngh%C4%A9a%20l%C3%A0,s%E1%BA%A3n%20d%E1%BB%AF%20li%E1%BB%87u%20li%C3%AAn%20quan.
https://www.fullstackpython.com/sqlalchemy-sql-schema-examples.html

API
https://petstore.swagger.io/#/store/placeOrder


login
https://sanic.dev/en/guide/how-to/authentication.html#server.py
https://sanic.dev/en/guide/how-to/toc.html

SQLAlchemy 2020 (Tutoria
https://www.youtube.com/watch?v=1Va493SMTcY
https://www.youtube.com/watch?v=sO7FFPNvX2s
